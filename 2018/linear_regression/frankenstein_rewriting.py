# This script is a first attempt to generate a story that can be used as input to train a linear regression model.
# The idea is to rewrite the story so that the R² measure becomes closer and closer to 1
# the hypothesis we take as a start is the following:
# the closer an adjective is to the word 'monster' or its synonym, the more negative it becomes

import nltk
import nltk.data
from nltk.tokenize import sent_tokenize, word_tokenize
import csv
from operator import itemgetter

# find adjectives for each sentence and choose adjective that is closest in position to monster word
def selecting_adjective(words, position_word):
	pos_adjective = 0
	adjetivo = ''
	distance = 0
	min_distance = 10000
	position = 0
	counter = 0
	# find part of speech tags for each word of the sentence
	pos = nltk.pos_tag(words)
	# check if pos is adjective
	for el in pos:
		# if pos is adjective
		if el[1] == 'JJ':
			# find position of word
			pos_adjective = words.index(el[0])
			# calculate distance between adjective and monster word
			distance = abs(pos_adjective - position_word)
			# find the minimum distance
			if distance < min_distance:
				min_distance = distance
				position = counter
				adjetivo = el[0]
		counter += 1
	# return adjective & position
	return adjetivo, position

# open novel frankenstein, cleaned up for this type of machinic exercises
with open("frankenstein_for_machines.txt", 'r') as text:
	# read the text
	text = text.read()
	# split the text in sentences
	sentences = sent_tokenize(text)
#print(sentences)

# open a dictionary of adjectives with their sentiment scores (from 0 to 10) 
# --- this list needs revision!!!!
with open('creature_data_frankie.csv', 'r') as data:
	data = csv.reader(data)
	data = list(data)

# find sentences with monster words and for each of those sentences, 
# find monster word, position monsterword, closest adj, position adj, sentiment score adj
selected_sentences = []
# list of synonyms monster
monsterly = ['monster', 'beast', 'creature', 'freak', 'giant', 'monstrosity', 'miscreation', 'demon']
# loop over sentences of the novel
for sentence in sentences:
	# split sentence in words
	words = word_tokenize(sentence)
	# check each word against list of monsterly words
	for w in words:
		# if monster word is in
		if w in monsterly:
			# capture position monster word
			position_w = words.index(w)
			# find closest adjective and its position
			adj, adj_pos = selecting_adjective(words, position_w)
			# find sentiment score for adjective in external dictionary
			for el in data:
				if adj == el[0]:
					score = el[1]
					# add everything as sublist to list
					monsterly_data = sentence, w, position_w, adj, adj_pos, int(score)
					selected_sentences.append(monsterly_data)

# sort sublists by sentiment score of the adjective
selected_data = sorted(selected_sentences, key=itemgetter(5))

# read the text
for d in selected_data:
	print(d[0])

# issue to solve still: now some sentences appear multiple times, should not be the case!

