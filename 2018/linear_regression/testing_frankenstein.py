# -*- coding: utf-8 -*-
# following this manual: https://towardsdatascience.com/linear-regression-using-python-ce21aa90ade6
# using simple linear regression, trying to predict a continuous variable

import pandas as pd
import numpy as np
import tkinter 
import matplotlib.pyplot as plt   #Data visualisation libraries 
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression


# import data
test_data = pd.read_csv('creature_data_nrs.csv')

# look at data
# test_data.head()
# test_data.info()
# test_data.describe()
# test_data.columns

# shows all the data in graphs
#graph1 = sns.pairplot(test_data)
#plt.show()

# shows the histogram to see the distribution of the target variable
# graph2 = sns.distplot(test_data['VALUEADJ'])
# plt.show()

# find the correlation between variables in dataset
#correlation = test_data.corr()

# Create heatmap
# The black colour represents that there is no linear relationship between the two variables. 
# A lighter shade shows that the relationship between the variables is more linear.
# graph3 = sns.heatmap(correlation)
# plt.show()

# Split data in training data (independent variable / distance) & labels (predicted variable / positivity rate adj)
X = test_data[['DISTANCE']]
y = test_data[['VALUEADJ']]

# Split data into train & test data
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=101)

# define model Linear Regression
model = LinearRegression()

# train model with the training data
model.fit(X_train,y_train)

# predictions check
predictions = model.predict(X_test)

# visualise predictions
# graph4 = plt.scatter(y_test,predictions)
# plt.show()

# Returns the coefficient of determination R^2 of the prediction.
scores = model.score(X, y)
print(scores)

# Plot outputs
plt.scatter(X_test, y_test,  color='black')
plt.plot(X_test, predictions, color='blue', linewidth=3)

plt.xticks(())
plt.yticks(())

plt.show()