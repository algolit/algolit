var natural = require("natural");

var TfIdf = natural.TfIdf;
var tfidf = new TfIdf();

tfidf.addFileSync("books/heart-of-darkness.txt");
tfidf.addFileSync("books/pride-and-prejudice.txt");
tfidf.addFileSync("books/the-importance-of-being-earnest.txt");

console.log("marry --------------------------------");
tfidf.tfidfs("marry", function(i, measure) {
  console.log("document #" + i + " is " + measure);
});

console.log("lady --------------------------------");
tfidf.tfidfs("lady", function(i, measure) {
  console.log("document #" + i + " is " + measure);
});
