# Table embeddings
# written during Algolit
# 13 April 2018
# Notes: https://pad.constantvzw.org/p/130418_algolit_word2vec2

import os
from collections import Counter
import pprint
import random

pp = pprint.PrettyPrinter(indent=4)

filename = 'embeddings.train'
text = open('{}.txt'.format(filename),'r').readlines()
# [['empty, bankcard, pen holder\n'],
#  ['bankcard, pen holder, notebook\n'],
#  ['pen holder, notebook, keys\n']]

main = {}
# main = {
# 	'human' { 
#		Counter({
# 			'cup' : 1,
# 			'laptop' : 1,
# 		})
#	 }
# }

for ngram in text:
	ngram = ngram.split(', ')
	center = ngram[1]
	left = ngram[0]
	right = ngram[2].replace('\n','')

	if not center in main:
		main[center] = Counter()
	main[center][left] +=1
	main[center][right] +=1

pp.pprint(main)
print('************')

numbers = {1 : 'one', 2 : 'two', 3 : 'three', 4 : 'four', 5 : 'five', 6 : 'six', 7 : 'seven', 8 : 'eight', 9 : 'nine', 10 : 'ten'}
determiners = {3 : 'a few', 5 :'many', 7: 'a lot of', 6 : 'most', 2 : 'some', 1 : 'any', 4 : 'enough'}

sentences = []
x = 0
for cw, c in main.items():
	x += 1
	wwords = []
	wwordsandcounts = []
	wwordsanddeterminers = []
	for word, count in c.items():
		wwords.append(word)
		
		if count > 1:
			word = word+'s'
		determiner = determiners[count]
		
		wwordsandcounts.append(numbers[count] + ' ' + word)
		wwordsanddeterminers.append(determiner + ' ' + word)

	intro = ''
	templates = [
		'What makes a {0} a {0}, is its closeness to {1} and {2}.'.format(cw, ', '.join(wwordsanddeterminers[:-1]), wwordsanddeterminers[-1]),
		'You can identify a {} when it appears in the company of {} and {}.'.format(cw, ', '.join(wwordsandcounts[:-1]), wwordsandcounts[-1]),
		'A {} can be recognized when it lies next to a {} and a {}.'.format(cw, ', '.join(wwords[:-1]), wwords[-1])
	]

	if x == 5:
		sentences.append('\nMeanwhile ...\n')
		x = 0

	s = random.choice(templates)
	sentences.append(s)

for sentence in sentences:
	print(sentence)

head = '''<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<style>
		body{
			position:relative;
			width:700px;
			margin:3em auto;
		}
		.meanwhile{
			margin: 2em 0 2em 1em;
		}
		img{
			width:50%;
		}
		.left{
			float:left;
			margin:0 2em 1em -2em;
		}
		.right{
			float:right;
			margin:0 -2em 1em 2em;
		}
	</style>
</head>
<body>
<h1>situation embeddings</h1>
'''

foot = '''</body>
</html>
'''

images = [f for f in os.listdir('./img/{}'.format(filename))]

with open('{}.html'.format(filename),'w') as html:
	html.write(head)
	html.write('<img src="./img/{}/{}" class="right">'.format(filename, random.choice(images)))
	i = 0
	for sentence in sentences:
		if 'Meanwhile' in sentence: 
			html.write('<p class="meanwhile">'+sentence+'</p>')
			print(i, i % 2)
			if i % 2 == 0: 
				html.write('<img src="./img/{}/{}" class="left">'.format(filename, random.choice(images)))
			else:
				html.write('<img src="./img/{}/{}" class="right">'.format(filename, random.choice(images)))
		else:
			html.write('<p>'+sentence+'</p>')
			i += 1
	html.write(foot)
	print('*html file written*')