class TrainingDatasetPoint extends DatasetPoint {

  TrainingDatasetPoint(float x_axis_width, float y_axis_height) {
    super(x_axis_width, y_axis_height);
    //We would like to use any particular function
    float lineY = exact_function(x);
    if (y > lineY) {
      label = 1;
    } else {
      label = -1;
    }
  }

  //Draws the point on the display
  void show(boolean currentPoint) {
    stroke(0); 
    if ( label == 1) {
      fill(255);
    } else {
      fill(0);
    }
    if ( currentPoint == true) {
      ellipse(pixelX(), pixelY(), 8, 8);
      noFill();
      stroke(0);
      ellipse(pixelX(), pixelY(), 14, 14);
      fill(0);
      text("("+x+","+y+")", pixelX()+10, pixelY()+10);
    } else {
      ellipse(pixelX(), pixelY(), 8, 8);
    }
  }
}