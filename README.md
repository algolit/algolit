Algolit is a workgroup around i-literature, free code and texts.It was initiated by Constant in 2012.

The group meets regularly following the principles of the Oulipo-meetings: they share work and thoughts and create together, with or without the company of an invitee. Algolit is open to anyone interested in exchanging practises around digital ways of reading and writing. 

We find it a joy when our works are taken on by others, tweaked, customized and redistributed, so please feel free to copy and test the code from this repository. If the sources of a particular project are not there, you can always contact us through the mailinglist. You can find a link to our repository, etherpads, and wiki at http://www.algolit.net.

All code on this repository is published as follows:
Algolit. Copyright. 2019. This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    These programs are distributed in the hope that they will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For the full license, see <https://www.gnu.org/licenses/>.
