cgi-interface template
======================
Structure of this folder: 

> cgi-bin
>    interface.cgi
> interface.html
> css
>    stylesheet.css
>    fonts
> img
> js
>    jquery-2.1.3.js

Requirements
============
* Jinja 
* Weasyprint (for the export function)


Before running the cgi script
=============================
* the cgi-bin/interface.cgi needs to be executable

> chmod u+x cgi-bin/interface.cgi

* the same counts for cgi-bin/export.cgi

> chmod u+x cgi-bin/export.cgi

* the output folder needs to be writable

> chmod u+w output


Use 
===
To run the browser-tool locally, use the python server module. 
Navigate to this folder and start the server from there. 

> cd ~/path/of/main/folder
> python -m CGIHTTPServer

This run a CGI server on localhost:8000.
Open in a browser: 

> localhost:8000/cgi-bin/interface.cgi 

It is possible to run multiple python servers, but not on the same port. 
To run a server on port 9000 for example: 

> python -m CGIHTTPServer 9000
