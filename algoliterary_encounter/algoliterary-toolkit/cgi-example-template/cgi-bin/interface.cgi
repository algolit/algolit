#!/usr/bin/env python
from __future__ import print_function
import os, sys
import cgi
import cgitb; cgitb.enable()
from jinja2 import FileSystemLoader, Environment
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize, TreebankWordTokenizer
from nltk.util import ngrams
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

# *******************************************************
# settings
# *******************************************************
path = "./"
env = Environment(loader=FileSystemLoader(path)) 
template = env.get_template("interface.html")

export = 'enabled'
# export = 'disabled'

# *******************************************************
# read form fields into variables
# *******************************************************
method = os.environ.get("REQUEST_METHOD")
form = cgi.FieldStorage()
title = form.getvalue("title", "").decode('utf-8')
input_text = form.getvalue("input_text", "").decode('utf-8')

# export settings
source = form.getvalue("source", "").decode('utf-8')
filename = form.getvalue("filename", "").decode('utf-8')
author = form.getlist('author')
if len(author) > 1:
	', '.join(author)

# *******************************************************
# change variables (when form is submitted)
# *******************************************************

# for example:
lowercase = input_text.lower()







# *******************************************************
# set jinja variables
# *******************************************************
tvars = {}
tvars['method'] = method
tvars['title'] = title
tvars['input_text'] = input_text

# to use objects in the interface.html, define new variables here

# for example: 
tvars['lowercase'] = lowercase

# export
tvars['export'] = export
tvars['filename'] = filename
tvars['source'] = source
tvars['author'] = author

# *******************************************************
# render
# *******************************************************
print('*variables rendered*\n', file=sys.stderr)

print("Content-type: text/html;charset=utf-8")
print()
html = template.render(tvars).encode('utf-8')
print(html)