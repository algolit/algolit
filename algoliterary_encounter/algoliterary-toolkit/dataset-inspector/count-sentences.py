import os

# x = 1

# while x==1:

# read dataset file
dataset = open('dataset.txt', 'r').read().decode('utf8')
# print dataset

filterstopwords = 'yes'

# *** NON NLTK ***

# # split the text into sentences on the "."
# sentences = dataset.split('.')
# print sentences

# # print every sentence (and add the "." again)
# for sentence in sentences:
# 	print sentence+'.', '\n'


# *** USING NLTK ***
import nltk, collections

sent_tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
sentences = sent_tokenizer.tokenize(dataset)

print sentences
numberofsentences = len(sentences)


# *** most common sorted dict (from Word2Vec) ***

words = []
for line in sentences:
	wordsline = line.split(' ')
	for word in wordsline:
		word = word.lower()
		
		# apply stopwords filter
		if filterstopwords == 'yes':
			from nltk.corpus import stopwords
			stop = set(stopwords.words('english'))

			if word not in stop:
				words.append(word)

# define the unknown word
count = [['UNK', -1]]

count.extend(collections.Counter(words).most_common(numberofsentences - 1))

# create dictionary of most common words + index number
# frequency value is ignored, the number sets the order
dictionary = dict()
for word in sentences:
	dictionary[word] = len(dictionary)

# print('dictionary', dictionary)

data = list()

# counting how many words from the input file are disregarded
unk_count = 0

# if word of text is in (vocabulary_size) most common words, 
# it is translated into the index number of that word
for word in words:
	if word in dictionary:
		index = dictionary[word]
	else:
		index = 0  # dictionary['UNK']
		unk_count += 1

		# printing the excluded words
		# print(word)
	
	data.append(index)

print count

top = []

for i, item in enumerate(count):
	print item
	if i != 0:
		for x in item:
			if i <= 250:
				print x
				top.append('<div class="word">'+str(x)+'</div>')

# *** export to html ***
if filterstopwords == 'yes':
	stopwords = 'Words that are included in NLTK\'s dictionary of stopwords are not taken into account'
else:
	stopwords = ''
html = '''
<DOCTYPE html>
<html>
<head>
	<meta charset="utf8">
	<link rel="stylesheet" type="text/css" href="count-sentences.css"/>
	<meta refresh content="5 count-sentences.html">
</head>
<body>
	<div id="count">
		<div class="text stopwords">'''+stopwords+'''</div>
		<div class="text">Number of sentences in the current dataset:</div>
		<div class="number">'''+str(numberofsentences)+'''</div>
		<div class="number">sentences</div>
	</div>
	<div id="list">
		'''+' '.join(top)+'''
	</div>
</body>
</html>
'''

with open('count-sentences.html', 'w+') as f:
	f.write(html)
	f.close()
	print '*html generated*'