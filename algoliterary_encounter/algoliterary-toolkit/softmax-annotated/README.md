Softmax excercise
=================

Python version of the softmax function.

This script is an expanded version with many options that do the same (or approximations to the very same)).

The code is based on a Softmax example script on the following Wikipedia page: https://en.wikipedia.org/wiki/Softmax_function.
