# wiki softmax function (expanded version with many options that do the same (or approximations to the very same))
# following the Wikipedia example: https://en.wikipedia.org/wiki/Softmax_function

import math

print ''

print '** input values:'
inputvalues = [1.0, 2.0, 3.0, 4.0, 1.0, 2.0, 3.0]
print '**', inputvalues

print ''

# -------------------------------------------------
# ORIGINAL CODE -----------------------------------
print '-- multiplication with math.e (original code):'
x_exp = [math.exp(x) for x in inputvalues]
print '--', x_exp # Result: [2.72, 7.39, 20.09, 54.6, 2.72, 7.39, 20.09]
# -------------------------------------------------

print ''

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# STUDY CODE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print '~~ multiplication with math.e (study code):'
x_exp = []
print '~~ math.e:', math.e
for x in inputvalues:
	# exp = math.exp(x)
	exp = math.e**x
	# math.e = 2.718281828459045 # (this is an approximation) [~~] 
	# exp = 2.718281828459045**x # (this is an approximation) [~~]
	x_exp.append(exp)
	print '~~ x:', x
	print '~~ x * math.e:', exp
print '~~', x_exp # Result: [2.72, 7.39, 20.09, 54.6, 2.72, 7.39, 20.09]
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

print ''

# -------------------------------------------------
# ORIGINAL CODE -----------------------------------
print '-- sum of multiplied values (original code)'
sum_x_exp = sum(x_exp)
print '--', sum_x_exp  # Result: 114.98 
# -------------------------------------------------

print ''

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# STUDY CODE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print '~~ sum of multiplied values (study code)'
sumofall = 0
for y in x_exp:
	print '~~ y:', y
	sumofall = sumofall + y
print '~~', sumofall # Result: 114.98 
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

print ''

# -------------------------------------------------
# ORIGINAL CODE -----------------------------------
print '-- softmax: a list of values that are constructed by "multiplied value / sum of multiplied values" (original code)'
softmax = [round(y / sum_x_exp, 3) for y in x_exp]
print '--', softmax # Result: [0.024, 0.064, 0.175, 0.475, 0.024, 0.064, 0.175]
# -------------------------------------------------

print ''

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# STUDY CODE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print '~~ softmax: a list of values that are constructed by "multiplied value / sum of multiplied values" (study code)'

# [~~] remember!!!
# print '~~', 5 / 3 # Result: 1
# print '~~', 3 / 5 # Result: 0
# print '~~', 3.0 / 5.0 # Result: 0.6

softmax = []
for y in x_exp:
	s = round(y / sumofall, 3) # Function round() = round(number [, ndigits]) (?)

	print '~~', 'y:', y, '- sum:', sumofall
	# [~~] i = 1.0, Result: y: 2.71(...) - sum: 114.9(...)
	# [~~] i = 2.0, Result: y: 7.38(...) - sum: 114.9(...)
	# [~~] i = 3.0, Result: y: 20.08(...) - sum: 114.9(...)
	# [~~] i = 4.0, Result: y: 54.59(...) - sum: 114.9(...)

	print '~~', 'y / sum:', y / sumofall
	# [~~] i = 1.0, Result: 2.71 / 114.9 = 0.0236405430216
	# [~~] i = 2.0, Result: 7.38 / 114.9 = 0.0642616585105
	# [~~] i = 3.0, Result: 20.08 / 114.9 = 0.174681298596
	# [~~] i = 4.0, Result: 54.59 / 114.9 = 0.474832999744

	softmax.append(s)

print '~~', softmax # Result: [0.024, 0.064, 0.175, 0.475, 0.024, 0.064, 0.175]
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

print ''

# -------------------------------------------------
# links:
# -------------------------------------------------
# source: https://docs.scipy.org/doc/numpy/reference/generated/numpy.exp.html