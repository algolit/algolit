#!/usr/bin/python -tt
# -*- coding: utf-8 -*-

from nltk.tokenize import word_tokenize
import re
import string
import codecs

filename = '../input/book-7-Advances in Computer Vision and Pattern Recognition_Le Lu, Yefeng Zheng, Gustavo Carneiro, Lin Yang (eds.)-Deep Learning and Convolutional Neural Networks for Medical Image Computing_ Precision Medi.txt'

sentences = []
regex = re.compile('[%s]' % re.escape(string.punctuation)) #see documentation here: http://docs.python.org/2/library/string.html

with open(filename, 'r') as source:
	lines = source.readlines()
	if len(lines) == 1:
		lines = lines[0]
		lines = lines.split('. ')
	print lines
	for line in lines:
		# print line
		if "”" in line:
			line = line.replace("”","")
		if "“" in line:
			line = line.replace("“","")
		line = line.decode('utf8')
		words = word_tokenize(line)
		print words
		string = []
		for word in words:
			new_word = regex.sub(u'', word)
			if not new_word == u'':
				string.append(new_word)
		string = ' '.join(string)
		sentences.append(string)

outputfilename = filename.replace('.txt','_strippted.txt')
with codecs.open(outputfilename, "w", "utf-8") as destination:
	for sentence in sentences:
		destination.write(sentence.strip().capitalize()+" ")
	print '*text is stripped*'
