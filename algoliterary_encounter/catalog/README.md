=Algolit Catalog=
The algolit catalog is generated using a workflow where the content is retreived
from the algolit wiki. A simple scraper downloads the a page from the wiki and
follows all the links to the lemmas.

The CSS is downloaded from an etherpad.

This html is opened in a browser and 'printed' to a pdf file. Because of the use of
css-columns and a bug in Firefox' (the pdf is a single page) column-fill property
currently only chrome and chromium can be used.

==Install requirements==
to install the requirements:
> pip install -r requirements.txt

==Generate==
To generate the html files for the French and English catalog:
> python makeCatalog.py

==style.css==
To edit the css, visit this [pad](http://pad.constantvzw.org/p/neural_networks_maisondulivre_catalogue_css).

To download the styles from the pad
> bash loadstyles.sh

==read & print==
Open the catalog.html in the browser, print the file to a pdf file.

==Pagenumbers (optional)==
Blink doesn't support pagenumbers yet so they are generated using a silly hack: pagenumbers.html.
This file is 'printed' to a seperate pdf file.
Using pdftk the two pdf-files are laid on top of each other:
pdftk catalog-file.pdf multistamp pagenumbers-file.pdf output catalog-with-pagenumbers.pdf 

Oddly Chrom(e/ium) all of a sudden doesn't print 'transparent' pdf anymore so for this catalog
[OSPKitPDF](http://osp.kitchen/tools/ospkit/) was used to generate a proper transparent file for the pagenumbers.
