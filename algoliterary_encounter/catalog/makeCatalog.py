#! /usr/bin/env python2
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup as bs
from bs4 import NavigableString, Tag
import urllib
from urlparse import urljoin, urlparse
import codecs
import copy
import re

baseurl = 'http://www.algolit.net'

template = u"""<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Algolit Catalog</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
  </body>
</html>"""


def makeCircleHeader(title, soup):
    asciii1 = soup.new_tag('pre')
    asciii1['class'] = 'ascii'
    asciii1.append(u'''
           %%%    %%%
      %%%              %%%

  %%%                      %%%

%%%                          %%%''')
    asciiiname = soup.new_tag('div')
    asciiiname['class'] = 'asciiname'
    asciii2 = soup.new_tag('pre')
    asciii2['class'] = 'ascii'
    asciii2.append(u'''%%%                          %%%

 %%%                        %%%

    %%%                  %%%

          %%%     %%%''')
    asciiiname.append(title)

    wrapper = soup.new_tag('section')
    wrapper['class'] = 'asciiheaderwrapper groupheader center'

    wrapper.append(asciii1)
    wrapper.append(asciiiname)
    wrapper.append(asciii2)

    return wrapper


def makeBranch(level, tags, soup):
    branch = soup.new_tag('ul')
    leaf = None

    while len(tags) > 0:
        t = tags[0]

        if t['level'] > level and leaf:
            leaf.append(makeBranch(t['level'], tags, soup))
        elif t['level'] < level:
            if (leaf):
                branch.append(leaf)
                leaf = None

            return branch
        else:
            if (leaf):
                branch.append(leaf)
                leaf = None

            leaf = soup.new_tag('li')
            leaf.append(tagContent(tags[0]['tag']))
            tags.pop(0)

    if (leaf):
        branch.append(leaf)
        leaf = None
    return branch


def makeIndex(soup):
    eligible_tags = ['h1', 'h2', 'h3', 'h4', 'h5', 'li']

    buffer = soup.new_tag('ul')
    tags = [{'tag': copy.copy(tag), 'level': eligible_tags.index(
        tag.name)} for tag in soup.find_all(eligible_tags)]

    return makeBranch(tags[0]['level'], tags, soup)


def tagContent(tag):
    if tag.string:
        return tag.string
    else:
        return ''.join(tag.strings)


def classSafeContent(string):
    return re.sub(r'[^\w\-]+', '', re.sub(r'\s+', '-', string.lower())).encode("ascii", "replace")


def makeLemma(title, url, bigSoup):
    print url
    lemmaSoup = bs(urllib.urlopen(
        '{0}?action=render'.format(url)), 'html.parser')
    lemma = bigSoup.new_tag("section")
    lemma['class'] = 'lemma {}'.format(classSafeContent(title))

    sectiontitle = tagContent(child)

    asciii = bigSoup.new_tag('pre')
    asciii['class'] = 'ascii'
    asciii.append(u'''%
%
% {}'''.format(title))

    header = bigSoup.new_tag('section')
    header['class'] = 'asciiheaderwrapper lemmaheader'

    header.append(asciii)
    lemma.append(header)

    for t in lemmaSoup.contents:
        if isinstance(t, Tag):
            lemma.append(copy.copy(t))

    for img in lemma.find_all('img'):
        img.attrs['src'] = urljoin(baseurl, img.attrs['src'])

    return lemma


def pageBreaker(soup):
    breaker = soup.new_tag('section')
    breaker.attrs['class'] = 'page-breaker'
    breaker.string = u' '

    return breaker


doublebreak = ['algoliterary-explorations']

pages = (('en', 'http://www.algolit.net/index.php/Algoliterary_Encounters?action=render'),
         ('fr', 'http://algolit.net/index.php/Rencontres_Algolittéraires?action=render'))

for (lang, url) in pages:
    print lang, url
    soup = bs(template, 'html.parser')
    pageSoup = bs(urllib.urlopen(url), 'html.parser')
    container = soup.new_tag('section')
    container['class'] = u'language {}'.format(lang)

    index = soup.new_tag('section')
    index.attrs['class'] = 'index'
    index.append(makeCircleHeader('Algoliterary Encounters' if (
        lang == 'en') else 'Rencontres Algolittéraires', soup))
    index.append(makeIndex(pageSoup))
    soup.append(index)

    for child in pageSoup.contents:
        # print child.name
        if child.name == 'ul':
            chapter = soup.new_tag('section')
            chapter['class'] = 'group'

            for li in child.find_all('li'):
                links = li.find_all('a')
                if links:
                    url = urljoin(baseurl, links[-1].attrs['href'])
                    if re.match('.*algolit.net$', urlparse(url).netloc) or re.match('.*algolit.constantvzw.org$', urlparse(url).netloc):
                        title = tagContent(links[-1])
                        chapter.append(
                            makeLemma(re.sub(r'\s*FR\s*', '', title), url, soup))

            container.append(chapter)

        elif child.name == 'h2':
            title = tagContent(child)
            hid = classSafeContent(title)
            header = makeCircleHeader(title, soup)
            header.attrs['id'] = hid

            if (hid in doublebreak):
                container.append(pageBreaker(soup))

            container.append(header)

        elif child.name == 'h3':
            sectiontitle = tagContent(child)
            hid = classSafeContent(sectiontitle)

            asciii1 = soup.new_tag('pre')
            asciii1['class'] = 'ascii box'
            asciii1.append(u'''%%%      %%%''')
            asciiiname = soup.new_tag('div')
            asciiiname['class'] = 'asciiname box'
            asciiiname.append(sectiontitle)

            headerwrapper = soup.new_tag('section')
            headerwrapper.attrs['class'] = 'asciiheaderwrapper center h3'
            headerwrapper.attrs['id'] = hid

            headerwrapper.append(asciii1)
            headerwrapper.append(asciiiname)

            container.append(headerwrapper)
        else:
            container.append(copy.copy(child))

    for header in container.find_all(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']):
        header.attrs['id'] = classSafeContent(tagContent(header))
        header.replace_with(header)

    soup.append(container)

    with codecs.open('html/catalog.{}.html'.format(lang), 'w', encoding='utf-8') as out:
        out.write(unicode(soup))
        out.close()
