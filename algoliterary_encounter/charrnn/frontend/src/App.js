import React, { Component } from 'react';
import { generate as generateText } from './services/api.js';
import { CheckpointSelector, DatasetSelector, LengthSlider } from './components';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { dataset: null, checkpoint: null, length: 500, text: '', generating: false };
  }

  onDataset = (dataset) => this.setState({ dataset })

  onCheckpoint = (checkpoint) => this.setState({ checkpoint })

  onLength = (length) => this.setState({ length })

  generate = () => {
    const dataset = this.state.dataset;
    const checkpoint = this.state.checkpoint;
    const length = this.state.length;
    this.setState({ generating: true })
    generateText({ dataset, checkpoint, length })
      .then((data) => this.setState({ text: data.text, generating: false }))
      .catch(() => {
        this.setState({ generating: false })
      })
  }

  render() {
    let generateButton;
    if (this.state.generating) {
      generateButton = <section className="control">
        <button onClick={this.generate} disabled>Generating...</button>
      </section>;
    }
    else {
      generateButton = <section className="control">
        <button onClick={this.generate}>Generate</button>
      </section>;
    }
    return (
      <div className="App">
        <nav className="controls">
          <DatasetSelector onChange={this.onDataset} />
          <CheckpointSelector dataset={this.state.dataset} onChange={this.onCheckpoint} />
          <LengthSlider onChange={this.onLength} />
          {generateButton}
        </nav>
        <article className="generated-text">
          {this.state.text.split('\n').map((t) => <p>{t}</p>)}
        </article>
      </div>
    );
  }
}

export default App;
