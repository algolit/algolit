import React, { Component } from 'react';
import { checkpoints as getCheckpoints } from '../services/api';

export default class DatasetSelector extends Component {
  constructor(props) {
    super(props);
    this.state = { data: null, value: null, dataset: null };
  }

  onChange = (e) => {
    const value = this.state.data[parseInt(e.target.value, 10)];
    this.setState({ value });
    this.props.onChange(value);
  }

  render = () => {
    if (this.state.data !== null && this.state.dataset === this.props.dataset) {
      return <section className="control">
        <label htmlFor="checkpointSelector">Checkpoint</label>
        <select id="checkpointSelector" onChange={this.onChange}>
          {this.state.data.map((v, k) =>
            <option key={k} value={k}>{v}</option>)}
        </select>
      </section>
    }
    else {
      const dataset = this.props.dataset;
      getCheckpoints(dataset).then((data) => {
        this.setState({ data, dataset });
        this.props.onChange(data[0]);
      });
      return <section className="control checkpoint-selector">
        <label htmlFor="checkpointSelector">Checkpoint</label>
        <select id="checkpointSelector" disabled></select>
      </section>
    }
  }
}