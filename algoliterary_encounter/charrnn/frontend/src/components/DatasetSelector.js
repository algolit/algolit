import React, { Component } from 'react';
import { datasets as getDatasets } from '../services/api';

export default class DatasetSelector extends Component {
  constructor(props) {
    super(props);
    this.state = { data: null, value: null }
    getDatasets().then((data) => {
      this.setState({ data });
      this.props.onChange(data[0]);
    });
  }

  onChange = (e) => {
    const value = this.state.data[parseInt(e.target.value, 10)];
    this.setState({ value });
    this.props.onChange(value);
  }

  render = () => {
    if (this.state.data !== null) {
      return <section className="control">
        <label htmlFor="datasetSelector">Dataset</label>
        <select id="datasetSelector" onChange={this.onChange}>
          {this.state.data.map((v, k) =>
            <option key={k} value={k}>{v}</option>)}
        </select>
      </section>
    }
    else {
      return <section className="control dataset-selector">
        <label htmlFor="datasetSelector">Dataset</label>
        <select id="datasetSelector" disabled></select>
      </section>
    }
  }
}