import React, { Component } from 'react';

export default class DatasetSelector extends Component {
  constructor(props) {
    super(props);
    this.state = { value: 500, };
  }

  onChange = (e) => {
    const value = e.target.value;
    this.setState({ value });
    this.props.onChange(value);
  }

  render = () => {
    return <section className="control length-slider">
      <label htmlFor="checkpointSelector">Textlength</label>
      <input type="range" min="500" step="100" max="2000" value={this.state.value} onChange={this.onChange} />
      <input type="number" size="4" value={this.state.value} onChange={this.onChange} />
    </section>
  }
}