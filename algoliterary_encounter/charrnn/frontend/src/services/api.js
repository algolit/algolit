const API_URL = "http://192.168.9.119:5556/";

export const api_url = (tail) => {
  return `${API_URL}${tail}`;
}

const get = (url) => {
  const promise = new Promise((resolve, reject) => {
    fetch(api_url(url), {
      method: "GET"
    }).then((response) => {
      if (response.ok) {
        response.json()
          .then(data => resolve(data))
          .catch(reject);
      }
      else {
        reject();
      }
    }).catch(reject);
  });

  return promise;
}

// TODO generalize API call function
const post = (url, data) => {
  const promise = new Promise((resolve, reject) => {
    let postData = new FormData();

    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        postData.append(key, data[key]);
      }
    }

    fetch(api_url(url), {
      method: "POST",
      body: postData
    }).then((response) => {
      if (response.ok) {
        response.json()
          .then((data) => resolve(data))
          .catch(reject);
      } else {
        reject();
      }
    }).catch(reject);
  });

  return promise;
}

export const datasets = () => get('datasets')

export const checkpoints = (dataset) => get(`checkpoints/${dataset}`)

export const generate = (data) => post('generate', data)