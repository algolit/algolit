### Python 2.7
create virtual environment for project
$ virtualenv
$ pip install
$ pip freeze -> creates list of installed packages

### Requirements install
pip install -r requirements.txt


### Follow installation instructions Torch-RNN here:
http://pad.constantvzw.org/p/algorithmic_uncertainty.torch-rnn.install
(needs translation to English!)

### New text
copy .txt in charrnn/torch-rnn/data
Convert text to json, go to charrnn/torch-rnn/scripts
§ python preprocess.py

### Train on new text
Go to charrnn/torch-rnn
$ th train.lua -input_h5 data/timmermans.h5 -input_json data/timmermans.json -model_type rnn -num_layers 2 -rnn_size 256 -wordvec_size 128

### Print checkpoints
$ th sample.lua -checkpoint cv/checkpoint_89000.t7 -length 200

### Run model
Go to folder Frontend/build
launch public server :
$ python2 -m SimpleHTTPServer

Open Browser
localhost:8000

Launch api!
Go to folder Backend
$ python app.py
