#! /bin/bash

# Script to concatenate all the raw data in the rawdata folder
# and train the network

cd /home/algolit/torch-rnn/;

if [ ! -d ./raw-data ]; then
  mkdir ./raw-data
fi

name="$(date +%s)";
txt="./raw-data/${1}.txt"
h5="./data/${1}-${name}.h5"
json="./data/${1}-${name}.json"
cv="cvs/${1}/${name}"

mkdir -p "${cv}";

# Combine raw texts and merge into one file
#cat ./texts/*.txt > $txt;
# Make a copy of the data-set for processing outside
#cp -f $txt ./raw-data/latest.txt;

# Activate the venv and preprocess the data
source ~/venvs/torch-rnn/bin/activate;
python scripts/preprocess.py \
  --input_txt $txt \
  --output_h5 $h5 \
  --output_json $json

# Start the training
th train.lua -input_h5 $h5 -input_json $json -num_layers 3 -rnn_size 256 -seq_length 75 -max_epochs 150 -checkpoint_name "${cv}/checkpoint";

# Echo after the training
echo "Training done. Use the following command to sample from the latest checkpoint:"
files=$(ls -t "${cv}/checkpoint*.t7");
set -- $files;
echo "th sample.lua -checkpoint ${1}";
