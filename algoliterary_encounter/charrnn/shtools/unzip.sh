#! /bin/bash

# Script to unzip all books and put them in a folder 'merged'

#cd /home/algolit/gutenberg;

if [ ! -d ./merged ]; then
  mkdir ./merged
fi

for path in /home/algolit/gutenberg/aleph.gutenberg.org/*/*/*/*/*/*.zip; do
	unzip $path -d ./merged;
done;

# Remove books with -8 name, unsure why these copies are there anyhow; codec?
for file in /home/algolit/gutenberg/merged/*-8.txt; do
	name=$(basename $file);
	num="${name%-8.txt}.txt";
	rm ./merged/$num;
	rm ./merged/$name;
done;
