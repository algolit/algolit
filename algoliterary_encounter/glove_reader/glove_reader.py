#!/usr/bin/env python
# This script takes pretrained wordvectors using GloVe as input and prints each word with the 300 dimensions, using a colour and central position for the word
# Download wordvectors here: http://nlp.stanford.edu/data/glove.42B.300d.zip

#    Copyright (C) 2016 Constant, Algolit
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details: <http://www.gnu.org/licenses/>.

import re
import colors
from colors import green
import time
from collections import OrderedDict
import os

# open file
#path = "data/glove.42B.300d.txt"
path="data/glovesample.txt"

# declare regular expression, any letter
pattern = re.compile("[a-zA-Z]*")

# read line per line
with open(path, 'r', 1) as source:
	os.system('cls' if os.name == 'nt' else 'clear')
	for line in source:
		# find the word
		cuts = re.findall(pattern, line)
		# if there is a word
		if cuts[0]:
			# isolate word
			key = cuts[0]
			# calculate lenght of the word
			cut = len(key)
			# split line in a list of word & numbers
			value = line[cut:]
			print("\t\t\t\t\t\t\t\t\t", green(key), "\n")
			# print numbers
			print(value, "\n")
			# allow 1 second pause for legibility
			time.sleep(1)
			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")
	source.close()

# 66 woorden per seconde als we in 8u zouden tonen
# alfabetisch rangeschikken in tweede script
