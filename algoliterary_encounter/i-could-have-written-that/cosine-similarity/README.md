Writing Machines
================
This is a writing system that works with the [cosine similarity](http://scikit-learn.org/stable/modules/generated/sklearn.metrics.pairwise.cosine_similarity.html) function of SciKit Learn.

Requirements
============
The requirements for this tool are listed in the requirements.txt file.
To install these, run:

> pip install -r requirements.txt

Output folder
=============
Exported html & pdf documents are written to the 'output' folder. Before using the tool, create the following folders:

> writing-systems/cosine-similarity/output/html/regular/
> writing-systems/cosine-similarity/output/html/booklet/
> writing-systems/cosine-similarity/output/pdf/regular/
> writing-systems/cosine-similarity/output/pdf/booklet/

Use 
===
To run the tool locally, use the python server module. 
Navigate to this folder in the terminal and start the server from there. 

> cd ~/path/i-could-have-written-that/writing-machines/cosine-similarity/
> python -m CGIHTTPServer

This run a CGI server on localhost:8000.

Open in a browser: 

> localhost:8000/cgi-bin/cosine-similarity-morphs.cgi 

---

Note: It is possible to run multiple python servers, but not on the same port. 
To run a server on port 9000 for example: 

> python -m CGIHTTPServer 9000

modules
=======
beside the .cgi interface, there are some standalone scripts in the modules folder.


