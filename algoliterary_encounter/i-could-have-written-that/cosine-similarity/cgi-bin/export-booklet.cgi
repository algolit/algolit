#!/usr/bin/env python

from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
import os, sys
from datetime import datetime

# *******************************************************
# read input fields into variables
# *******************************************************
form = cgi.FieldStorage()

filename = form.getvalue("filename", "").decode('utf-8')
title = form.getvalue("title", "").decode('utf-8')
source = form.getvalue("source", "").decode('utf-8')
author = form.getvalue("author", "").decode('utf-8')
results = form.getvalue("results", "").decode('utf-8')
result1 = form.getvalue("result1", "").decode('utf-8')
result2 = form.getvalue("result2", "").decode('utf-8')
result3 = form.getvalue("result3", "").decode('utf-8')
result4 = form.getvalue("result4", "").decode('utf-8')

print('> printing title', title, file=sys.stderr)
print('> printing ', filename, file=sys.stderr)

date = datetime.now().strftime('%d %B %Y, at %H:%M:%S')

# ****************
# export
# ****************
def save(filename):

	# ***********************
	# HTML > posted via AJAX
	# ***********************
	html_out = 'output/html/booklet/'+filename+'.html'
	with open (html_out,'w+') as f:
		html = '''<!DOCTYPE html>
			<html class="page">
			<head>
				<meta charset="utf-8">
				<title>'''+title+''' (booklet)</title>
				<link rel="stylesheet" type="text/css" href="../../../css/stylesheet.css">
				<link rel="stylesheet" type="text/css" href="../../../css/export-booklet.css">
			</head>
			<body>
				<div id="header">
					This document is generated with the Cosine Similarity Morph interface on <em>'''+date+'''</em>, and is written by the following author(s): <em>'''+author+'''</em>. It is based on the source <em>'''+source+'''</em> and set in the font <a href="https://www.checkmyworking.com/cm-web-fonts/" target="_blank">Computer Modern Unicode Typewriter</a>. Licenced under the <a href="http://artlibre.org/licence/lal/en/" target="_blank">Free Art licence</a>. </div>
				<div id="wrapper" class="first">
					'''+result4+'''
					<div class="pagenumber">4 - <em>'''+title+'''</em></div>
				</div>
				<div id="wrapper" class="second">
					'''+result1+'''
					<div class="pagenumber">1 - <em>'''+title+'''</em></div>
				</div>
				<div id="wrapper" class="third">
					'''+result2+'''
					<div class="pagenumber">2 - <em>'''+title+'''</em></div>
				</div>
				<div id="wrapper" class="fourth">
					'''+result3+'''
					<div class="pagenumber">3 - <em>'''+title+'''</em></div>
				</div>
			</body>
			</html>'''
		f.write(html.encode('utf8'))
		f.close()

		print('> html file written', file=sys.stderr)


	# *******************************************
	# PDF with wkhtmltopdf: html > pdf
	# *******************************************
	pdf_out = 'output/pdf/booklet/'+filename+'.pdf'
	cmd = "wkhtmltopdf --page-width 210 --page-height 297 --disable-smart-shrinking --zoom 1.5 "+html_out+' '+pdf_out
	print('>>>', cmd, file=sys.stderr)
	os.system(cmd)

	print('> pdf generated', file=sys.stderr)
	print('> done', file=sys.stderr)

if filename:
	save(filename)