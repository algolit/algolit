#!/usr/bin/env python
from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
import os, sys
from datetime import datetime

# *******************************************************
# read input fields into variables
# *******************************************************
form = cgi.FieldStorage()
filename = form.getvalue("filename", "").decode('utf-8')
results = form.getvalue("results", "").decode('utf-8')
source = form.getvalue("source", "").decode('utf-8')

title = form.getvalue("title", "").decode('utf-8')
source = form.getvalue("source", "").decode('utf-8')
author = form.getvalue("author", "").decode('utf-8')

print('> printing ', filename, file=sys.stderr)

date = datetime.now().strftime('%d %B %Y, at %H:%M:%S')

# ****************
# export
# ****************
def save(filename):

	# ***********************
	# HTML > posted via AJAX
	# ***********************
	html_out = 'output/html/regular/'+filename+'.html'
	with open (html_out,'w+') as f:
		html = '''<!DOCTYPE html>
			<html class="page">
			<head>
				<meta charset="utf-8">
				<title>'''+title+'''</title>
				<link rel="stylesheet" type="text/css" href="../../../css/stylesheet.css">
				<link rel="stylesheet" type="text/css" href="../../../css/export.css">
			</head>
			<body>
				<div id="header">This document <b>'''+title+'''</b> is generated with the Cosine Similarity Morph interface on <b>'''+date+'''</b>, and is written by the following author(s): <b>'''+author+'''</b>. It is based on the source '''+source+''' and set in the font <a href="https://www.checkmyworking.com/cm-web-fonts/" target="_blank">Computer Modern Unicode Typewriter</a>. Licenced under the <a href="http://artlibre.org/licence/lal/en/" target="_blank">Free Art licence</a>. </div>
				<div id="wrapper">
					'''+results+'''
				</div>
			</body>
			</html>'''
		f.write(html.encode('utf8'))
		f.close()

		print('> html file written', file=sys.stderr)


	# *******************************************
	# PDF with wkhtmltopdf: html > pdf
	# *******************************************
	pdf_out = 'output/pdf/regular/'+filename+'.pdf'
	cmd = "wkhtmltopdf "+html_out+' '+pdf_out
	print('>>>', cmd, file=sys.stderr)
	os.system(cmd)

	print('> pdf generated', file=sys.stderr)
	print('> done', file=sys.stderr)

if filename:
	save(filename)