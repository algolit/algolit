#!/usr/bin/env python
from __future__ import print_function
import os, sys
import cgi
import cgitb; cgitb.enable()
from jinja2 import FileSystemLoader, Environment
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize, TreebankWordTokenizer
from nltk.util import ngrams
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

# *******************************************************
# settings
# *******************************************************
path = "./"
env = Environment(loader=FileSystemLoader(path)) 
template = env.get_template("cosine-similarity-gap_sentence-selection.html")

# *******************************************************
# read form fields into variables
# *******************************************************
method = os.environ.get("REQUEST_METHOD")
form = cgi.FieldStorage()
input_text = form.getvalue("input_text", "").decode('utf-8')
documentsize = form.getvalue("documentsize", "sentences")
features = form.getvalue("features", "")
stopwords = word_tokenize(form.getvalue("stopwords", "").decode('utf-8'))
countingmethod = form.getvalue("countingmethod", "")
lowercase = form.getvalue("lowercase", "")
matrix = form.getvalue("matrix", "")
input_vector_index = form.getvalue("input_vector_index", "None")
gap_text = form.getvalue("gap_text", "").decode('utf-8')
source = form.getvalue("source", "").decode('utf-8')
filename = form.getvalue("filename", "").decode('utf-8')
export_mode = form.getvalue("export_mode", "")
if input_vector_index != 'None':
	# -1 is important, the interface counting starts at '1', counting here starts at '0'
	input_vector_index = int(input_vector_index) - 1

# *******************************************************
# change variables (when form is submitted)
# *******************************************************

# **********************
# CORPUS
# **********************
corpus = input_text.replace('\n',' ')
corpus = sent_tokenize(corpus)

print('\n*corpus with document size "'+documentsize+'" created*', file=sys.stderr)

# **********************
# TF-IDF VECTORIZER
# **********************

def createfeatures(corpus):
	words = word_tokenize(corpus)
	for w in words:
		if 'word' in features:
			yield w
		if 'chargram2' in features:
			for i in range(len(w) - 2):
				yield w[i:i+2]
		if 'chargram3' in features:
			for i in range(len(w) - 3):
				yield w[i:i+3]
		if 'chargram4' in features:
			for i in range(len(w) - 4):
				yield w[i:i+4]
		if 'chargram5' in features:
			for i in range(len(w) - 5):
				yield w[i:i+5]
	if 'bigram' in features:
		bigrams = ngrams(words, 2)
		for bigram in bigrams:
			bigram = '_'.join(bigram)
			yield bigram
	if 'trigram' in features:
		trigrams = ngrams(words, 3)
		for trigram in trigrams:
			trigram = '_'.join(trigram)
			yield trigram
	if 'pos_bigr' in features:
		bigrams = ngrams(words, 2)
		for bigram in bigrams:
			pos = nltk.pos_tag(bigram)
			bigram_pos = '_'.join(tag for w, tag in pos)
			yield bigram_pos
	if 'pos_trigr' in features:
		trigrams = ngrams(words, 3)
		for trigram in trigrams:
			pos = nltk.pos_tag(trigram)
			trigram_pos = '_'.join(tag for w, tag in pos)
			yield trigram_pos
	if 'pos' in features:
		pos = nltk.pos_tag(words)
		for tag in pos:
			yield 'POS_'+tag[1]

if lowercase == 'True':
	c = True
	print('>lowercase: True', file=sys.stderr)
else:
	c = False
	print('>lowercase: False', file=sys.stderr)

if features:
	print('>features:', features, file=sys.stderr)

if stopwords:
	print('>filter:', stopwords, file=sys.stderr)

if corpus:
	tfidf_vectorizer = TfidfVectorizer(analyzer=createfeatures, stop_words=stopwords, min_df=1, max_df=1.0, lowercase=c, norm=None, tokenizer=TreebankWordTokenizer().tokenize)
	# TfidfVectorizer(analyzer=u'word', binary=False, decode_error=u'strict', dtype=<type 'numpy.int64'>, encoding=u'utf-8', input=u'content', lowercase=True, max_df=1.0, max_features=None, min_df=1, ngram_range=(1, 1), norm='l2', preprocessor=None, smooth_idf=True, stop_words=None, strip_accents=None, sublinear_tf=False, token_pattern=u'(?u)\\b\\w\\w+\\b', tokenizer=None, use_idf=True, vocabulary=None)

	tfidf_matrix = tfidf_vectorizer.fit_transform(corpus)
	tfidf_matrix_array = tfidf_matrix.toarray()

	matrix = []
	for vector in tfidf_matrix_array:
		v = []
		for val in vector:
			v.append(val)
		matrix.append(v)

def makeTwoDigitFloat(num):
	return '{0:.2f}'.format(float(num)) # make a 0.00 float
env.globals.update(makeTwoDigitFloat=makeTwoDigitFloat)

def getFeatureName(index):
	if matrix:
		return tfidf_vectorizer.get_feature_names()[index]
env.globals.update(getFeatureName=getFeatureName)

print('*matrix created*', file=sys.stderr)

# **********************
# COSINE SIMILARITY
# **********************
from sklearn.metrics.pairwise import cosine_similarity

# if there is an input sentence selected
if input_vector_index != 'None':
	print('>input index found:', input_vector_index, file=sys.stderr)
	input_vector = matrix[input_vector_index]
	input_vector_sentence = corpus[input_vector_index]
	print('>input sentence found:', input_vector_sentence, file=sys.stderr)

	# calculate the cosine similarities 
	# between the input vector and all the other vectors
	input_vector_similarities = cosine_similarity(input_vector, tfidf_matrix)

	if max(input_vector_similarities[0]) != 0:
		# transform numpy array to python list
		tmp_similarities = []
		for num in input_vector_similarities[0]:
			tmp_similarities.append(num)
		highest_cossim_value = max(n for n in tmp_similarities if n!=max(tmp_similarities))
		highest_cossim_value_utf = highest_cossim_value.astype(unicode)
		most_similar_vector_index = tmp_similarities.index(highest_cossim_value)
		most_similar_vector = matrix[most_similar_vector_index]
		most_similar_vector_sentence = corpus[most_similar_vector_index]
		print('>most similar index found:', most_similar_vector_index +1, file=sys.stderr)
		print('>most similar sentence found:', most_similar_vector_sentence, file=sys.stderr)

	print('*cosine similarity calculated*', file=sys.stderr)

	# ************************
	# similarity visualisation
	# ************************

	# create feature lists
	# ********************
	input_vector_html = []
	input_vector_values = []
	input_vector_features = []
	most_similar_sentence_vector_html = []
	most_similar_sentence_vector_values = []
	most_similar_sentence_vector_features = []
	for index, f in enumerate(input_vector):
		# do not include features with a value of '0'
		if f != 0:
			value = '{0:.2f}'.format(float(f))
			feature = tfidf_vectorizer.get_feature_names()[index]
			input_vector_values.append(value)
			input_vector_features.append(feature)
			input_vector_html.append('<div>'+value+' &nbsp; '+feature+'</div>')
	for index, f in enumerate(most_similar_vector):
		# do not include features with a value of '0'
		if f != 0:
			value = '{0:.2f}'.format(float(f))
			feature = tfidf_vectorizer.get_feature_names()[index]
			most_similar_sentence_vector_values.append(value)
			most_similar_sentence_vector_features.append(feature)
			most_similar_sentence_vector_html.append('<div>'+value+' &nbsp; '+feature+'</div>')

	# create the overlapping features list
	# ************************************
	similarity_features_html = []
	similarity_features = []
	for i, feature in enumerate(input_vector_features):
		if feature in most_similar_sentence_vector_features:
			index = most_similar_sentence_vector_features.index(feature)
			val_input = input_vector_values[i]
			val_similar = most_similar_sentence_vector_values[index]
			similarity_features.append([feature, val_input, val_similar])

			# exception for POS features: 
			# words are added to "overlapping features" list
			if 'pos' in features:
				input_sentence_words = [word for word in word_tokenize(input_vector_sentence)]
				most_similar_sentence_words = [word for word in word_tokenize(most_similar_vector_sentence)]
				pos_list_input = nltk.pos_tag(input_sentence_words)
				pos_list_most_similar = nltk.pos_tag(most_similar_sentence_words)
				feature = feature.replace("POS_","")
				pos_list = []
				for list in [pos_list_input, pos_list_most_similar]:
					for pos_item in list:
						word = pos_item[0]
						pos = pos_item[1]
						if pos == feature:
							html = '<span class="pos-feature-word">'+word+'</span>'
							pos_list.append(html)

				# replace POS tags with names, following the Penn Treebank (https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html)
				if feature == "CC":
					feature = "COORDINATING CONJUNCTION"
				if feature == "CD":
					feature = "CARDINAL NUMBER"
				if feature == "DT":
					feature = "DETERMINER"
				if feature == "EX":
					feature = "EXISTENTIAL THERE"
				if feature == "FW":
					feature = "FOREIGN WORD"
				if feature == "IN":
					feature = "PREPOSITION OR SUBORDINATING CONJUNCTION"
				if feature == "JJ":
					feature = "ADJECTIVE"
				if feature == "JJR":
					feature = "ADJECTIVE, COMPARATIVE"
				if feature == "JJS":
					feature = "ADJECTIVE, SUPERLATIVE"
				if feature == "LS":
					feature = "LIST ITEM MARKER"
				if feature == "MD":
					feature = "MODAL"
				if feature == "NN":
					feature = "NOUN, SINGULAR OR MASS"
				if feature == "NNS":
					feature = "NOUN, PLURAL"
				if feature == "NNP":
					feature = "PROPER NOUN, SINGULAR"
				if feature == "NNPS":
					feature = "PROPER NOUN, PLURAL"
				if feature == "PDT":
					feature = "PREDETERMINER"
				if feature == "POS":
					feature = "POSSESSIVE ENDING"
				if feature == "PRP":
					feature = "PERSONAL PRONOUN"
				if feature == "PRP$":
					feature = "POSSESSIVE PRONOUN"
				if feature == "RB":
					feature = "ADVERB"
				if feature == "RBR":
					feature = "ADVERB, COMPARATIVE"
				if feature == "RBS":
					feature = "ADVERB, SUPERLATIVE"
				if feature == "RP":
					feature = "PARTICLE"
				if feature == "SYM":
					feature = "SYMBOL"
				if feature == "TO":
					feature = "TO"
				if feature == "UH":
					feature = "INTERJECTION"
				if feature == "VB":
					feature = "VERB, BASE FORM"
				if feature == "VBD":
					feature = "VERB, PAST TENSE"
				if feature == "VBG":
					feature = "VERB, GERUND OR PRESENT PARTICIPLE"
				if feature == "VBN":
					feature = "VERB, PAST PARTICIPLE"
				if feature == "VBP":
					feature = "VERB, NON-3RD PERSON SINGULAR PRESENT"
				if feature == "VBZ":
					feature = "VERB, 3RD PERSON SINGULAR PRESENT"
				if feature == "WDT":
					feature = "WH-DETERMINER"
				if feature == "WP":
					feature = "WH-PRONOUN"
				if feature == "WP$":
					feature = "POSSESSIVE WH-PRONOUN"
				if feature == "WRB":
					feature = "WH-ADVERB"
				if feature == ".": # my addition to this list
					feature = "PUNCTUATION" # my addition to this list

				feature = feature+': '+' '.join(pos_list)
				similarity_features_html.append('<span><strong>'+feature+'</strong> ('+str(val_input)+', '+str(val_similar)+'); </span>')
			else:
				similarity_features_html.append('<span><strong>'+feature+'</strong> ('+str(val_input)+', '+str(val_similar)+'); </span>')

	# create similarity visualization with outlines
	# *********************************************
	if type(features) != list:
		features = str(features)
	else:
		features = ' '.join(features)

	# wrapping the features with a <span> in the sentences
	# which uses the feature TFIDF value as thickness of the outline offset
	sentence_in = input_vector_sentence
	sentence_out = most_similar_vector_sentence
	if 'pos' in features:
		for x, sentence in enumerate([sentence_in, sentence_out]):
			words = []
			tmp = []
			w = [word for word in word_tokenize(sentence)]
			pos_list = nltk.pos_tag(w)
			for pos_item in pos_list:
				done = False
				word = pos_item[0]
				pos = pos_item[1]
				for i, item in enumerate(similarity_features):
					feature = item[0].replace("POS_","")	
					if pos == feature:
						outline_intensity = 0.45 # to tweak the outline
						value_in = float(item[1]) * outline_intensity
						value_out = float(item[2]) * outline_intensity
						word = '<span class="highlight" style="text-shadow: -'+str(value_in)+'px -'+str(value_in)+'px 0 #000, '+str(value_in)+'px -'+str(value_in)+'px 0 #000, -'+str(value_in)+'px '+str(value_in)+'px 0 #000, '+str(value_in)+'px '+str(value_in)+'px 0 #000;">'+word+'</span>'
						words.append(word)
						done = True
						continue
					else:
						if i == len(similarity_features)-1:
							if done == False:
								words.append(word)
			if x == 0:
				sentence_in = ' '.join(words)
			if x == 1:
				sentence_out = ' '.join(words)
			print('>sentence_in:', sentence_in, file=sys.stderr)
			print('>sentence_out:', sentence_out, file=sys.stderr)
	else:
		for item in similarity_features:
			outline_intensity = 0.25 # to tweak the outline offset
			value_in = float(item[1]) * outline_intensity # value of feature in input sentence
			value_out = float(item[2]) * outline_intensity # value of feature in output sentence
			space_1 = False # space character on the left
			space_2 = False # space character on the right
			if 'chargram' in features:
				feature = item[0].replace("_","")
			if 'bigram' or 'trigram' in features:
				feature = item[0].replace("_"," ")
				if " '" in feature:
					feature = feature.replace(" '","'")
				if "," in feature:
					feature = feature.replace(" ,",",")
				if feature[-1] == '.' or '!' or '?':
					feature = feature.replace(" ","")
			if 'word' in features:
				feature = item[0]
				words_in = [w for w in word_tokenize(sentence_in)]
				words_out = [w for w in word_tokenize(sentence_in)]
				if feature == words_in[0]:
					feature = feature+' '
				elif feature == words_in[-2]:
					feature = ' '+feature
					space_1 = True
				elif feature == words_in[-1]:
					feature = feature
				elif feature == words_out[0]:
					feature = feature+' '
					space_2 = True
				elif feature == words_out[-2]:
					feature = ' '+feature
					space_1 = True
				else:
					feature = ' '+feature+' '
					space_1 = True
					space_2 = True

			def getHtml(feature, value):
				return '<span class="highlight" style="text-shadow: -'+str(value)+'px -'+str(value)+'px 0 #000, '+str(value)+'px -'+str(value)+'px 0 #000, -'+str(value)+'px '+str(value)+'px 0 #000, '+str(value)+'px '+str(value)+'px 0 #000;">'+feature+'</span>'

			# include or not include spaces in searching for features in sentences
			if space_1 == True:
				if space_2 == True:
					html = getHtml(feature, value_in)
					sentence_in = sentence_in.replace(feature,' '+html+' ')
					html = getHtml(feature, value_out)
					sentence_out = sentence_out.replace(feature,' '+html+' ')
				if space_2 == False:
					html = getHtml(feature, value_in)
					sentence_in = sentence_in.replace(feature,' '+html)
					html = getHtml(feature, value_out)
					sentence_out = sentence_out.replace(feature,' '+html)
			if space_2 == True:
				if space_1 == False:
					html = getHtml(feature, value_in)
					sentence_in = sentence_in.replace(feature, html+' ')
					html = getHtml(feature, value_out)
					sentence_out = sentence_out.replace(feature, html+' ')
			else:
				html = getHtml(feature, value_in)
				sentence_in = sentence_in.replace(feature, html)				
				html = getHtml(feature, value_out)
				sentence_out = sentence_out.replace(feature, html)
else:
	print('>no input index found:', input_vector_index, file=sys.stderr)


# *******************************************************
# save HTML + PDF
# *******************************************************
def save(filename, features, sentence_in, gap_text, sentence_out):
	folder = "output/html/"
	ext = ".html"
	htmlname = folder+filename+ext

	with open (htmlname,'w+') as f:
		html = '''
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8">
				<title>output vectorizer: Cosine Similarity Gap</title>
				<link rel="stylesheet" type="text/css" href="css/similarity-gap.css">
			</head>
			<body>
				<h1>Cosine Similarity Gap
				<br>(features: '''+features+''')</h1>
				<div id="wrapper">
					<div class="sentence"><h2>'''+sentence_in+'''</h2></div>
					<div class="gap_text">'''+gap_text+'''</div>
					<div class="sentence"><h2>'''+sentence_out+'''</h2></div>
					<br><br>
					<div class="info"><u>Input source</u>: '''+source+'''</div>
					<div class="info"><u>Sentence</u>: #'''+str(input_vector_index+1)+'''/'''+str(len(corpus))+'''</div>
					<div class="info"><u>Most similar sentence</u>: #'''+str(most_similar_vector_index + 1)+'''/'''+str(len(corpus))+'''</div> 
					<div class="info"><u>Similarity rate</u>: '''+highest_cossim_value_utf+'''</div> 
					<div class="info"><u>Overlapping features</u>: '''+' '.join(similarity_features_html)+'''</div>
					<div class="left info"><u>Input sentence features</u>: '''+' '.join(input_vector_html)+'''</div>
					<div class="right info"><u>Most similar sentence features</u>: '''+' '.join(most_similar_sentence_vector_html)+'''</div>
				</div>
			</body>
			</html>
		'''
		f.write(html.encode('utf8'))
		f.close()

	# *******************************************************
	# export pdfs with wkhtmltopdf: html > pdf
	# *******************************************************
	folder = "output/pdf/"
	ext = ".pdf"
	pdfname = folder+filename+ext
	cmd = "wkhtmltopdf "+htmlname+' '+pdfname
	print('>>> pdf generated', cmd, file=sys.stderr)
	os.system(cmd)

if filename:
	save(filename, features, sentence_in, gap_text, sentence_out)

# *******************************************************
# set jinja variables
# *******************************************************
tvars = {}
tvars['input_text'] = input_text
tvars['documentsize'] = documentsize
tvars['features'] = features
tvars['stopwords'] = ' '.join(stopwords)
tvars['countingmethod'] = countingmethod
tvars['corpus'] = corpus
tvars['lowercase'] = lowercase
tvars['matrix'] = matrix
if input_vector_index != 'None':
	tvars['input_vector_index'] = input_vector_index +1
else:
	tvars['input_vector_index'] = input_vector_index
tvars['source'] = source
if input_vector_index != 'None':
	tvars['input_vector'] = input_vector
	# tvars['input_vector_sentence'] = input_vector_sentence
	tvars['input_vector_html'] = input_vector_html
	tvars['most_similar_vector'] = most_similar_vector
	tvars['most_similar_vector_index'] = most_similar_vector_index +1
	# tvars['most_similar_vector_sentence'] = most_similar_vector_sentence
	tvars['most_similar_sentence_vector_html'] = most_similar_sentence_vector_html
	# tvars['similarity_features'] = similarity_features
	tvars['similarity_features_html'] = similarity_features_html
	tvars['sentence_in'] = sentence_in
	tvars['sentence_out'] = sentence_out
	tvars['cossim_value'] = highest_cossim_value_utf


# *******************************************************
# render
# *******************************************************
print('*variables rendered*\n', file=sys.stderr)

print("Content-type: text/html;charset=utf-8")
print()
html = template.render(tvars).encode('utf-8')
print(html)