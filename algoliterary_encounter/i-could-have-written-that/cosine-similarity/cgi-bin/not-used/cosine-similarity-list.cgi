#!/usr/bin/env python
from __future__ import print_function
import os, sys
import cgi
import cgitb; cgitb.enable()
from jinja2 import FileSystemLoader, Environment
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize, TreebankWordTokenizer
from nltk.util import ngrams
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

# *******************************************************
# settings
# *******************************************************
path = "./"
env = Environment(loader=FileSystemLoader(path)) 
template = env.get_template("cosine-similarity-list.html")

# *******************************************************
# read form fields into variables
# *******************************************************
method = os.environ.get("REQUEST_METHOD")
form = cgi.FieldStorage()
input_text = form.getvalue("input_text", "").decode('utf-8')
documentsize = form.getvalue("documentsize", "sentences")
features = form.getvalue("features", "")
stopwords = word_tokenize(form.getvalue("stopwords", "").decode('utf-8'))
countingmethod = form.getvalue("countingmethod", "")
lowercase = form.getvalue("lowercase", "")
matrix = form.getvalue("matrix", "")
input_vector_index = form.getvalue("input_vector_index", "None")
gap_text = form.getvalue("gap_text", "").decode('utf-8')
filename = form.getvalue("filename", "").decode('utf-8')
export_mode = form.getvalue("export_mode", "")
if input_vector_index != 'None':
	input_vector_index = int(input_vector_index)

# print('corpus:', corpus, file=sys.stderr)
# print('documentsize:', documentsize, file=sys.stderr)
# print('features:', features, file=sys.stderr)
# print('countingmethod:', countingmethod, file=sys.stderr)

# *******************************************************
# change variables (as soon as the form is submitted)
# *******************************************************

# **********************
# CORPUS
# **********************
corpus = input_text.replace('\n',' ')
corpus = sent_tokenize(corpus)

print('\n*corpus with document size "'+documentsize+'" created*', file=sys.stderr)

# **********************
# TF-IDF VECTORIZER
# **********************

def createfeatures(corpus):
	words = word_tokenize(corpus)
	for w in words:
		if 'word' in features:
			yield w
		if 'chargram2' in features:
			for i in range(len(w) - 2):
				yield w[i:i+2]
		if 'chargram3' in features:
			for i in range(len(w) - 3):
				yield w[i:i+3]
		if 'chargram4' in features:
			for i in range(len(w) - 4):
				yield w[i:i+4]
		if 'chargram5' in features:
			for i in range(len(w) - 5):
				yield w[i:i+5]
	if 'bigram' in features:
		bigrams = ngrams(words, 2)
		for bigram in bigrams:
			bigram = '_'.join(bigram)
			yield bigram
	if 'trigram' in features:
		trigrams = ngrams(words, 3)
		for trigram in trigrams:
			trigram = '_'.join(trigram)
			yield trigram
	if 'pos_bigr' in features:
		bigrams = ngrams(words, 2)
		for bigram in bigrams:
			pos = nltk.pos_tag(bigram)
			bigram_pos = '_'.join(tag for w, tag in pos)
			yield bigram_pos
	if 'pos_trigr' in features:
		trigrams = ngrams(words, 3)
		for trigram in trigrams:
			pos = nltk.pos_tag(trigram)
			trigram_pos = '_'.join(tag for w, tag in pos)
			yield trigram_pos
	if 'pos' in features:
		pos = nltk.pos_tag(words)
		for tag in pos:
			yield 'POS_'+tag[1]

if lowercase == 'True':
	c = True
	print('>lowercase: True', file=sys.stderr)
else:
	c = False
	print('>lowercase: False', file=sys.stderr)

if features:
	print('>features:', features, file=sys.stderr)

if stopwords:
	print('>filter:', stopwords, file=sys.stderr)

if corpus:
	tfidf_vectorizer = TfidfVectorizer(analyzer=createfeatures, stop_words=stopwords, min_df=1, max_df=1.0, lowercase=c, norm=None, tokenizer=TreebankWordTokenizer().tokenize)
	# TfidfVectorizer(analyzer=u'word', binary=False, decode_error=u'strict', dtype=<type 'numpy.int64'>, encoding=u'utf-8', input=u'content', lowercase=True, max_df=1.0, max_features=None, min_df=1, ngram_range=(1, 1), norm='l2', preprocessor=None, smooth_idf=True, stop_words=None, strip_accents=None, sublinear_tf=False, token_pattern=u'(?u)\\b\\w\\w+\\b', tokenizer=None, use_idf=True, vocabulary=None)

	tfidf_matrix = tfidf_vectorizer.fit_transform(corpus)
	tfidf_matrix_array = tfidf_matrix.toarray()

	matrix = []
	for vector in tfidf_matrix_array:
		v = []
		for val in vector:
			v.append(val)
		matrix.append(v)

def makeTwoDigitFloat(num):
	return '{0:.2f}'.format(float(num)) # make a 0.00 float
env.globals.update(makeTwoDigitFloat=makeTwoDigitFloat)

def getFeatureName(index):
	if matrix:
		return tfidf_vectorizer.get_feature_names()[index]
env.globals.update(getFeatureName=getFeatureName)

print('*matrix created*', file=sys.stderr)

# **********************
# COSINE SIMILARITY
# **********************
from sklearn.metrics.pairwise import cosine_similarity

if input_vector_index != 'None':
	input_vector = matrix[input_vector_index-1]
	input_sentence = corpus[input_vector_index-1]
	input_features = []
	input_feature_values = []
	for x, feature_value in enumerate(input_vector):
		if feature_value != 0:
			feature = tfidf_vectorizer.get_feature_names()[x]
			feature_value = '{0:.2f}'.format(float(feature_value))
			input_features.append(feature)
			input_feature_values.append(feature_value)

	print('>input index found:', input_vector_index, file=sys.stderr)
	print('>input sentence found:', input_sentence, file=sys.stderr)

	X = tfidf_matrix[input_vector_index-1]
	Y = tfidf_matrix
	# X = input sentence, Y = all vectors in matrix

	similarity_values_numpy = cosine_similarity(X, Y)
	# outputs a numpy list of similarity values, one value for each vector in matrix

	similarity_values = []
	for s in similarity_values_numpy[0]:
		similarity_values.append(s)
	# transform numpy list to python list

	# print('>>> X:', X, file=sys.stderr)
	# print('>>> Y:', Y, file=sys.stderr)
	print('>>> similarity_values:', similarity_values, file=sys.stderr)

	if max(similarity_values) != 0:
		sorted_similarity_values = sorted(similarity_values, reverse=True)
		print('>sorted_similarity_values:', sorted_similarity_values, file=sys.stderr)

		sorted_similarity = []
		# [rank, sentence, cossim_value, vector, [overlapping_features], index]
		# overlapping_features = [feature, overlapping_feature_value, input_feature_value]
		for i, val in enumerate(sorted_similarity_values):
			if i == 0:
				continue
			elif i == 10:
				break
			cossim_value = '{0:.2f}'.format(float(val))
			index = similarity_values.index(val)
			sentence = corpus[index]
			vector = matrix[index]
			overlapping_features = []
			for x, feature_value in enumerate(vector):
				if feature_value != 0:
					feature = tfidf_vectorizer.get_feature_names()[x]
					# print('>feature:', feature, file=sys.stderr)
					feature_value = '{0:.2f}'.format(float(feature_value))
					if feature in input_features:
						# print('>input_features:', input_features, file=sys.stderr)
						input_feature_index = input_features.index(feature)
						input_feature_value = '{0:.2f}'.format(float(input_feature_values[input_feature_index]))
						print('>input_feature_index:', input_feature_index, file=sys.stderr)
						print('>input_feature_value:', input_feature_value, file=sys.stderr)
						overlapping_features.append([feature, feature_value, input_feature_value])

			# print('>index:', index, file=sys.stderr)
			# print('>sentence:', sentence, file=sys.stderr)
			# print('>vector:', vector, file=sys.stderr)
			# print('>cossim_value:', cossim_value, file=sys.stderr)
			# print('>overlapping_features:', overlapping_features, file=sys.stderr)

			sorted_similarity.append([i, sentence, cossim_value, vector, overlapping_features, index + 1])

		# print('>sorted_similarity:', sorted_similarity, file=sys.stderr)

else:
	print('>no input index found:', input_vector_index, file=sys.stderr)

# *******************************************************
# save
# *******************************************************
def save(filename, features, input_sentence, sorted_similarity):
	folder = "output/html/"
	ext = ".html"
	htmlname = folder+filename+ext
	if type(features) != list:
		features = str(features)
	else:
		features = ' '.join(features)
	with open (htmlname,'w+') as f:
		html_top = '''
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8">
				<title>output vectorizer: Cosine Similarity List</title>
				<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
			</head>
			<body>
				<h1>Cosine Similarity List<br>(features: '''+features+''')</h1>
				<div <div id="result-list">
					<div class="sentence">'''+input_sentence+'''</div>
					<div class="info">#'''+str(input_vector_index)+'''/'''+str(len(corpus))+'''</div>
				</div>'''
		f.write(html_top.encode('utf8'))

		for s in sorted_similarity:
			sentence = s[1]
			cossim_value = s[2]
			overlapping_features = s[4]
			index = s[5]
			html = '''
			<div id="result-list" style="opacity:'''+str(cossim_value)+''';">
				<div class="sentence">'''+sentence+'''</div>
				<div class="info">#'''+str(index)+'''/'''+str(len(corpus))+'''</div>
				<div class="info"><u>Similarity rate</u>: '''+str(cossim_value)+'''</div>
				<div class="info"><u>Overlapping features</u>:'''
			f.write(html.encode('utf8'))
			for feature in overlapping_features:
				html = '''<strong>'''+feature[0]+'''</strong> ('''+feature[2]+''', '''+feature[1]+'''); '''
				f.write(html.encode('utf8'))
			html = '''</div>
			</div>
			'''
			f.write(html.encode('utf8'))

		html_bottom = '''
			</body>
			</html>'''
		f.write(html_bottom.encode('utf8'))
		f.close()

	# *******************************************************
	# export pdfs with wkhtmltopdf: html > pdf
	# *******************************************************
	folder = "output/pdf/"
	ext = ".pdf"
	pdfname = folder+filename+ext
	# cmd = "wkhtmltopdf "+htmlname+' '+pdfname
	cmd = "weasyprint "+htmlname+' '+pdfname+" -s output/html/css/stylesheet.css"
	print('>>> pdf generated', cmd, file=sys.stderr)
	os.system(cmd)

if filename:
	save(filename, features, input_sentence, sorted_similarity)

# *******************************************************
# set jinja variables
# *******************************************************
tvars = {}
tvars['input_text'] = input_text
tvars['documentsize'] = documentsize
tvars['features'] = features
tvars['stopwords'] = ' '.join(stopwords)
tvars['countingmethod'] = countingmethod
tvars['corpus'] = corpus
tvars['corpus_length'] = len(corpus)
tvars['lowercase'] = lowercase
tvars['matrix'] = matrix
tvars['input_vector_index'] = input_vector_index
if input_vector_index != 'None':
	tvars['input_vector'] = input_vector
	tvars['input_sentence'] = input_sentence
	tvars['sorted_similarity'] = sorted_similarity

# *******************************************************
# render
# *******************************************************
print('*variables rendered*\n', file=sys.stderr)

print("Content-type: text/html;charset=utf-8")
print()
html = template.render(tvars).encode('utf-8')
print(html)