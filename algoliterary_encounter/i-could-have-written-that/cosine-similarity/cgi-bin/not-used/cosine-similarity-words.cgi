#!/usr/bin/env python
from __future__ import print_function
import os, sys
import cgi
import cgitb; cgitb.enable()
from jinja2 import FileSystemLoader, Environment
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize, TreebankWordTokenizer
from nltk.util import ngrams
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

# *******************************************************
# settings
# *******************************************************
path = "./"
env = Environment(loader=FileSystemLoader(path)) 
template = env.get_template("cosine-similarity-words.html")

# *******************************************************
# read form fields into variables
# *******************************************************
method = os.environ.get("REQUEST_METHOD")
form = cgi.FieldStorage()
input_text = form.getvalue("input_text", "").decode('utf-8')
stopwords = word_tokenize(form.getvalue("stopwords", "").decode('utf-8'))
countingmethod = form.getvalue("countingmethod", "")
lowercase = form.getvalue("lowercase", "")
window_size = int(form.getvalue("window_size", "2"))
minimum_count = int(form.getvalue("minimum_count", "3"))
layer_size = int(form.getvalue("layer_size", "250"))
iterations = int(form.getvalue("iterations", "50"))
query = form.getvalue("query", "").decode('utf-8')
matrix = form.getvalue("matrix", "")
filename = form.getvalue("filename", "").decode('utf-8')

# *******************************************************
# change variables (as soon as the form is submitted)
# *******************************************************

corpus = input_text
lines = sent_tokenize(corpus)
words = []
for line in lines:
	w = word_tokenize(line)
	words.append(w)

if query != "":

	# ****************************
	# word2vec & GloVe
	# ****************************

	# based on the following tutorial:
	# http://textminingonline.com/getting-started-with-word2vec-and-glove-in-python
	# https://rare-technologies.com/word2vec-tutorial/
	# https://radimrehurek.com/gensim/models/word2vec.html

	# Glove:
	# https://nlp.stanford.edu/projects/glove/

	from gensim.models import word2vec
	from nltk.tokenize import word_tokenize, sent_tokenize

	import logging
	logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

	# ****************************
	# SETTINGS
	# ****************************
	window_size = window_size
	minimum_count = minimum_count
	layer_size = layer_size 

	# ****************************

	model = word2vec.Word2Vec(words, window=window_size, min_count=minimum_count, size=layer_size, workers=1)

	# window_size:
	# "window is the maximum distance between the current and predicted word within a sentence"

	# min_count: 
	# "Words that appear only once or twice in a billion-word corpus are probably uninteresting typos and garbage. In addition, there's not enough data to make any meaningful training on those words, so it's best to ignore them. A reasonable value for min_count is between 0-100, depending on the size of your dataset. "

	# layer_size: 
	# "Another parameter is the size of the NN layers, which correspond to the 'degrees' of freedom the training algorithm has. Bigger size values require more training data, but can lead to better (more accurate) models. Reasonable values are in the tens to hundreds."
	# >>> is this the number of features?

	# workers:
	# " The last of the major parameters (full list here) is for training parallelization, to speed up training. The workers parameter has only effect if you have Cython installed. Without Cython, you'll only be able to use one core because of the GIL (and word2vec training will be miserably slow)."

	# ****************************

	input_word = query
	queries = []
	queries_tmp = []


	value = 1 # a static starting value for the first query
	most_similar_word = ''
	iterations = iterations

	def nextQuery(results):
		for i, result in enumerate(results):
			if result[0] not in queries_tmp:
				return results[i]
				break

	import datetime
	timestamp = '{:%Y-%m-%d-%H-%M-%S}'.format(datetime.datetime.now())
	log_filename = 'log_'+query+'_'+timestamp

	for i in range(iterations):
		index = 0

		# find next unique query
		if query != input_word:
			if query in queries_tmp:
				result = nextQuery(results)
				if result != None:
					query = result[0]
					value = result[1]
				else:
					break	
						
		# store in queries list
		queries.append([query, value])
		queries_tmp.append(query)

		# find most similar list of words
		try:
			results = model.most_similar([query])
		except:
			break	

		most_similar = results[0]
		most_similar_word = str(most_similar[0])
		most_similar_value = most_similar[1]

		query = most_similar_word

# *******************************************************
# save
# *******************************************************
def save(filename, queries, window_size, minimum_count, layer_size, iterations):
	folder = "output/html/"
	ext = ".html"
	htmlname = folder+filename+ext
	with open (htmlname,'w+') as f:
		html_top = '''
			<!DOCTYPE html>
			<html>
			<head>
				<meta charset="utf-8">
				<title>output vectorizer: Recursive Similarity - words</title>
				<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
			</head>
			<body>
				<h1>Recursive Similarity - words<br>(using word2vec)</h1>
				<div <div id="result-list">
					<div class="info"><u>window size</u>: '''+str(window_size)+'''</div>
					<div class="info"><u>minimum count</u>: '''+str(minimum_count)+'''</div>
					<div class="info"><u>layer size</u>: '''+str(layer_size)+'''</div>
					<div class="info"><u>iterations</u>: '''+str(iterations)+'''</div>
				</div>'''
		f.write(html_top.encode('utf8'))

		for s in queries:
			word = s[0]
			sim_value = s[1]
			html = '''
			<div id="result-list" style="opacity:'''+str(value)+''';">
				<div class="word">'''+word+'''</div>
				<div class="info"><u>similarity rate</u>: '''+str(sim_value)+'''</div>'''
			f.write(html.encode('utf8'))

		html_bottom = '''
			</body>
			</html>'''
		f.write(html_bottom.encode('utf8'))
		f.close()

	# *******************************************************
	# export pdfs with wkhtmltopdf: html > pdf
	# *******************************************************
	folder = "output/pdf/"
	ext = ".pdf"
	pdfname = folder+filename+ext
	# cmd = "wkhtmltopdf "+htmlname+' '+pdfname
	cmd = "weasyprint "+htmlname+' '+pdfname+" -s output/html/css/stylesheet.css"
	print('>>> pdf generated', cmd, file=sys.stderr)
	os.system(cmd)

if filename:
	save(filename, queries, window_size, minimum_count, layer_size, iterations)

from collections import Counter
counter = Counter()
for word in words:
    counter.update(word)
# print('>>> common words', counter, file=sys.stderr)
common_words = []
for word, value in counter.most_common():
	common_words.append([word,value])

# *******************************************************
# set jinja variables
# *******************************************************
tvars = {}
tvars['input_text'] = input_text
tvars['window_size'] = str(window_size)
tvars['minimum_count'] = str(minimum_count)
tvars['layer_size'] = str(layer_size)
tvars['iterations'] = str(iterations)
tvars['stopwords'] = ' '.join(stopwords)
tvars['corpus'] = corpus
tvars['corpus_length'] = len(corpus)
tvars['lowercase'] = lowercase
tvars['matrix'] = matrix
tvars['common_words'] = common_words
if query: 
	tvars['queries'] = queries

# *******************************************************
# render
# *******************************************************
print('*variables rendered*\n', file=sys.stderr)

print("Content-type: text/html;charset=utf-8")
print()
html = template.render(tvars).encode('utf-8')
print(html)