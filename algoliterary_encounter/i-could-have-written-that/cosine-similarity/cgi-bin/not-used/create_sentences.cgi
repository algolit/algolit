#!/usr/bin/env python
from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
# from jinja2 import FileSystemLoader, Environment
import os, sys

# *******************************************************
# read input fields into variables
# *******************************************************
form = cgi.FieldStorage()
a = form.getvalue("a", "").decode('utf-8')
b = form.getvalue("b", "").decode('utf-8')
text_a = form.getvalue("text_a", "").decode('utf-8')
text_b = form.getvalue("text_b", "").decode('utf-8')

print('a:', a, file=sys.stderr)
print('b:', b, file=sys.stderr)
print('text_a:', text_a, file=sys.stderr)
print('text_b:', text_b, file=sys.stderr)

# *******************************************************
# change variables
# *******************************************************
out_a = []
out_b = []
out_ab = []
texts = [('a', text_a.split('\n')), ('b', text_b.split('\n'))]
for text in texts:
	for line in text[1]:
		if text[0] == 'a':
			if text_a:
				out_a.append(line)
		if text[0] == 'b':
			if text_b:
				out_b.append(line)


# *******************************************************
# set output variables
# *******************************************************
out = {}
out['a'] = a
out['b'] = b
out['out_a'] = out_a
out['out_b'] = out_b

print('Your data is returned to the interface through AJAX ...', file=sys.stderr)

# *******************************************************
# return json
# *******************************************************
import json

print ("Content-type: application/json")
print ()
print (json.dumps(out, indent=4).encode('utf-8'))