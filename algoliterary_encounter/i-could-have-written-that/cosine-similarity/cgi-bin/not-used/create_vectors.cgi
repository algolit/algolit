#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
import os, sys

# *******************************************************
# read input fields into variables
# *******************************************************
form = cgi.FieldStorage()
a = form.getvalue("a", "").decode('utf-8')
b = form.getvalue("b", "").decode('utf-8')
input_csv = form.getvalue("input_csv", "").decode('utf-8')
csv = form.getvalue("csv", "").decode('utf-8')

print('a:', a, file=sys.stderr)
print('b:', b, file=sys.stderr)
print('input_csv:', input_csv, file=sys.stderr)
print('csv:', csv, file=sys.stderr)

# *******************************************************
# change variables
# *******************************************************

from pattern.vector import Document, count, Model, Vector, TF, TFIDF, BINARY, IG
from pattern.vector import NB, MAJORITY, MULTINOMIAL, BINOMIAL, KNN, EUCLIDEAN, SVM, CLASSIFICATION, LINEAR, RADIAL, kfoldcv, gridsearch
from pattern.vector import chngrams
from pattern.db import csv
from random import shuffle
from pattern.en import parsetree, ngrams
 
def seed():
	"""
	Returns the seed to be used in a random function
	"""
	return 0.47231099848

## TRAIN + TEST
# declare file
data = csv('output/'+input_csv)
# data = csv # >>> to use the form input

# generate list with tuples from csv 
tuples = [(sentence, c) for sentence, c in data]
print("length dataset:", len(tuples), file=sys.stderr)

print("first tuples:", tuples[:5], file=sys.stderr)
shuffle(tuples, seed)
print("shuffled tuples:", tuples[:5], file=sys.stderr)

## ================================
## Feature Extraction
#### generate features for each sentence

## ALL WORDS:
# Document creëert een object met ID en tag: Document(id='QBJExYq-1', type=u'female')
# op basis van frequentie van woorden
sentence_vectors = [Document(sentence, type=c, stopwords=True) for sentence, c in tuples] 

vectors_a = []
vectors_b = []
for document in sentence_vectors:
	print("vector:", document.vector(), file=sys.stderr)
	print("document.type:", document.type, file=sys.stderr)
	if document.type == ' a':
		vectors_a.append(document.vector())
	else:
		vectors_b.append(document.vector())

# *******************************************************
# set output variables
# *******************************************************
out = {}
out['a'] = a
out['b'] = b
out['vectors_a'] = vectors_a
out['vectors_b'] = vectors_b

print('Your data is returned to the interface through AJAX ...', file=sys.stderr)

# *******************************************************
# return json
# *******************************************************
import json

print ("Content-type: application/json")
print ()
print (json.dumps(out, indent=4).encode('utf-8'))