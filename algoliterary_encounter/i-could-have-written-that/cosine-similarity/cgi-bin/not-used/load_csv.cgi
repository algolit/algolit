#!/usr/bin/env python
from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
import os, sys

# *******************************************************
# read input fields into variables
# *******************************************************
form = cgi.FieldStorage()
a = form.getvalue("a", "").decode('utf-8')
b = form.getvalue("b", "").decode('utf-8')

input_csv = form.getvalue("input_csv", "")
csv = open('output/'+input_csv,'r').read().decode('utf-8')

print('a:', a, file=sys.stderr)
print('b:', b, file=sys.stderr)
print('input_csv:', input_csv, file=sys.stderr)
print('csv:', csv, file=sys.stderr)

# *******************************************************
# change variables
# *******************************************************


# *******************************************************
# set output variables
# *******************************************************
out = {}
out['a'] = a
out['b'] = b
out['csv'] = csv

print('Your data is returned to the interface through AJAX ...', file=sys.stderr)

# *******************************************************
# return json
# *******************************************************
import json

print ("Content-type: application/json")
print ()
print (json.dumps(out, indent=4).encode('utf-8'))