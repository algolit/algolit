#!/usr/bin/env python
from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
# from jinja2 import FileSystemLoader, Environment
import os, sys
sys.path.append(os.path.relpath("./modules"))
from collect_tweets import *

# *******************************************************
# read input fields into variables
# *******************************************************
form = cgi.FieldStorage()
a = form.getvalue("a", "").decode('utf-8')
b = form.getvalue("b", "").decode('utf-8')
num = form.getvalue("num", "")
lang = form.getvalue("lang", "en")
tweets_a = form.getvalue("tweets_a", "")
tweets_b = form.getvalue("tweets_b", "")

print('a:', a, file=sys.stderr)
print('b:', b, file=sys.stderr)
print('num:', num, file=sys.stderr)
print('lang:', lang, file=sys.stderr)

# *******************************************************
# change variables (as soon as the form is submitted)
# *******************************************************
oppositions = [('a', a), ('b', b)]

for position in oppositions:
	tmp = []
	tweets = []
	print('Requesting tweets from the Twitter Streamer API for "'+position[1]+'"...', file=sys.stderr)
	filename = requestTweets(position[1], int(num), lang)
	print('Tweets are saved to '+filename, file=sys.stderr)

	data = openTweetsJson('/home/mb/'+filename)
	print('The file '+filename+' is openend again ...', file=sys.stderr)

	for item in data:
		tmp.append('<div class="tweet '+position[0]+'">')
		tmp.append('<img src="'+item['user']['profile_image_url']+'">')
		if item['user']['screen_name']:
			tmp.append('by <span>'+item['user']['screen_name']+'</span>')
		if item['user']['favourites_count']:
			tmp.append(' (with a favourites_count of <span>'+str(item['user']['favourites_count'])+'</span>)')
		if item['user']['location']:
			tmp.append(' from <span>'+item['user']['location']+'</span>')
		if item['user']['created_at']:
			tmp.append(' at <span>'+item['user']['created_at']+'</span>')
		if item['source']:
			tmp.append(' using <span>'+item['source']+'</span>')
		tmp.append('<div class="text">'+item['text']+'</div><br>')
		tmp.append('</div>')
		tweets.append(item['text'])
	if position[0] == 'a':
		newtweets_a = '\n'.join(tmp)
		tweets_a = tweets
	else:
		newtweets_b = '\n'.join(tmp)
		tweets_b = tweets

# *******************************************************
# set output variables
# *******************************************************
out = {}
out['a'] = a
out['b'] = b
out['num'] = num
out['lang'] = lang
out['tweets_a'] = tweets_a
out['tweets_b'] = tweets_b

print('Tweets are returned in JSON format to the interface using AJAX ...', file=sys.stderr)

# *******************************************************
# return json
# *******************************************************
import json

print ("Content-type: application/json")
print ()
print (json.dumps(out, indent=4).encode('utf-8'))