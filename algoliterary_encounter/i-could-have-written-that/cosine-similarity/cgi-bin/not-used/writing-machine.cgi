#!/usr/bin/env python
from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
from jinja2 import FileSystemLoader, Environment
import os, sys, datetime

# *******************************************************
# settings
# *******************************************************
path = "./"
env = Environment(loader=FileSystemLoader(path)) 
template = env.get_template("interface.html")

# *******************************************************
# read form fields into variables
# *******************************************************
method = os.environ.get("REQUEST_METHOD")
form = cgi.FieldStorage()
a = form.getvalue("a", "").decode('utf-8')
b = form.getvalue("b", "").decode('utf-8')
text_a = form.getvalue("text_a", "").decode('utf-8')
text_b = form.getvalue("text_b", "").decode('utf-8')
csv_a = form.getvalue("csv_a", "").decode('utf-8')
csv_b = form.getvalue("csv_b", "").decode('utf-8')

print('a:', a, file=sys.stderr)
print('b:', b, file=sys.stderr)

# *******************************************************
# change variables (as soon as the form is submitted)
# *******************************************************
fmt = '%Y.%m.%d-%H%M%S' 
timestamp = datetime.datetime.now().strftime(fmt)

if csv_a:
	with open('output/'+timestamp+'_'+a+'-'+b+'.csv','w+') as f:
		f.write(csv_a);
		f.write(csv_b);
		f.close();
		print('*output is written to output/'+timestamp+'_'+a+'-'+b+'.csv*', file=sys.stderr)

# *******************************************************
# set jinja variables
# *******************************************************
tvars = {}
tvars['a'] = a
tvars['b'] = b
tvars['text_a'] = text_a
tvars['text_b'] = text_b
tvars['csv_a'] = csv_a
tvars['csv_b'] = csv_b
tvars['saved_csv'] = 'output/'+timestamp+'_'+a+'-'+b+'.csv'

# *******************************************************
# render
# *******************************************************
print('Variables are rendered using Jinja ...', file=sys.stderr)

print("Content-type: text/html;charset=utf-8")
print()
html = template.render(tvars).encode('utf-8')
print(html)