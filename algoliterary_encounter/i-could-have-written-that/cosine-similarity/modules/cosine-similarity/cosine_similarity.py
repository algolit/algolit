#  from: http://scikit-learn.org/stable/modules/feature_extraction.html#common-vectorizer-usage

from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
from nltk.tokenize import word_tokenize, sent_tokenize
np.set_printoptions(threshold=np.inf) # allows to print full arrays / prevents printing truncated representations of arrays 

# filename = 'cyborg-manifesto'
# filename = 'goedelescherbach'
filename = 'test'
# filename = 'weizenbaum_fullchapter'
corpus = open('input/'+filename+'.txt','r').read().decode('utf-8')
corpus = sent_tokenize(corpus)
# print corpus
# print len(corpus) # prints number of sentences


# **********************
# TF-IDF
# **********************
from sklearn.feature_extraction.text import TfidfVectorizer

tfidf_vectorizer = TfidfVectorizer(analyzer=u'word', stop_words=None, min_df=1, max_df=1.0, lowercase=False, norm=None, tokenizer=None)
	# TfidfVectorizer(analyzer=u'word', binary=False, decode_error=u'strict', dtype=<type 'numpy.int64'>, encoding=u'utf-8', input=u'content', lowercase=True, max_df=1.0, max_features=None, min_df=1, ngram_range=(1, 1), norm='l2', preprocessor=None, smooth_idf=True, stop_words=None, strip_accents=None, sublinear_tf=False, token_pattern=u'(?u)\\b\\w\\w+\\b', tokenizer=None, use_idf=True, vocabulary=None)

tfidf_matrix = tfidf_vectorizer.fit_transform(corpus)
print tfidf 
# (0, 114)	0.175690133779
# (0, 111)	0.175690133779
# (0, 8)	0.101721966517

# print tfidf_matrix.shape
# (9, 130) > 9 sentences, 130 unique features (tokens in this case)

tfidf_matrix_array = tfidf_matrix.toarray()

# **********************
# COSINE SIMILARITY
# **********************
from sklearn.metrics.pairwise import cosine_similarity

# the Cosine Similarity between the first document with each of the other documents of the set
vector = tfidf_matrix[0]
# print cosine_similarity(vector, tfidf_matrix)
# [[ 1.  0.48508533  0.31469909  0.22362743  0.14357789  0.09211256  0.27894438  0.03478676  0.27369404]]


# **********************
# OUTPUT > HTML
# **********************
write = False

if write == True:
	def printVector(vector):
		html = ''
		html+="<div class='vector'>"
		for y, column in enumerate(vector):
			feature_name = tfidf_vectorizer.get_feature_names()[y]
			feature_value = column
			utf_value = feature_value.astype(unicode)
			utf_value = '{0:.2f}'.format(float(utf_value)) # make a 0.00 float
			html += "<span class='feature'><span class='fname'>"+feature_name+"</span><span class='fval'>"+utf_value+"</span></span>"
		html += "</div>"
		return html

	fileout = open('output/cosine_similarity_'+filename+'.html','w+')
	fileout.write('<link rel="stylesheet" type="text/css" href="../stylesheet.css">')
	fileout.write("<h1>Cosine Similarity</h1>")

	vectors = tfidf_matrix[0:len(corpus)]
	similarities = cosine_similarity(vectors, tfidf_matrix)
	notlisted = []
	for i, list in enumerate(similarities):
		# print list
		if max(list) != 0:
			tmp_vector = []
			for num in list:
				tmp_vector.append(num)
			current = corpus[i]
			current_vector_html = printVector(tfidf_matrix_array[i])
			cossim = max(n for n in list if n!=max(list))
			cossim_utf = cossim.astype(unicode)
			index = tmp_vector.index(cossim)
			simsentence = corpus[index]
			sim_vector_html = printVector(tfidf_matrix_array[index])
			print '***'
			print '> current sentence:\n', current
			print 'vector:', tfidf_matrix_array[i]
			print '> most similar sentence:\n', simsentence
			print '> highest cosine similarity:', cossim_utf
			print 'vector:', tfidf_matrix_array[index]

			html = '''<div id="wrapper">
			<hr>
			<div><b>sentence</b>: '''+current+'''</div>
			<div><b>most similar</b>: '''+simsentence+'''</div>
			<div><b>cosine similarity</b>: '''+cossim_utf+'''</div>
			'''+current_vector_html+'''
			'''+sim_vector_html+'''
			</div>
			'''
			fileout.write(html.encode('utf-8'))
		else:
			notlisted.append(corpus[i])

	if len(notlisted) != 0:
		fileout.write('<hr><h2>not listed sentences (empty cosine similarity vectors)</h2><div>'+' '.join(notlisted)+'</div>')
	else:
		fileout.write('<hr><h2>not listed sentences (empty cosine similarity vectors)</h2><div>None</div>')
	fileout.close()
	print '\n*output written*\n'


