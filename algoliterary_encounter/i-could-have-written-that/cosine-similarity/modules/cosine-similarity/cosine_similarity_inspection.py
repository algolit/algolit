# *******************************
# SCIKIT LEARN
# *******************************
#  from: http://scikit-learn.org/stable/modules/feature_extraction.html#common-vectorizer-usage

from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
from nltk.tokenize import word_tokenize, sent_tokenize
np.set_printoptions(threshold=np.inf) # allows to print full arrays / prevents printing truncated representations of arrays 

# filename = 'cyborg-manifesto'
# filename = 'goedelescherbach'
filename = 'test'
# filename = 'weizenbaum_fullchapter'
corpus = open('input/'+filename+'.txt','r').read().decode('utf-8')
corpus = sent_tokenize(corpus)
# a machine runs.
# we run too.
# the machine executes.
# another machine too.

# **********************
# TF-IDF MATRIX
# **********************
from sklearn.feature_extraction.text import TfidfVectorizer

tfidf_vectorizer = TfidfVectorizer(analyzer=u'word', stop_words=None, min_df=1, max_df=1.0, lowercase=False, norm=None, tokenizer=None)
	# TfidfVectorizer(analyzer=u'word', binary=False, decode_error=u'strict', dtype=<type 'numpy.int64'>, encoding=u'utf-8', input=u'content', lowercase=True, max_df=1.0, max_features=None, min_df=1, ngram_range=(1, 1), norm='l2', preprocessor=None, smooth_idf=True, stop_words=None, strip_accents=None, sublinear_tf=False, token_pattern=u'(?u)\\b\\w\\w+\\b', tokenizer=None, use_idf=True, vocabulary=None)

tfidf_matrix = tfidf_vectorizer.fit_transform(corpus)
print tfidf_matrix 
# (0, 2)	1.22314355131
# (0, 4)	1.91629073187
# (1, 7)	1.91629073187
# (1, 3)	1.91629073187
# (1, 6)	1.51082562377
# (2, 2)	1.22314355131
# (2, 5)	1.91629073187
# (2, 1)	1.91629073187
# (3, 2)	1.22314355131
# (3, 6)	1.51082562377
# (3, 0)	1.91629073187

print tfidf_matrix.shape
# (4, 8) > 4 sentences, 8 unique features

tfidf_matrix_array = tfidf_matrix.toarray()
print tfidf_matrix_array
#[[ 0.          0.          1.22314355  0.          1.91629073  0.     	0. 			0.        ]
# [ 0.          0.          0.          1.91629073  0.          0. 		1.51082562  1.91629073]
# [ 0.          1.91629073  1.22314355  0.          0.          1.91629073 0.          0.     ]
# [ 1.91629073  0.          1.22314355  0.          0.          0. 		1.51082562  0.        ]]

# This vector has 8 dimensions.

feature_names = []
for i in range(7):
	feature_name = tfidf_vectorizer.get_feature_names()[i]
	feature_names.append(feature_name)
print feature_names

# **********************
# COSINE SIMILARITY
# **********************
from sklearn.metrics.pairwise import cosine_similarity

# the Cosine Similarity between the first document with each of the other documents of the set

vector = tfidf_matrix[0]
print cosine_similarity(vector, tfidf_matrix)
#[[ 1.          0.          0.22133323  0.24109064]]


# *******************************
# using python to study math
# *******************************
# source: https://stackoverflow.com/questions/18424228/cosine-similarity-between-2-number-lists

import math
def cosine_similarity(A,B):
	"compute cosine similarity of A to B: (A dot B)/{||A||*||B||)"
	sumxx, sumxy, sumyy = 0, 0, 0
	for i in range(len(A)):
		x = A[i]
		y = B[i]
		sumxx += x*x
		sumyy += y*y
		sumxy += x*y
	print 'sumxy:', sumxy
	return sumxy/math.sqrt(sumxx*sumyy)

A = [0,  0, 1.22314355, 0, 1.91629073, 0, 0, 0]			# "a machine runs."
B = [0, 1.91629073, 1.22314355, 0, 0, 1.91629073, 0, 0]	# "the machine executes."
print cosine_similarity(A, B)

# ---------
# INSPECTOR
# ---------

# cosine_similarity function: (A dot B)/{||A||*||B||)

# corpus:
	# a machine runs.
	# we run too.
	# the machine executes.
	# another machine too.

# 4 sentences, 8 unique features

# features: 
	# another
	# executes
	# machine
	# run
	# runs
	# the
	# too

# somehow dismissed features by the tfidf_vectorizer: 
	# 	a
	# 	we
	# 	.

# A = "a machine runs."
# B = "the machine executes."

# 		another		executes	machine		 run 	runs			the 		too		?
# A = [ 0,  		0, 			1.22314355,	 0, 	1.91629073, 	0, 			0, 		0 ]
# B = [ 0, 			1.91629073, 1.22314355,	 0, 	0, 				1.91629073, 0, 		0 ]

# inner dot product of vector A and B (the multiplication of two vectors):
# A1*B1 + A2*B2  + A3*B3 	 + A4*B4 + A5*B5  + A6*B6  + A7*B7 + A8*B8 = 
# 0*0	+ 0*1.91 + 1.22*1.22 + 0*0   + 1.91*0 + 0*1.91 + 0*0   + 0*0   =
# 0	 	+ 0		 + 1.4960801 + 0	 + 0	  + 0 	   + 0	   + 0	   = 1.496

# divided by

# sqaure root of 
# 	(
# 		(the sum of 
# 			(each value in vector A)^2)
# 		* 
# 		(the sum of 
# 			(each value in vector B)^2)
# 		)
# 	)

# the sum of (each value in vector A)^2: 
# A1^2 + A2^2 + A3^2   + A4^2 + A5^2   + A6^2 + A7^2 + A8^2 = 
# 0^2  + 0^2  + 1.22^2 + 0^2  + 1.91^2 + 0^2  + 0^2  + 0^2  = 
# 0	   + 0	  + 1.496  + 0    + 3.672  + 0    + 0    + 0    =  5.141

# the sum of (each value in vector B)^2: 
# B1^2 + B2^2   + B3^2   + B4^2 + B5^2 + B6^2   + B7^2 + B8^2 = 
# 0^2  + 1.91^2 + 1.22^2 + 0^2  + 0^2  + 1.91^2 + 0^2  + 0^2  =
# 0    + 3.672  + 1.496  + 0    + 0    + 3.672  + 0    + 0    = 8.84

# 5.141 * 8.84 = 45.446

# square root of 45.446 = 6.741

# cosine similarity(A,B) = 1.496 / 6.741
# cosine similarity(A,B) = 0.221

# Mmm, because A & B have one word in common, "machine", the first number is positive. If there would be no overlap, this number would be zero. Which would always return in a final result of zero as well, as zero divided by anything results in zero. An overlap of at least one word is crucial. 
# Oke, there needs to be an overlap of at least one word to have a positive end-result. Well, that makes sense if we speak of a similarity calculation i guess. 
# The only overlap we run into is the word "machine". Which has a value of 1.223 in both sentences, caused by the fact that both sentences have the same length and contain only once the word "machine". The inner product of these two sentences is calculated by multiplying 1.223 ("machine" in sentence A) with 1.223 ("machine" in sentence B) and results in 1.496.
# But then, the second number is calculated. This number is based on all the positive values in the vectors. These are the relative counting values of all the words in sentence A. This means that not only the overlapping words play a role in the similarity calculation, also all the other words of sentence A contribute to the final similarity result. These words don't have a direct connection to sentence B. 
# In our case, sentence A and B have the same length. But does this mean that a longer sentence includes more words/values to the calculation? Yes right?
# The second number is calculated by taking the square of each value in vector A, after which they are summed up. The same is done for vector B. By multiplying the sum of vector A^2 (5.141) with the sum of vector B^2 (8.84), an even bigger number is created (45.446). 
# This is the number of which a sqaure root is taken. Funny to see how first all the numbers are sqaured, and then square rooted again. As if you let something grow, and then divide its growth by its growing curve? Bad example, but anyway. The square root of 45.446 = 6.741.
# The first number, the inner dot product, is now ready to be divided by the square rooted second number. Resulting in 1.49/6.74 = 0.221, our similarity rate. (so abstract)!

