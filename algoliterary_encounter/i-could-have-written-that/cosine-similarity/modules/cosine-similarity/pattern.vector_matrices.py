from pattern.vector import count, words, Document, Vector, distance
from pattern.en import parse, Sentence
from nltk.tokenize import word_tokenize, sent_tokenize

corpus = open('weizenbaum_lines.txt','r').readlines()


# **********************
# TF
# **********************
matrix = []
for line in corpus:
	line = line.decode('utf-8')

	w = words(line)
	tf = count(w, stopwords=True)
	# print line
	# print tf

	d = Document(tf, stopwords=True)
	# document = Document(string, 
		#       filter = lambda w: w.lstrip("'").isalnum(), 
		#  punctuation = '.,;:!?()[]{}\'`"@#$*+-|=~_', 
		#          top = None,       # Filter words not in the top most frequent.
		#    threshold = 0,          # Filter words whose count falls below threshold.
		#      exclude = [],         # Filter words in the exclude list. 
		#      stemmer = None,       # STEMMER | LEMMA | function | None.
		#    stopwords = False,      # Include stop words?
		#         name = None,
		#         type = None,
		#     language = None, 
		#  description = None)

	vector = Vector(d.words)
	# print vector
	matrix.append(vector)

v1 = matrix[0]
v2 = matrix[1]
v3 = matrix[2]
print 1 - distance(v1,v2)
print 1 - distance(v2,v3)
print 1 - distance(v1,v3)


# ----------------------
# OUTPUT > HTML
# ----------------------
baseline = 10 # baseline fontsize in pt
stepsize = 5 # stepsize in pt
lowercase = True
outputvalues = True

if outputvalues == True:
	fileout = open('output-pattern.vector_fontsize-tf_val.html','w+')
else:
	fileout = open('output-pattern.vector_fontsize-tf.html','w+')
# fileout = open('output-fontsize-tf-fullchapter.html','w+')
fileout.write('<link rel="stylesheet" type="text/css" href="stylesheet.css">')
fileout.write("<h1>Pattern<br>Counting - Term Frequency</h1>")
fileout.write("<div id='wrapper' style='position:relative;'>")

notlistedwords = []
for i, line in enumerate(corpus):
	# print line
	vector = matrix[i]
	# print vector
	# for word in words(line):
	for word in word_tokenize(line): #using nltk
		processed = False
		if lowercase == True:
			word = word.lower()
		for x, (key, value) in enumerate(vector.items()):
			if word == key:
				val = value
				# print word, key, val
				processed = True
		if processed == False:
			fontsize = baseline
			notlistedwords.append(word)
			html = "<span class='feature notlisted' style='display:inline-block;font-size:"+str(fontsize)+"pt;opacity:0.5;'>"+word+"</span> "
			fileout.write(html.encode('utf8'))
		else:
			fontsize = baseline + (val * stepsize)
			if val == 0:
				fontsize = baseline
			if outputvalues == True:
				html = "<span class='feature' style='display:inline-block;font-size:"+str(fontsize)+"pt;'>"+word+"<sup>("+str(val)+")</sup></span> "
			else:
				html = "<span class='feature' style='display:inline-block;font-size:"+str(fontsize)+"pt;'>"+word+"</span> "
			fileout.write(html.encode('utf8'))
fileout.write("</div>")

fileout.write("<hr><div>")
fileout.write("<h1>Features that are not listed in the vector:</h1>")
for word in notlistedwords:
	html = "<span style='opacity:0.5;'>"+word+"</span> "
	fileout.write(html.encode('utf8'))
fileout.write("</div>")

print '*output TF written to file*'
fileout.close()	
