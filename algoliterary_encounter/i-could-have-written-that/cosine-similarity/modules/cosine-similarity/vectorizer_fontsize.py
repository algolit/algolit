#  from: http://scikit-learn.org/stable/modules/feature_extraction.html#common-vectorizer-usage
from sklearn.feature_extraction.text import CountVectorizer
from nltk.tokenize import word_tokenize, sent_tokenize
import numpy as np
np.set_printoptions(threshold=np.inf) # allows to print full arrays / prevents printing truncated representations of arrays 

def notlisted(corpus, vectorizer):
	list = []
	for line in corpus:
		for word in word_tokenize(line):
			features = vectorizer.get_feature_names()
			try:
				index = features.index(word)
				# print index
			except ValueError:
				# print word, 'not in the list'
				list.append(word)
	return list

# **********************
# CORPUS
# **********************
plain = open('weizenbaum.txt','r').read().decode('utf-8')
# corpus = open('weizenbaum_fullchapter.txt','r').read().decode('utf-8')
corpus = sent_tokenize(plain)
# print corpus
# print len(corpus) # prints number of sentences


# **********************
# TF
# **********************
vectorizer = CountVectorizer(min_df=1, lowercase=False, strip_accents='unicode')
# docs: http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html#sklearn.feature_extraction.text.CountVectorizer

# print vectorizer # prints default CountVectorizer settings
# CountVectorizer(analyzer='word', binary=False, decode_error='strict', dtype=<'numpy.int64'>, encoding='utf-8', input='content', lowercase=True, max_df=1.0, max_features=None, min_df=1,  ngram_range=(1, 1), preprocessor=None, stop_words=None, strip_accents=None, tokenizer=None, vocabulary=None)

tf = vectorizer.fit_transform(corpus)
# print X # prints scipy.sparse matrix: a matrix, in sparse (= without 0's) dictionary notation
# (row, column), value = (sentence, feature), value
  # (0, 91)		1
  # (0, 125)	1
  # (0, 118)	1
  # ... 

analyze = vectorizer.build_analyzer()
# print analyze(corpus[0]) # prints features in first line of corpus

matrix = tf.toarray()
# print matrix # prints matrix
# print matrix[0] # print the vector of the first line

print vectorizer.get_feature_names() # prints unique features
# print vectorizer.vocabulary_.get('things') # print column-index > 112
# print vectorizer.get_feature_names()[112] # prints feature-name of column-index > 'things'

# ----------------------
# OUTPUT > HTML
# ----------------------
baseline = 10 # baseline fontsize in pt
stepsize = 5 # stepsize in pt
lowercase = False
outputvalues = False

if outputvalues == True:
	fileout = open('output-fontsize-tf_val.html','w+')
else:
	fileout = open('output-fontsize-tf.html','w+')
# fileout = open('output-fontsize-tf-fullchapter.html','w+')
fileout.write('<link rel="stylesheet" type="text/css" href="stylesheet.css">')
fileout.write("<h1>SciKit-Learn<br>Counting - Term Frequency</h1>")
fileout.write("<div id='wrapper' style='position:relative;'>")

notlistedwords = []
for i, line in enumerate(corpus):
	# print line
	for word in word_tokenize(line):
		if lowercase == True:
			word = word.lower()
		index = vectorizer.vocabulary_.get(word)
		vector = matrix[i]
		val = vector[index]
		# print word, index, vector, val
		if index == None:
			fontsize = baseline
			notlistedwords.append(word)
			html = "<span class='feature notlisted' style='display:inline-block;font-size:"+str(fontsize)+"pt;opacity:0.5;'>"+word+"</span> "
			fileout.write(html.encode('utf8'))
		else:
			fontsize = baseline + (val * stepsize)
			if val == 0:
				fontsize = baseline
			if outputvalues == True:
				html = "<span class='feature' style='display:inline-block;font-size:"+str(fontsize)+"pt;'>"+word+"<sup>("+str(val)+")</sup></span> "
			else:
				html = "<span class='feature' style='display:inline-block;font-size:"+str(fontsize)+"pt;'>"+word+"</span> "
			fileout.write(html.encode('utf8'))
fileout.write("</div>")

fileout.write("<hr><div>")
fileout.write("<h1>Features that are not listed in the vector:</h1>")
for word in notlistedwords:
	html = "<span style='opacity:0.5;'>"+word+"</span> "
	fileout.write(html.encode('utf8'))
fileout.write("</div>")

print '*output TF written to file*'
fileout.close()	


# **********************
# TF-IDF
# **********************
from sklearn.feature_extraction.text import TfidfVectorizer

vectorizer = TfidfVectorizer(min_df=1, lowercase=False, strip_accents='unicode')
# docs: http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html#sklearn.feature_extraction.text.TfidfVectorizer

# print vectorizer   
# TfidfVectorizer(analyzer=u'word', binary=False, decode_error=u'strict', dtype=<type 'numpy.int64'>, encoding=u'utf-8', input=u'content', lowercase=True, max_df=1.0, max_features=None, min_df=1, ngram_range=(1, 1), norm='l2', preprocessor=None, smooth_idf=True, stop_words=None, strip_accents=None, sublinear_tf=False, token_pattern=u'(?u)\\b\\w\\w+\\b', tokenizer=None, use_idf=True, vocabulary=None)

tfidf = vectorizer.fit_transform(corpus)
# print tfidf 
# (0, 114)	0.175690133779
# (0, 111)	0.175690133779
# (0, 8)	0.101721966517

matrix = tfidf.toarray() # creates matrix
# print matrix
# [[ 0.          0.14839068  0.          0.          0.          0.0913432    0.          0.          0.10172197  0.          0.11399744  0.          0.    0.          0.          0.          0.17569013  0.          0.          0.   0.          0.          0.          0.          0.          0.          0. 0.          0.          0.29678137  0.          0.          0.          0. 0.          0.          0.          0.          0.          0.          0. 0.          0.          0.          0.          0.          0. 0.14839068  0.          0.12902142  0.          0.          0.          0. 0.          0.          0.3051659   0.          0.          0.          0. 0.          0.          0.29678137  0.          0.          0.          0. 0.          0.12902142  0.          0.          0.          0.          0. 0.          0.          0.          0.          0.          0.          0. 0.          0.          0.          0.          0.          0.          0. 0.          0.          0.17569013  0.          0.          0.          0. 0.17569013  0.          0.          0.          0.          0.          0. 0.14839068  0.          0.07442252  0.          0.          0.          0. 0.          0.17569013  0.          0.50860983  0.17569013  0. 0.12902142  0.          0.          0.          0.          0. 0.34199233  0.12902142  0.          0.          0.17569013  0.          0. 0.        ]

# ----------------------
# OUTPUT > HTML
# ----------------------
fileout = open('output-fontsize-tfidf.html','w+')
# fileout = open('output-fontsize-tfidf_val.html','w+')
# fileout = open('output-fontsize-tfidf-fullchapter.html','w+')
fileout.write('<link rel="stylesheet" type="text/css" href="stylesheet.css">')
fileout.write("<h1>Counting - Term Frequency Inverse Document Frequency</h1>")
fileout.write("<div id='wrapper' style='position:relative;'>")

baseline = 10
stepsize = 35

for i, line in enumerate(corpus):
	# print line 

	vector = matrix[i]
	features = vectorizer.get_feature_names()
	# print line, vector, features

	for word in word_tokenize(line):
		try:
			index = features.index(word)
			# print word, features[index], vector[index]
			val = float(vector[index].astype(unicode))
			if val == 0.0:
				fontsize = baseline
			fontsize = baseline + (val * stepsize)
			val = '{0:.2f}'.format(float(val)) # make a 0.00 float
			html = "<span class='feature' style='display:inline-block;font-size:"+str(fontsize)+"pt;'>"+word+"</span> "
			# html = "<span class='feature' style='display:inline-block;font-size:"+str(fontsize)+"pt;'>"+word+"<sup>("+str(val)+")</sup></span> "
			fileout.write(html.encode('utf8'))
		except ValueError:
			# print word, '= not in vector'
			fontsize = baseline
			html = "<span class='feature notlisted' style='display:inline-block;font-size:"+str(fontsize)+"pt;opacity:0.5;'>"+word+"</span> "
			fileout.write(html.encode('utf8'))
	# print '********'
fileout.write("</div>")

	
# *** print not listed words ***
notlistedwords = notlisted(corpus, vectorizer)
# print 'not listed words: '+' '.join(notlisted)
fileout.write("<hr><div>")
fileout.write("<h1>Features that are not listed in the vector:</h1>")
for word in notlistedwords:
	html = "<span style='opacity:0.5;'>"+word+"</span> "
	fileout.write(html.encode('utf8'))
fileout.write("</div>")

print '*output TFIDF written to file*'
fileout.close()	