#  from: http://scikit-learn.org/stable/modules/feature_extraction.html#common-vectorizer-usage

from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
from nltk.tokenize import word_tokenize, sent_tokenize
np.set_printoptions(threshold=np.inf) # allows to print full arrays / prevents printing truncated representations of arrays 

def notlisted(corpus, vectorizer):
	list = []
	for line in corpus:
		for word in word_tokenize(line):
			features = vectorizer.get_feature_names()
			try:
				index = features.index(word)
				# print index
			except ValueError:
				# print word, 'not in the list'
				list.append(word)
	return list

# **********************
# TF
# **********************
vectorizer = CountVectorizer(min_df=1, lowercase=False)
# docs: http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html#sklearn.feature_extraction.text.CountVectorizer

# print vectorizer # prints default CountVectorizer settings
# CountVectorizer(analyzer='word', binary=False, decode_error='strict', dtype=<'numpy.int64'>, encoding='utf-8', input='content', lowercase=True, max_df=1.0, max_features=None, min_df=1, ngram_range=(1, 1), preprocessor=None, stop_words=None, strip_accents=None, token_pattern='(?u)\\b\\w\\w+\\b', tokenizer=None, vocabulary=None)

corpus = open('input/weizenbaum.txt','r').read()
corpus = sent_tokenize(corpus)
# print corpus
# print len(corpus) # prints number of sentences

tf = vectorizer.fit_transform(corpus)
# print tf # prints scipy.sparse matrix: a matrix, in sparse (= without 0's) dictionary notation
# (row, column), value = (sentence, feature), value
  # (0, 91)		1
  # (0, 125)	1
  # (0, 118)	1
  # ... 

analyze = vectorizer.build_analyzer()
# print analyze(corpus[0]) # prints unique words from first line in corpus

matrix_tf = tf.toarray()
# print matrix_tf # prints matrix
# print matrix_tf[0] # print the vector of the first line

# print vectorizer.get_feature_names() # prints unique words
# print vectorizer.vocabulary_.get('things') # print column-index > 112
# print vectorizer.get_feature_names()[112] # prints feature-name of column-index > 'things'


# **********************
# TF-IDF
# **********************
from sklearn.feature_extraction.text import TfidfVectorizer

vectorizer = TfidfVectorizer(min_df=1, lowercase=False)
# docs: http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html#sklearn.feature_extraction.text.TfidfVectorizer

# print vectorizer   
# TfidfVectorizer(analyzer=u'word', binary=False, decode_error=u'strict', dtype=<type 'numpy.int64'>, encoding=u'utf-8', input=u'content', lowercase=True, max_df=1.0, max_features=None, min_df=1, ngram_range=(1, 1), norm='l2', preprocessor=None, smooth_idf=True, stop_words=None, strip_accents=None, sublinear_tf=False, token_pattern=u'(?u)\\b\\w\\w+\\b', tokenizer=None, use_idf=True, vocabulary=None)

tfidf = vectorizer.fit_transform(corpus)
# print tfidf 
# (0, 114)	0.175690133779
# (0, 111)	0.175690133779
# (0, 8)	0.101721966517

matrix_tfidf = tfidf.toarray() # creates matrix
# print matrix
# [[ 0.          0.14839068  0.          0.          0.          0.0913432    0.          0.          0.10172197  0.          0.11399744  0.          0.    0.          0.          0.          0.17569013  0.          0.          0.   0.          0.          0.          0.          0.          0.          0. 0.          0.          0.29678137  0.          0.          0.          0. 0.          0.          0.          0.          0.          0.          0. 0.          0.          0.          0.          0.          0. 0.14839068  0.          0.12902142  0.          0.          0.          0. 0.          0.          0.3051659   0.          0.          0.          0. 0.          0.          0.29678137  0.          0.          0.          0. 0.          0.12902142  0.          0.          0.          0.          0. 0.          0.          0.          0.          0.          0.          0. 0.          0.          0.          0.          0.          0.          0. 0.          0.          0.17569013  0.          0.          0.          0. 0.17569013  0.          0.          0.          0.          0.          0. 0.14839068  0.          0.07442252  0.          0.          0.          0. 0.          0.17569013  0.          0.50860983  0.17569013  0. 0.12902142  0.          0.          0.          0.          0. 0.34199233  0.12902142  0.          0.          0.17569013  0.          0. 0.        ]

# ----------------------
# OUTPUT > HTML
# ----------------------
write = True
if write == True:
	fileout = open('output/output-matrices.html','w+')
	fileout.write('<link rel="stylesheet" type="text/css" href="stylesheet.css">')
	fileout.write("<h1>Counting - TF & TFIDF</h1>")
	fileout.write("<h2>input</h2>")
	fileout.write("""<p>Two things are clear: If we wish a machine to do something, we have to tell it to do it, and the machine must be able to understand what we say to it. <br><br> The most common way to tell a computer what to do, at least to this day, is to give it a specific program for the task we have in mind and, of course, the data to which that program is to be applied. <br><br> We may, for example, give it a square-root program and the number 25, and expect it to deliver the number 5 to us. <br><br> The computer "understands" the square-root program in the sense that it can interpret it in precisely the way we had in mind when we composed it. <br><br> But then such a program converts a computer into a very special-purpose machine, a square-root-taking machine, and nothing more. <br><br> Humans, if they are machines at all, are vastly general-purpose machines and, what is most important, they understand communications couched in natural languages (e.g., English) that lack, by very far, the precision and unambiguousness of ordinary programming languages. <br><br> Since the over-all aim of AI is to build machines that are "responsive to the full detail of a library of encyclopedias," work must naturally be done to enable them to under- stand natural language. <br><br> But, even apart from such dreams, there are both practical and scientific reasons for working on the natural-language problem. <br><br> If people from outside the computer fields are to be able to interact significantly with computers, then either they must learn the computer's languages or it must learn theirs.</p>""")


	def addFeature(feature_name, utf_value):
		html = "<span class='feature'><span class='fname'>"+feature_name+"</span><span class='fval'>"+utf_value+"</span></span>"
		fileout.write(html.encode('utf8'))

	matrices = [matrix_tf, matrix_tfidf]
	for x, matrix in enumerate(matrices):

		# *** print full matrix ***
		if x == 0:
			fileout.write("<h2>full matrix TF</h2>")
		else:
			fileout.write("<h2>full matrix TFIDF</h2>")
		fileout.write("<div class='full matrix'>")
		for vector in matrix:
			fileout.write("<div class='vector'>")
			for i, column in enumerate(vector):
				feature_name = vectorizer.get_feature_names()[i]
				feature_value = column
				utf_value = feature_value.astype(unicode)
				if x == 1: #tfidf
					utf_value = '{0:.2f}'.format(float(utf_value)) # make a 0.00 float
				html = "<span class='feature'><span class='fname'>"+feature_name+"</span><span class='fval'>"+utf_value+"</span></span>"
				fileout.write(html.encode('utf8'))
			fileout.write("</div>")
		fileout.write("</div>")

		# *** print sparse matrix ***
		if x == 0:
			fileout.write("<h2>sparse matrix TF</h2>")
		else:
			fileout.write("<h2>sparse matrix TFIDF</h2>")
		fileout.write("<div class='sparse matrix'>")
		for vector in matrix:
			fileout.write("<div class='sparse vector'>")
			for i, column in enumerate(vector):
				feature_name = vectorizer.get_feature_names()[i]
				feature_value = column
				utf_value = feature_value.astype(unicode)
				if x == 0: #tf
					if utf_value != '0':
						addFeature(feature_name, utf_value)
				else: #tfidf
					if utf_value != '0.0':
						utf_value = '{0:.2f}'.format(float(utf_value)) # make a 0.00 float
						addFeature(feature_name, utf_value)
			fileout.write("</div>")

		fileout.write("</div>")

	# *** print not listed words ***
	notlistedwords = notlisted(corpus, vectorizer)
	# print 'not listed words: '+' '.join(notlisted)
	fileout.write("<div>")
	fileout.write("<h2>not listed words</h2>")
	for word in notlistedwords:
		html = "<span style='opacity:0.5;'>"+word+"</span> "
		fileout.write(html.encode('utf8'))
	fileout.write("</div>")

	fileout.write("</div>")
	print '*output written to file*'
	fileout.close()