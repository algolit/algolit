#  from: http://scikit-learn.org/stable/modules/feature_extraction.html#common-vectorizer-usage

import re
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.util import ngrams
np.set_printoptions(threshold=np.inf) # allows to print full arrays / prevents printing truncated representations of arrays 

def notlisted(corpus, vectorizer):
	list = []
	for line in corpus:
		for word in word_tokenize(line):
			features = vectorizer.get_feature_names()
			try:
				index = features.index(word)
				# print index
			except ValueError:
				# print word, 'not in the list'
				list.append(word)
	return list

def createfeatures(corpus):
	words = word_tokenize(corpus)
	for w in words:
		# word
		yield w
		# chargram2
		for i in range(len(w) - 2):
			yield w[i:i+2]
		# chargram3
		for i in range(len(w) - 3):
			yield w[i:i+3]
		# chargram4
		for i in range(len(w) - 4):
			yield w[i:i+4]
		# chargram5
		for i in range(len(w) - 4):
			yield w[i:i+4]
		bigrams = ngrams(words, 2)
		for bigram in bigrams:
			bigram = '_'.join(bigram)
			yield bigram
		trigrams = ngrams(words, 3)
		for trigram in trigrams:
			print trigram
			pos = nltk.pos_tag(trigram)
			trigram_pos = '_'.join(tag for w, tag in pos)
			print trigram_pos
			print pos
			trigram = '_'.join(trigram)
			yield trigram
	pos = nltk.pos_tag(words)
	for tag in pos:
		yield 'POS_'+tag[1]

# **********************
# CORPUS
# **********************
filename = 'test'
# filename = 'cyborg-manifesto'
# filename = 'goedelescherbach'
# filename = 'weizenbaum_fullchapter'
corpus = open('input/'+filename+'.txt','r').read().decode('utf-8')
corpus = sent_tokenize(corpus)
# print corpus
# print len(corpus) # prints number of sentences

ngrm = 3

# **********************
# TF
# **********************
vectorizer = CountVectorizer(analyzer=createfeatures, min_df=1, lowercase=False)
# docs: http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html#sklearn.feature_extraction.text.CountVectorizer

# print vectorizer # prints default CountVectorizer settings
# CountVectorizer(analyzer='word', binary=False, decode_error='strict', dtype=<'numpy.int64'>, encoding='utf-8', input='content', lowercase=True, max_df=1.0, max_features=None, min_df=1, ngram_range=(1, 1), preprocessor=None, stop_words=None, strip_accents=None, token_pattern='(?u)\\b\\w\\w+\\b', tokenizer=None, vocabulary=None)

tf = vectorizer.fit_transform(corpus)
# print tf # prints scipy.sparse matrix: a matrix, in sparse (= without 0's) dictionary notation
# (row, column), value = (sentence, feature), value
  # (0, 91)		1
  # (0, 125)	1
  # (0, 118)	1
  # ... 

analyze = vectorizer.build_analyzer()
# print analyze(corpus[0]) # prints unique words from first line in corpus

matrix_tf = tf.toarray()
print matrix_tf # prints matrix
# print matrix_tf[0] # print the vector of the first line

print vectorizer.get_feature_names() # prints unique features
# print vectorizer.vocabulary_.get('things') # print column-index > 112
# print vectorizer.get_feature_names()[112] # prints feature-name of column-index > 'things'


# **********************
# TF-IDF
# **********************
from sklearn.feature_extraction.text import TfidfVectorizer

vectorizer = TfidfVectorizer(analyzer=createfeatures, min_df=1, lowercase=False)
# docs: http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html#sklearn.feature_extraction.text.TfidfVectorizer

# print vectorizer   
# TfidfVectorizer(analyzer=u'word', binary=False, decode_error=u'strict', dtype=<type 'numpy.int64'>, encoding=u'utf-8', input=u'content', lowercase=True, max_df=1.0, max_features=None, min_df=1, ngram_range=(1, 1), norm='l2', preprocessor=None, smooth_idf=True, stop_words=None, strip_accents=None, sublinear_tf=False, token_pattern=u'(?u)\\b\\w\\w+\\b', tokenizer=None, use_idf=True, vocabulary=None)

tfidf = vectorizer.fit_transform(corpus)
# print tfidf 
# (0, 114)	0.175690133779
# (0, 111)	0.175690133779
# (0, 8)	0.101721966517

matrix_tfidf = tfidf.toarray() # creates matrix
# print matrix
# [[ 0.          0.14839068  0.          0.          0.          0.0913432    0.          0.          0.10172197  0.          0.11399744  0.          0.    0.          0.          0.          0.17569013  0.          0.          0.   0.          0.          0.          0.          0.          0.          0. 0.          0.          0.29678137  0.          0.          0.          0. 0.          0.          0.          0.          0.          0.          0. 0.          0.          0.          0.          0.          0. 0.14839068  0.          0.12902142  0.          0.          0.          0. 0.          0.          0.3051659   0.          0.          0.          0. 0.          0.          0.29678137  0.          0.          0.          0. 0.          0.12902142  0.          0.          0.          0.          0. 0.          0.          0.          0.          0.          0.          0. 0.          0.          0.          0.          0.          0.          0. 0.          0.          0.17569013  0.          0.          0.          0. 0.17569013  0.          0.          0.          0.          0.          0. 0.14839068  0.          0.07442252  0.          0.          0.          0. 0.          0.17569013  0.          0.50860983  0.17569013  0. 0.12902142  0.          0.          0.          0.          0. 0.34199233  0.12902142  0.          0.          0.17569013  0.          0. 0.        ]

# ----------------------
# OUTPUT > HTML
# ----------------------
write = True
if write == True:
	fileout = open('output/vectorizer_matrices_multiple-feature-types_'+filename+'.html','w+')
	fileout.write('<!DOCTYPE html>')
	fileout.write('<html>')
	fileout.write('<head>')
	fileout.write('<meta charset="utf-8">')
	fileout.write('<link rel="stylesheet" type="text/css" href="stylesheet.css">')
	fileout.write('''<style>
			th, td{
				padding:0 10px
			}
		</style>''')
	fileout.write('</head>')
	fileout.write('<body>')
	fileout.write("<h1>Counting - TF & TFIDF</h1>")
	fileout.write("<h2>input</h2>")
	for sentence in corpus:
		fileout.write("<p>"+sentence+"</p>")

	# def addFeature(feature_name, utf_value):
	# 	html = "<span class='feature'><span class='fname'>"+feature_name+"</span><span class='fval'>"+utf_value+"</span></span>"
	# 	fileout.write(html.encode('utf8'))

	matrices = [matrix_tf, matrix_tfidf]
	for x, matrix in enumerate(matrices):

		# *** print table ***
		if x == 0:
			fileout.write("<h2>matrix TF</h2>")
		else:
			fileout.write("<h2>matrix TFIDF</h2>")
		fileout.write("<table id='matrix_table' class='display'>")
		# thead
		fileout.write("<thead><tr>")
		feature_names = vectorizer.get_feature_names()
		for feature_name in feature_names:
			fileout.write('<th>'+feature_name+'</th>')
		fileout.write("</tr></thead>")
		fileout.write("<tbody>")
		for vector in matrix:
			fileout.write("<tr>")
			for i, column in enumerate(vector):
				feature_value = column
				utf_value = feature_value.astype(unicode)
				if x == 0: #tf
					fileout.write("<td>"+utf_value+"</td>")
				else: #tfidf
					utf_value = '{0:.2f}'.format(float(utf_value)) # make a 0.00 float
					fileout.write("<td>"+utf_value+"</td>")
			fileout.write("</tr>")
		fileout.write("</tbody>")
		fileout.write("</table>")


		# # *** print full matrix ***
		# if x == 0:
		# 	fileout.write("<h2>full matrix TF</h2>")
		# else:
		# 	fileout.write("<h2>full matrix TFIDF</h2>")
		# fileout.write("<div class='full matrix'>")
		# for vector in matrix:
		# 	fileout.write("<div class='vector'>")
		# 	for i, column in enumerate(vector):
		# 		feature_name = vectorizer.get_feature_names()[i]
		# 		feature_value = column
		# 		utf_value = feature_value.astype(unicode)
		# 		if x == 1: #tfidf
		# 			utf_value = '{0:.2f}'.format(float(utf_value)) # make a 0.00 float
		# 		addFeature(feature_name, utf_value)
		# 	fileout.write("</div>")
		# fileout.write("</div>")

		# # *** print sparse matrix ***
		# if x == 0:
		# 	fileout.write("<h2>sparse matrix TF</h2>")
		# else:
		# 	fileout.write("<h2>sparse matrix TFIDF</h2>")
		# fileout.write("<div class='sparse matrix'>")
		# for vector in matrix:
		# 	fileout.write("<div class='sparse vector'>")
		# 	for i, column in enumerate(vector):
		# 		feature_name = vectorizer.get_feature_names()[i]
		# 		feature_value = column
		# 		utf_value = feature_value.astype(unicode)
		# 		if x == 0: #tf
		# 			if utf_value != '0':
		# 				addFeature(feature_name, utf_value)
		# 		else: #tfidf
		# 			if utf_value != '0.0':
		# 				utf_value = '{0:.2f}'.format(float(utf_value)) # make a 0.00 float
		# 				addFeature(feature_name, utf_value)
		# 	fileout.write("</div>")

	# # *** print not listed words ***
	# notlistedwords = notlisted(corpus, vectorizer)
	# # print 'not listed words: '+' '.join(notlisted)
	# fileout.write("<div>")
	# fileout.write("<h2>not listed words</h2>")
	# for word in notlistedwords:
	# 	html = "<span style='opacity:0.5;'>"+word+"</span> "
	# 	fileout.write(html.encode('utf8'))
	# fileout.write("</div>")

	# fileout.write("</div>")
	fileout.write('</body>')
	fileout.write('</html>')
	print '*output written to file*'
	fileout.close()