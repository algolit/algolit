Writing Machines
================
This writing system is based on the existing rule-based script [modality.py](https://github.com/clips/pattern/blob/master/pattern/text/en/modality.py), and is distributed as part of the text-mining software package [Pattern](https://github.com/clips/pattern).

Requirements
============
The requirements for this tool are listed in the requirements.txt file.
To install these, run:

> pip install -r requirements.txt

Output folder
=============
Exported html & pdf documents are written to the 'output' folder.
Before using the tool, create the following folders:

> writing-systems/rule-based-modality/output/html/
> writing-systems/rule-based-modality/output/pdf/

Use 
===
To run the tool locally, use the python server module. 
Navigate to this folder in the terminal and start the server from there. 

> cd ~/path/i-could-have-written-that/writing-machines/rule-based-modality/
> python -m CGIHTTPServer

This run a CGI server on localhost:8000.

To use the tool that is based on modality.py, open in a browser: 

> localhost:8000/cgi-bin/rule-based-modality.py.cgi 

or for the tool without preset rules: 

> localhost:8000/cgi-bin/rule-based-writing.py.cgi 

---

Note: It is possible to run multiple python servers, but not on the same port. 
To run a server on port 9000 for example: 

> python -m CGIHTTPServer 9000

modules
=======
beside the .cgi interface, there are some standalone scripts in the modules folder.

