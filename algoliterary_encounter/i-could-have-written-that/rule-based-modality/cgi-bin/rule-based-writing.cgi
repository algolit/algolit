#!/usr/bin/env python
from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
from jinja2 import FileSystemLoader, Environment
import os, sys, datetime

# *******************************************************
# settings
# *******************************************************
path = "./"
env = Environment(loader=FileSystemLoader(path)) 
template = env.get_template("interface.html")


# *******************************************************
# read form fields into variables
# *******************************************************
method = os.environ.get("REQUEST_METHOD")
form = cgi.FieldStorage()

input = form.getvalue("input", "").decode('utf-8')
# print('\n input:', input, file=sys.stderr)

# types = ['nouns','verbs','adjectives']
types = ['words']
parameters = {}
for type in types:
	dict = {}
	values = [-1.00, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0]
	for value in values:
		if form.getlist('parameter_'+type+str(value)) != []:
			words = form.getlist('parameter_'+type+str(value))
			print('\n words', words, file=sys.stderr)
			dict[value] = words
		else:
			dict[value] = []
	parameters[type] = dict
# print('\nparameters', parameters, file=sys.stderr)


# *******************************************************
# change variables (as soon as the form is submitted)
# *******************************************************

def s(word):
	# return word.string.lower()
	return word.string

### MODALITY #######################################################################################
# Functions take Sentence objects, see pattern.text.tree.Sentence and pattern.text.parsetree().

def d(*args):
	return dict.fromkeys(args, True)

AUXILLARY = {
	  "be": ["be", "am", "m", "are", "is", "being", "was", "were" "been"],
	 "can": ["can", "ca", "could"],
	"dare": ["dare", "dares", "daring", "dared"], 
	  "do": ["do", "does", "doing", "did", "done"],
	"have": ["have", "ve", "has", "having", "had"], 
	 "may": ["may", "might"], 
	"must": ["must"], 
	"need": ["need", "needs", "needing", "needed"],
   "ought": ["ought"], 
   "shall": ["shall", "sha"], 
	"will": ["will", "ll", "wo", "willing", "would", "d"]
}

MODIFIERS = ("fully", "highly", "most", "much", "strongly", "very")

EPISTEMIC = "epistemic" # Expresses degree of possiblity.

# -1.00 = NEGATIVE
# -0.75 = NEGATIVE, with slight doubts
# -0.50 = NEGATIVE, with doubts
# -0.25 = NEUTRAL, slightly negative
# +0.00 = NEUTRAL
# +0.25 = NEUTRAL, slightly positive
# +0.50 = POSITIVE, with doubts
# +0.75 = POSITIVE, with slight doubts
# +1.00 = POSITIVE

epistemic_NN = parameters['words']

# epistemic_NN = parameters_nn
print('\n dict: '+ str(epistemic_NN), file=sys.stderr)

epistemic_weaseling = {
	-0.75: d("popular belief"),
	-0.50: d("but that", "but this", "have sought", "might have", "seems to"),
	-0.25: d("may also", "may be", "may have", "may have been", "some have", "sort of"),
	+0.00: d("been argued", "believed to", "considered to", "claimed to", "is considered", "is possible", 
			 "overall solutions", "regarded as", "said to"),
	+0.25: d("a number of", "in some", "one of", "some of", 
			 "many modern", "many people", "most people", "some people", "some cases", "some studies", 
			 "scientists", "researchers"),
	+0.50: d("in several", "is likely", "many of", "many other", "of many", "of the most", "such as",
			 "several reasons", "several studies", "several universities", "wide range"),
	+0.75: d("almost always", "and many", "and some", "around the world", "by many", "in many", "in order to", 
			 "most likely"),
	+1.00: d("i.e.", "'s most", "of course", "There are", "without doubt"),
}
# print('\n dict: '+ str(epistemic_weaseling), file=sys.stderr)

epistemic_JJ = {
	-1.00: d(),
	-0.75: d(),
	-0.50: d(),
	-0.25: d(),
	 0.00: d(),
	+0.25: d(),
	+0.50: d(),
	+0.75: d(),
	+1.00: d(),
}


def s(word):
	# return word.string.lower()
	return word.string

wordlist = []
sentencelist = []

from nltk.tokenize import sent_tokenize
lines = sent_tokenize(input)
for line in lines: 

	from pattern.en import tokenize
	sentences = tokenize(line, punctuation=".,;:!?()[]{}`''\"@#$^&*+-|=~_", replace={})

	for sentence in sentences:
		words = {}

		try:
			# A Sentence is expected but a string given.
			# Attempt to parse the string on-the-fly.
			from pattern.en import parse, Sentence
			sentence = Sentence(parse(sentence))
		except ImportError:
			pass

		#+ start of modality object
		#+ s = sentence
		#+ n = number?
		#+ m = modality rate
		S, n, m = sentence, 0.0, 0

		if not (hasattr(S, "words") and hasattr(S, "parse_token")):
			raise TypeError, "%s object is not a parsed Sentence" % repr(S.__class__.__name__)

		r = S.string.rstrip(" .!")

		#+ looping through the epistemic_weaseling_dict
		for k, v in epistemic_weaseling.items():
			for phrase in v:
				if phrase in r:
					#+ # print  values here to see how they are changing
					# print  '    *WEASEL phrase*:', phrase
					# print  '    k:', k
					n += k
					# print  '    n:',n
					weight = 2
					m += weight
					# print  '    m:',m

					wordsPhrase = phrase.split(' ')
					for word in wordsPhrase:
						words[word] = ['weasel', str(k)+' '+str(weight)]
						# print  '*newWeasel* -->', word, words[word]

		#+ START!
		#+ ********************************************************* 
		#+ iterates over all words in the sentence
		for i, w in enumerate(S.words):
			# print  '------------------------------------------------'
			# print  w
			classified = 'NO'
			for type, dict, weight in (
			  # (  "MD", epistemic_MD, 4), 
			  # (  "VB", epistemic_VB, 2), 
			  # (  "RB", epistemic_RB, 2), 
			  (  "JJ", epistemic_JJ, 1),
			  (  "NN", epistemic_NN, 1)):
			  # (  "CC", epistemic_CC_DT_IN, 1),
			  # (  "DT", epistemic_CC_DT_IN, 1),
			  # (  "IN", epistemic_CC_DT_IN, 1),
			  # ("PRP" , epistemic_PRP, 1),
			  # ("PRP$", epistemic_PRP, 1),
			  # ( "WP" , epistemic_PRP, 1)
			  

				#+ ********************************************************* 
				#+ # print  for every word in the sentence, 
				#+ every dict_type 
				# print  'i+word+type+weight:', i, w, type, weight

				#+ ********************************************************* 
				#+ MODIFIERS add to the weight of the target of the MODIFIER
				# "likely" => weight 1, "very likely" => weight 2
				if i > 0 and s(S[i-1]) in MODIFIERS:
					weight += 1
					# print  '    *MODIFIER DETECTED*'
					# print  '    modifier:', s(S[i-1])
					# print  '    modified:', w 
					# print  '    weight:', weight
					# print 

					word = s(w)
					modifier = s(S[i-1])
					words[modifier].append('modifier')
					if word in words.keys():
						words[word].append('modified')
					else:
						words[word] = ['modified']
					# # remove modifier as 'none' word
					# for word in words.itervalues():
					# 	if word == modifier:
					# 	    try:
					# 	        word.remove('none')
					# 	    except ValueError:
					# 	        pass

					# # print  '*new word added* --> ', word
					# # print  '*new word added* --> ', modifier
					classified = 'YES'

				#+ ********************************************************* 
				# likely" => score 0.25 (neutral inclining towards positive).
				#+ here, the word_type is matched with the dict_type
				if w.type and w.type.startswith(type):

					for k, v in dict.items():
						#+ # print  all items of the matching dict
						#+ if word appears in the dict:
						if (w.lemma or s(w)) in v:

							word = s(w)
							if word in words.keys():
								words[word].append('nw')
								words[word].append(str(k))
								words[word].append(str(weight))
							else:
								words[word] = ['nw', str(k), str(weight), type]
							# # print  '*new word added* --> ', word, words[word]
							classified = 'YES'

							#+ ********************************************************* 
							# Reverse score for negated terms.
							#+ FASCINATING, negation in three lines:
							if i > 0 and s(S[i-1]) in ("not", "n't", "never", "without"):
								k = -k * 0.5
								# print 
								# print  '    negated k :', k

								word = s(w)
								negator = s(S[i-1])
								# print  negator
								words[negator].append('negator')
								words[word].append('nw')
								words[word].append('negated')
								words[word].append(str(k))
								words[word].append(str(weight))
								# # print  '*new word added* --> ', word, words[word]
								classified = 'YES'

							#+ ********************************************************* 
							#+ the word's weight of the dict_type is multiplied by the specific word_value from dict_type
							# print  '    former n  :', n
							n += weight * k

							#+ weight = dict_type weight (VB = 2, NN = 1)
							#+ k = the word_value from dict_type
							#+ n = ? 
							# print  '    weight    :', weight
							# print  '    k         :', k
							# print  '    weight * k:', weight * k
							# print  '    new n     :', n

							m += weight
							# print  '    m         :', m
							# break

			#+ ********************************************************* 
			# Numbers, citations, explanations make the sentence more factual.
			#+ numbers are heavy!
			#+ what is CD???? > Cardinal number https://en.wikipedia.org/wiki/Cardinal_number
			if w.type in ("CD", "\"", "'", ":", "("): 

				# print 
				# print  w.type
				# print  'former n      :', n

				k = 0.75
				n += k
				
				# print  'new n         :', n
				
				weight = 1
				m += weight
				# print  'm             :', m
				# print 

				word = s(w)
				words[word] = []
				words[word].append('special')
				words[word].append(str(k))
				words[word].append(str(weight))
				# # print  '*new word added* --> ', word, words[word]
				classified = 'YES'

			if classified == 'NO':
				word = s(w)
				# # print  words
				if word in words.keys():
					words[word].append('none')
				else:
					words[word] = ['none']
				# # print  '*new word added* --> ', word, words[word] 



		#+ ********************************************************* 
		# end of words-loop


		#+ ********************************************************* 
		# for word, value in words.iteritems():
			# # print  word, value

		# print  '------------------------------------------------------'

		# print  'dict:'

		sentence_div = []
		for word in S.words: 
			# print  '-------------------------------'
			word = s(word)

			values = []
			for w, vals in words.iteritems():
				if w == word:
					# print  w, vals
					# # print  'MATCH!!!!!!'
					
					patterns = [
						['-1.0','n1'],
						['-0.75','n75'],
						['-0.5','n50'],
						['-0.375','n375'],
						['-0.25','n25'],
						['-0.125','n125'],
						['0.0','np'],
						['0.125','p125'],
						['0.25','p25'],
						['0.375','p375'],
						['0.5','p50'],
						['0.75','p75'],
						['1.0','p1']
					]
					for pattern in patterns: 
						for v in vals:
							if pattern[0] in v:
								oldv = v
								v = v.replace(pattern[0], pattern[1])
								# # print  v
								vals.append(v)
								vals.remove(oldv)
								break
							# else:

					# values = ' '.join(v)
					vals = ' '.join(vals)
					worddiv = '<div class="'+vals+'">'+word+'</div>'
					# print  worddiv
					wordlist.append(worddiv)
					sentence_div.append(worddiv)

		# print  '------------------------------------------------'
		if m == 0:
			v = m
			# print  'm == 0!'
			# print  1.0 # No modal verbs/adverbs used, so statement must be true.

		m = max(-1.0, min(n / (m or 1), +1.0))
		m = str(m)
		W = ' '.join([w for w in sentence_div])
		# print  'modality rate:', W, m
		sentencelist.append((m, W))

		# def uncertain(sentence, threshold=0.5):
		#     return modality(sentence) <= threshold


# print  '------------------------------------------------'
html = ' '.join(wordlist)

sorted_html = []
list = sorted(sentencelist)
for v, sentence in list:
	if v > 0:
		# # print  v
		sorted_html.append('<small>'+str(v)+'</small>')
		sorted_html.append('<div>'+sentence+'</div>')
		sorted_html.append('<br>')
sorted_html = ' '.join(sorted_html)



# *******************************************************
# set jinja variables
# *******************************************************
tvars = {}
tvars['method'] = method
tvars['input'] = input
tvars['html'] = html
tvars['sorted_html'] = sorted_html
tvars['parameters'] = parameters
tvars['values'] = [-1.00, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0]



# *******************************************************
# render
# *******************************************************
# print ('Variables are rendered using Jinja ...', file=sys.stderr)

print ("Content-type: text/html;charset=utf-8")
print ()
html = template.render(tvars).encode('utf-8')
print (html)