Crowd Annotated Bag of Words (supervised-writing)
==========================================================================================

# Disambiguation

* crowd: making use of sources that have been constructed by a larger amount of people
* annotated: the act of labeling an object, such as a sentence or paragraph
* bag-of-words: bag-of-words model is a classification model which reads a text as a collection of words. While processing a text the model discards word order, punctuation and possibly conjugations. The model transforms the text into a unique list of words used in the text, or quite literally a bag of words.
* writing machine: an artistic approach to make web-based tools that offer an experience based insight in the mechanism of machine learning techniques

-------------------------------------------------------------------------------------------

# Use 

To run the browser-tool locally: 

> python -m CGIHTTPServer

this run a CGI server on localhost:8000
navigate to localhost:8000/cgi-bin/writing-machine.cgi 

-------------------------------------------------------------------------------------------

# Colophon

A study into the development of experimental writing machines, based on text mining processes. Supported by a financial contribution of CBK Rotterdam.

This writing system is made with the web-mining software library Pattern and is part of the project i-could-have-written-that.

All content released under a Free Art License.
