#!/usr/bin/env python

from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
import os, sys
from datetime import datetime
import re
# *******************************************************
# read input fields into variables
# *******************************************************
form = cgi.FieldStorage()
filename = form.getvalue("filename", "").decode('utf-8')
extension = form.getvalue("extension", "").decode('utf-8')
results = form.getvalue("results", "").decode('utf-8')
title = form.getvalue("title", "").decode('utf-8')
source = form.getvalue("source", "").decode('utf-8')
author = form.getvalue("author", "").decode('utf-8')

date = datetime.now().strftime('%d %B %Y, at %H:%M:%S')

# ****************
# export
# ****************
def save(filename):

	# ***********************
	# HTML > posted via AJAX
	# ***********************
	html_out = 'output/html/'+filename+'.html'
	with open (html_out,'w+') as f:
		html = '''<!DOCTYPE html>
			<html class="page">
			<head>
				<meta charset="utf-8">
				<title>'''+title+''' - Crowd Annotated Vocabularies</title>
				<link rel="stylesheet" type="text/css" href="../../css/export.css">
				<link rel="stylesheet" type="text/css" href="../../css/stylesheet.css">
			</head>
			<body>
				<small id="header">This document <b>'''+filename+'''.pdf</b> is based on the script	<a href="https://github.com/clips/pattern/blob/master/pattern/text/en/modality.py" target="_blank">modality.py</a>, a rule-based classification script published as part of the software package <a href="https://github.com/clips/pattern" target="_blank">Pattern</a> and developed by the CLiPS department of the University in Antwerp. The text <b>'''+source+'''</b> is used as a means to explore a supervised bag-of-words classification system. This document is written by the following author(s): <b>'''+author+'''</b> and is generated on '''+date+'''. Font: Computer Modern Unicode Typewriter.
				</small>
				<div id="wrapper">
					<h2>Crowd Aannotated binary Vocabularies</h2>
					<div>'''+results+'''</div>
				</div>
			</body>
			</html>'''
		f.write(html.encode('utf8'))
		f.close()

	# *******************************************
	# PDF with wkhtmltopdf: html > pdf
	# *******************************************
	if extension == 'pdf':
		pdf_out = 'output/pdf/'+filename+'.pdf'
		cmd = "weasyprint "+html_out+' '+pdf_out+' -s css/stylesheet.css -s css/export.css'
		print('>>>', cmd, file=sys.stderr)
		os.system(cmd)

if filename:
	save(filename)