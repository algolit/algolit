#!/usr/bin/env python

from __future__ import print_function
import sys
import cgi
import cgitb; cgitb.enable()
from pattern.web import Twitter
from pattern.en import Sentence, parse, tokenize

form = cgi.FieldStorage()
a_label = form.getvalue("a_label", "win")
a_label = a_label.strip()
b_label = form.getvalue("b_label", "fail")
b_label = b_label.strip()


# t = Twitter(license="Sk2JVoe074gIWQelnhdsxrEWT", throttle=0.5, language=None)
# t = Twitter(license="125770826-9SZMTEkWO8tU4lFQNvDSkmgYFWtrotHYKwviBp2A")
t = Twitter()
a_output = []
b_output = []
for page in range(1, 10):
	for tweet in t.search('#'+a_label, start=page, count=10, cached=True):
		s = tweet.text.lower().encode('utf-8')
		print (s, file=sys.stderr)
		if len(s) > 0:
			a_output.append(s)
	for tweet in t.search('#'+b_label, start=page, count=10, cached=True):
		s = tweet.text.lower().encode('utf-8')
		print (s, file=sys.stderr)
		if len(s) > 0:
			b_output.append(s)
out={}

out['a_output'] = a_output 
out['b_output'] = b_output 
out['a_label'] = a_label
out['b_label'] = b_label

out['a_count'] = len(a_output)
out['b_count'] = len(b_output)


import json

print ("Content-type: application/json")
print ()
print (json.dumps(out, indent=4).encode('utf-8'))

