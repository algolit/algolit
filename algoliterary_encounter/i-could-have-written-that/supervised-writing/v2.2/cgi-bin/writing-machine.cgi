#!/usr/bin/env python

from __future__ import print_function
import cgi
import cgitb; cgitb.enable()
import csv
from jinja2 import FileSystemLoader, Environment
from pattern.web import plaintext
from pattern.en import Sentence, parse, tokenize, parsetree
from pattern.search import search
from pattern.vector import Document, Model, KNN, TFIDF, TF, IG, BINARY, PORTER, LEMMA
import os, sys; sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..", ".."))

# Classification is a supervised machine learning method,
# where labeled documents are used as training material
# to learn how to label unlabeled documents.

# This example trains a simple classifier.
# The idea is that, if you have a number of texts with a "type"
# (mail/spam, positive/negative, language, author's age, ...),
# you can predict the type of other "unknown" texts.
# The k-Nearest Neighbor algorithm classifies texts according
# to the k documents that are most similar (cosine similarity) to the given input document.

path = "" # for working locally
# path = "../" # for working on the server
env = Environment(loader=FileSystemLoader(path)) 

def sorted_vector(elements, cat):
	"[[{u'this': 1, u'is': 2, u'happy': 1}, u'this is happy ']]"
	"(1, ['this', 'sentence']), (1, ['is', 'sentence'])" 

	# """ {"foo": 1, "bar": 1, "baz": 5} => (1, "foo"), (1, "bar"), (5, "baz") """
	items = []
	i = 0
	for vector, sentence in elements:
		for word, weight in vector.items():
			items.append((weight, [word, sentence]))
			i = i + 1
	if cat == "a":
		items.sort(reverse=True)
	if cat == "b":
		items.sort()
	# print ('>>> items', items, file=sys.stderr)
	return items

def group (things):
	""" (1, ["foo","sentence"]), (1, "bar"), (5, "baz") ==>   (1, [["foo", "sentence"], ["bar", "sentence"]]), (5, ["baz"]) """
	last_key = None
	out = []
	curitem = None
	for key, value in things:
		key = round(key,3)
		if key != last_key:
			last_key = key
			curitem = (key, [])
			out.append(curitem)
		curitem[1].append(value)
	# print ('>>> out', out, file=sys.stderr)
	return out

env.filters['sorted_vector'] = sorted_vector
env.filters['group'] = group
template = env.get_template("interface.html")

method = os.environ.get("REQUEST_METHOD")
form = cgi.FieldStorage()
title = form.getvalue("title", "").decode('utf-8')
author = form.getvalue("author", "").decode('utf-8')
email = form.getvalue("email", "").decode('utf-8')
terms = form.getvalue("terms", "")
licence = form.getvalue("licence", "-")
a_label = form.getvalue("a_label", "").decode('utf-8')
a_label = a_label.strip()
b_label = form.getvalue("b_label", "").decode('utf-8')
b_label = b_label.strip()
a_text = form.getvalue("a_text", "").decode('utf-8')
b_text = form.getvalue("b_text", "").decode('utf-8')
sentence_a = form.getvalue("sentence_a", "").decode('utf-8')
sentence_b = form.getvalue("sentence_b", "").decode('utf-8')
graduallens = form.getvalue("graduallens", "").decode('utf-8')
state = form.getvalue("state","")



tvars = {}

tvars['a_text'] = a_text
tvars['b_text'] = b_text
tvars['method'] = method
tvars['a_label'] = a_label
tvars['b_label'] = b_label
tvars['author'] = author
tvars['email'] = email
tvars['title'] = title
tvars['terms'] = terms
tvars['licence'] = licence
if licence == "-":
	licence = "no license selected"
tvars['state'] = state
tvars['sentence_a'] = sentence_a
tvars['sentence_b'] = sentence_b

print ('>>> author', author, file=sys.stderr)


if method == "POST":
	# word types, POS (part-of-speech)
	pos = form.getvalue("pos", "all words")
	tvars['pos'] = pos
	if pos == "all words":
		pos = ('CC', 'CD', 'DT', 'EX', 'FW', 'IN', 'JJ', 'JJR', 'JJS', 'LS', 'MD', 'NN', 'NNS', 'NNP', 'NNPS', 'PDT', 'POS', 'PRP', 'PRP', 'RB', 'RBR', 'RBS', 'RP', 'SYM', 'TO', 'UH', 'VB', 'VBZ', 'VBP', 'VBD', 'VBN', 'VBG', 'WDT', 'WP', 'WP', 'WRB')
		wordtype = 'all'
	elif pos == "nouns":
		pos = 'NN'
		wordtype = 'noun'
	elif pos == "adjectives":
		pos = 'JJ'
		wordtype = 'adjective'
	elif pos == "verbs":
		pos = 'VB'
		wordtype = 'verb'
	elif pos == "modal":
		pos = 'MD'
		wordtype = 'modal'
	elif pos == "symbols":
		pos = 'SYM'
		wordtype = 'symbol'

	# lemma
	lemma = form.getvalue("lemma", "False")
	tvars['lemma'] = lemma
	if lemma == "False":
		stemmer = None
	elif lemma == "True":
		stemmer = LEMMA

	# stopwords
	stopwords = form.getvalue("stopwords", "True")
	tvars['stopwords'] = stopwords
	print ('>>> stopwords', stopwords, file=sys.stderr)
	if stopwords == "True":
		stop = True
	elif stopwords == "False":
		stop = False
	print ('>>> stop', stop, file=sys.stderr)

	# numerization
	numerization = form.getvalue("numerization", "False")
	if numerization == "-":
		numerization = "False"
	tvars['numerization'] = numerization
	print ('>>> numerization', numerization, file=sys.stderr)
	if numerization == "False":
		weight = None
	if numerization == "TF":
		weight = TF
	if numerization == "TFIDF":
		weight = TFIDF
	# if numerization == "IG":
	# 	weight = IG
	# if numerization == "BINARY":
	# 	weight = BINARY
	# if numerization == "None":
	# 	weight = None
	print ('>>> numerization', numerization, file=sys.stderr)
	print ('>>> weight', weight, file=sys.stderr)

	# main data containers
	lines = {}
	lines['a'] = {}
	lines['b'] = {}
	allwords = {}
	allwords['a'] = []
	allwords['b'] = []

	# *****************************************
	# filtering text on pos-tag + lemmatizing 

	i = 0
	for sentence in a_text.strip().splitlines():
		if len(sentence) > 0:
			sentence = sentence.replace('"','\'')
			sentenceObject = parsetree(sentence, lemmata=True)[0]
			if sentenceObject: 
				if lemma == "True":
					words = [w.lemma for w in sentenceObject if w.tag.startswith(pos)]
				else:
					words = [w.string for w in sentenceObject if w.tag.startswith(pos)]

				if words:
					wordlist = {}
					for x, word in enumerate(words):
						wordlist[word] = {}
						if not word in allwords:
							allwords['a'].append(word)
					words = ', '.join(words)
				else:
					wordlist = {}
				lines['a'][i] = { 
						'sentence': sentence,
						'words' : words, 
						'wordlist' : wordlist,
						'filter' : wordtype, 
						'lemma' : lemma,
						'licence' : licence,
						'stopwords' : stopwords
					}
				i = i + 1

	tvars['state'] = 'filter'	

	i = 0
	for sentence in b_text.strip().splitlines():
		if len(sentence) > 0:
			sentence = sentence.replace('"','\'')
			sentenceObject = parsetree(sentence, lemmata=True)[0]
			if sentenceObject: 
				if lemma == "True":
					words = [w.lemma for w in sentenceObject if w.tag.startswith(pos)]
				else:
					words = [w.string for w in sentenceObject if w.tag.startswith(pos)]

				if words:
					wordlist = {}
					for x, word in enumerate(words):
						wordlist[word] = {}
						if not word in allwords:
							allwords['b'].append(word)
					words = ', '.join(words)
				else:
					wordlist = {}
				lines['b'][i] = { 
						'sentence': sentence,
						'words' : words, 
						'wordlist' : wordlist,
						'filter' : wordtype, 
						'lemma' : lemma,
						'stopwords' : stopwords
					}
				i = i + 1

	
	# print('>>> lines', lines, file=sys.stderr)


 	# *****************************************
 	# numerization

	# stopwords=True bizarely means INCLUDE the "stopwords" (which are predefined)
	# stemmer = LEMMA / stemmer = None
	# treshold is the minimum amount of appearing

	# print('\n>>> allwords', allwords, file=sys.stderr)

	for cat in ['a','b']:
		# make document for every sentence
		# and add tf for TF value to the wordlist for each (filtered!) word
		for i, item in lines[cat].items():
			d = Document(item['sentence'], stemmer=stemmer, stopwords=stop, treshold=1)
			lines[cat][i]['document'] = d
			for word, wlitem in item['wordlist'].items():
				for w, count in d.vector().items():
					count = round(count,3)
					if w == word:
						lines[cat][i]['wordlist'][word]['tf'] = count
				if lines[cat][i]['wordlist'][word] == {}:
					lines[cat][i]['wordlist'][word]['tf'] = 'not vectorized'

		# print('>>> lines', lines, file=sys.stderr)

		# make a model for all the (filtered!) words
		# create the tfidf for TFDIDF value for each word in wordlist
		# add sentences ([]) where that words appears in to wordlist
		documents = []
		for i, item in lines[cat].items():
			d = Document(item['words'], stemmer=stemmer, stopwords=stop, treshold=1)
			documents.append(d)
		model = Model(documents, weight=weight) # here the actual 'weighting' happens
		for i, item in lines[cat].items():
			# print('>>> item', item, file=sys.stderr)
			lines[cat][i]['vector'] = documents[i].vector()
			lines[cat][i]['categoryvector'] = model.vectors
			# print('>>> d vector', d.vector(), file=sys.stderr)
			for word, wlitem in item['wordlist'].items():
				s = []
				# print('>>> s na reset', s, file=sys.stderr)
				for d in documents:
					for w, count in d.vector().items():
						count = round(count,3)
						if word == w:
							lines[cat][i]['wordlist'][word]['tfidf'] = count
							s = [item['sentence'] for x, item in lines[cat].items() if word in item['sentence']] # collect all sentences with current word
							lines[cat][i]['wordlist'][word]['sentences'] = s
							# print('>>> s saved', s, file=sys.stderr)
				if not 'tfidf' in lines[cat][i]['wordlist'][word]:
					lines[cat][i]['wordlist'][word]['tfidf'] = 'not vectorized'
				if s == []:
					# print('>>> word', word, file=sys.stderr)
					# print('>>> s IS EMPTY!!!!!!!', s, file=sys.stderr)
					# print('>>> s', item['sentence'], file=sys.stderr)
					lines[cat][i]['wordlist'][word]['sentences'] = [item['sentence']]
		
		# print('>>> lines', lines, file=sys.stderr)

		# collect data from lines{}
		counted = []
		for i, item in lines[cat].items():
			for word, wlitem in item['wordlist'].items():
				# print('>>> wlitem', wlitem, file=sys.stderr)
				if numerization == "TF":
					counted.append([word, wlitem['tf'], item['sentence']])
				if numerization == "TFIDF":
					counted.append([word, wlitem])
		tvars['counted_'+cat] = counted

	# the main dict that holds all the data
	tvars['lines'] = lines

	if numerization != "False":
		tvars['state'] = 'numerization'


	# *****************************************
	# main {} reference
	# *****************************************
	# 	lines = {
	# 	'a' : {
	# 		1 : {
	# 			'sentence': "this is terrible",
	# 			'words' : "this, is, terrible",
	# 			'wordlist' : {
	# 				'this' : {
	# 					'count-tf' : 0.33,
	# 					'count-tfidf' : 0.366204342551,
	# 					'sentences' : [
	# 						'sentence 1 with the word this',
	# 						'sentence 2 with the word this',
	# 						'sentence 3 with the word this',
	# 						'sentence 4 with the word this',
	# 						'sentence 5 with the word this',
	# 					]
	# 				},
	# 				'is' : {
	# 					'count-tf' : 0.33,
	# 					'count-tfidf' : 0.366204342551,
	# 					'sentences' : [
	# 						'sentence 1 with the word is',
	# 						'sentence 2 with the word is',
	# 						'sentence 3 with the word is',
	# 						'sentence 4 with the word is',
	# 						'sentence 5 with the word is',
	# 					],
	# 				},
	# 				'terrible' : {
	# 					'count-tf' : 0.33,
	# 					'count-tfidf' : 0.366204342551,
	# 					'sentences' : [
	# 						'sentence 1 with the word terrible',
	# 						'sentence 2 with the word terrible',
	# 						'sentence 3 with the word terrible',
	# 						'sentence 4 with the word terrible',
	# 						'sentence 5 with the word terrible',
	# 					],
	# 				},
	# 			},
	# 			'filter' : wordtype, 
	# 			'lemma' : lemma,
	# 			'document' : '',
	# 		}
	# 	 }
	# }


 	# *****************************************
 	# global lens

 	vectors = {}
 	sentences = {}
 	vectors['a'] = []
 	vectors['b'] = []
 	for cat in ['a','b']:
 		for i, item in lines[cat].items():
 			vector = item['vector']
 			sentence = item['sentence']
			# print('>>> vector', vector, file=sys.stderr)
 			vectors[cat].append([vector,sentence])

		# print('>>> vectors['+cat+']', vectors[cat], file=sys.stderr)
		tvars['vectors_'+cat] = group(sorted_vector(vectors[cat], cat))

# ****************************************************************************
# output
import datetime

if sentence_a != "":
	n = datetime.datetime.now()
	n.strftime("%Y-%m-%d-%H-%M-%S")
	n = str(n)
	n = n.replace(' ','_')
	n = n.replace(':','-')
	n = n.replace('.','_')

	for cat in ['a','b']:

		# *****************************************
		# export output.html
		
		title = title.replace(" ","_")

		folder = "output/"
		ext = ".html"
		filename = 'output_'+title+'_'+n+'_'+cat
		htmlname = folder+filename+ext
		with open (htmlname,'w+') as f:
			if cat == 'a':
				out = sentence_a
			if cat == 'b':
				out = sentence_b
			frame = '''
				<!DOCTYPE html>
				<html>
				<head>
					<meta charset="utf-8">
					<title> &larr; {{a_label}} A.I. {{b_label}} &rarr; </title>
					<link rel="stylesheet" type="text/css" href="../css/output.css">
					<script type="text/javascript" src="../js/jquery-2.1.3.js"></script>
					<script type="text/javascript" src="../js/output.js"></script>
				</head>
				<body>
					<div id="container">
						<div id="header">
							<div class="inline"><div id="title" class="tag">title: </div><div id="title" class="value"></div></div>
							<div class="inline"><div id="author" class="tag">author: </div><div id="author" class="value"></div></div>
							<div class="inline"><div id="filter" class="tag">included word types:</div><div id="filter" class="value"></div></div>
							<div class="inline"><div id="numerization" class="tag">numerization:</div><div id="numerization" class="value"></div></div>
						</div>
						<div id="wrapper">
						'''+out+'''
						</div>
					</div>
				</body>
				</html>
			'''
			if cat == 'a':
				print('>>> sentence_a', sentence_a, file=sys.stderr)
			if cat == 'b':
				print('>>> sentence_b', sentence_b, file=sys.stderr)
			f.write(frame.encode('utf8'))
			f.close()
			print('>>> output html written', out, file=sys.stderr)

			tvars['html_'+cat] = htmlname
			tvars['html_'+cat] = htmlname

		# *****************************************
		# export pdfs with wkhtmltopdf: html > pdf
		
		folder = "pdf/"
		ext = ".pdf"
		pdfname = folder+filename+ext
		cmd = "wkhtmltopdf "+htmlname+' '+pdfname
		print('>>> pdf generated', cmd, file=sys.stderr)
		os.system(cmd)

		tvars['pdf_'+cat] = pdfname
		tvars['pdf_'+cat] = pdfname

		# *****************************************
		# export graduallens pdf
		
		folder = "output/"
		ext = ".html"
		filename = 'output_'+title+'_'+n+'_graduallens'
		htmlname = folder+filename+ext
		with open (htmlname,'w+') as f:
			frame = '''
					<!DOCTYPE html>
					<html>
					<head>
						<meta charset="utf-8">
						<title> graduallens - &larr; {{a_label}} A.I. {{b_label}} &rarr; </title>
						<link rel="stylesheet" type="text/css" href="../css/stylesheet.css">
						<script type="text/javascript" src="../js/jquery-2.1.3.js"></script>
					</head>
					<body>
						'''+graduallens+'''
					</body>
					</html>
				'''
			f.write(frame.encode('utf8'))
			f.close()

		folder = 'pdf/'
		ext = '.pdf'
		pdfname = folder+filename+ext
		cmd = "wkhtmltopdf --grayscale "+htmlname+' '+pdfname
		print('>>> pdf graduallens generated', cmd, file=sys.stderr)
		os.system(cmd)

		tvars['pdf_graduallens'] = pdfname

# ****************************************************************************
# rendering

print ("Content-type: text/html;charset=utf-8")
print ()
html_out = template.render(tvars).encode("utf-8")
print (html_out)

# ****************************************************************************
# log

with open (path+'log/'+title+'_'+n+'.html','w+') as f:
	f.write(html_out)
	f.close()
