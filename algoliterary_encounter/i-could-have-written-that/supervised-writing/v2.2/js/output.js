function fontsize(){
	console.log('test');
	$('.outputword').each(function(){
		f = $(this).data('fontsize') * 200;
		$(this).css('font-size',f+'px');
	});
}
function details(){
	var title = $('.outputword').data('title'),
		author = $('.outputword').data('author'),
		filter = $('.outputword').data('filter'),
		numerization = $('.outputword').data('numerization'),
		cat = $('.outputword').data('cat'),
		label = $('.outputword').data(cat+'_label'),
		licence = $('.outputword').data('licence');
	$('#title.value').text(title);
	$('#author.value').text(author);
	// $('#licence.value').text(licence);
	$('#filter.value').text(filter);
	$('#numerization.value').text(numerization);
}
function metadata(){
	$('.outputword').each(function(){
		var name = $(this).attr('name'),
			position = $(this).offset(),
			width = $(this).width(),
			height = $(this).height(),
			word = $(this).data('word'),
			cat = $(this).data('cat'),
			label = $(this).data(cat+'_label'),
			tf = $(this).data('tf'),
			tfidf = $(this).data('tfidf'),
			numerization = $(this).data('numerization'),
			sentence = $(this).data('sentence');
		console.log(numerization);
		if(numerization == "TF"){
			count = tf;
		}
		if(numerization == "TFIDF"){
			count = tfidf;
		}
		var count = '<div>'+count+' </div>';
		var sentence_element = '<div>('+sentence+')</div>';
		$(this).append('<div id="'+name+'" class="metadata"></div>');
		var metadata = '.metadata#'+name;
		$(metadata).append(
			count
			+sentence_element
		);
	});
}
$(window).load(function(){
	fontsize();
	details();
	metadata();

	console.log('test');
	
});
