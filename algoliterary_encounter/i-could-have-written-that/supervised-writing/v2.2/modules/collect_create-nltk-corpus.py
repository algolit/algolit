###################################
# make your own corpus using nltk #
###################################

from nltk.corpus import PlaintextCorpusReader

def createCorpus(dir):
	corpus_root = dir
	wordlists = PlaintextCorpusReader(corpus_root, '.*')
	return wordlists.fileids()
	# print wordlists.words('struct.txt')
	# print wordlists.raw('struct.txt')

print createCorpus('corpus')

