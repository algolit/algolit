# -*- coding: utf-8 -*-
import csv
import nltk
import codecs

# read text file
with codecs.open('woolf.txt', 'rt', 'utf-8') as source:
#with codecs.open('joyce.txt', 'rt','utf-8') as source:
	text = source.read()
	text = text.replace("\r\n", " ")
	text = text.replace("\r", " ")
	text = text.replace("\n", " ")
	text = text.replace("  ", " ")
	text = text.replace("(", " ")
	text = text.replace(")", " ")


# split text into sentences
sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
sentences = sent_tokenizer.tokenize(text)
print len(sentences) 
## male: 5368
## female: 3699


# create documents of 10 sentences each
# nr = 10
# processed = [sentences[s:s + nr] for s in xrange(0, len(sentences), nr)]
# print len(processed[0])


# tag documents


with open('gender.csv', 'ab') as csvfile:
	spamwriter = csv.writer(csvfile, delimiter=',',
							quotechar='"', quoting=csv.QUOTE_ALL)
	for s in sentences:
		if len(s)>3:
			spamwriter.writerow([unicode(s).encode("utf-8"), "female"]) 
