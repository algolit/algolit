#############################
# collect tweets using nltk #
#############################

# ********************************************************
# README
# ********************************************************
# These functions are based on the Twitter API tutorial from nltk: 
# http://www.nltk.org/howto/twitter.html

# and use the nltk Twitter Client:
# http://www.nltk.org/_modules/nltk/twitter/twitterclient.html

# Settings
# see 'requirements.txt' to install Twython
# see 'twitter-files' to set the Twitter export path
# see 'twitter-files' to leave your twitter credentials

# Twitter provides two API's:
# - the Streaming API (called 'Streamer')
# 	  - public stream
# 	  - user stream
#	  - site stream
# - the Search API (called 'Query')

# Twitter API limits
# - the Streaming API: 1% of current nr. of tweets = +-60 tweets / minute 
# - the Search API (REST): 15 requests / 15 minutes

# possible languages to request: 
# https://dev.twitter.com/rest/reference/get/help/languages

# Twitter(): returns tweets based on a set of default settings (language = en, limit = 100)
# TweetViewer(): prints tweets to the terminal
# TweetWriter(): writes tweets to a files, by default into 'twitter-files'

# ********************************************************
# variables
# ********************************************************

keywordsStream = 'sad, happy' # comma indicates a logical OR
keywordsSearch = 'sad, happy' # comma indicates a logical AND
accounts = ['759251', '612473'] # @CCN + @BBCNews > https://tweeterid.com/

# ********************************************************
# Simple function for the Public Stream
# ********************************************************
# The following functions work in a more simple way with the public stream of Twitter.

from nltk.twitter import Twitter

def collectTweetsByKeywordStream(w, num):
	tw = Twitter()
	return tw.tweets(keywords=w, limit=num) 

def collectTweetsByKeywordPast(w, num):
	tw = Twitter()
	return tw.tweets(keywords=w, stream=False, limit=num)

def collectTweetsFromAccount(ids, num):
	tw = Twitter()
	return tw.tweets(follow=ids, limit=num)

def saveTweets():
	tw = Twitter()
	return tw.tweets(to_screen=False, limit=25)

# print collectTweetsByKeywordStream(keywordsStream, 10)
# print collectTweetsByKeywordPast(keywordsSearch, 10)
# print collectTweetsFromAccount(accounts, 10)
# print saveTweets() # saves to /home/user/twitter-files/

# ********************************************************
# Twitter Streaming API & Search API
# ********************************************************
# The following functions work in more detail with the Twitter API's, 
# and connect to the service with your API credentials.

from nltk.twitter import Query, Streamer, Twitter, TweetViewer, TweetWriter, credsfromfile
from pprint import pprint

subdir = 'modules/twitter-api'
oauth = credsfromfile(subdir=subdir) # connects to your credentials.txt

# --------------------------------------------------------
# Streaming API > "live" near real-time Twitter data
# --------------------------------------------------------
# https://dev.twitter.com/streaming/overview

def requestStreamingTweetsRandom(num):
	client = Streamer(**oauth) 
	client.register(TweetViewer(limit=num)) # view/save tweets
	return client.sample() # random sample of tweets

def requestStreamingTweetsFilter(w, num):
	client = Streamer(**oauth)
	client.register(TweetViewer(limit=num))
	return client.filter(track=w)

def requestStreamingTweetsFilterByLanguage(w, num, lang):
	client = Streamer(**oauth)
	client.register(TweetViewer(limit=num))
	tweets = client.filter(lang=[lang],track=w)
	return tweets

def saveTweetsStream(w, num):
	client = Streamer(**oauth)
	client.register(TweetWriter(limit=num))
	client.filter(track=w)

# print requestStreamingTweetsRandom(10)
# print requestStreamingTweetsFilter(keywordsStream, 10)
# print requestStreamingTweetsFilterByLanguage(keywordsStream, 10, ['en'])
# print saveTweetsStream(keywordsStream, 10)

# --------------------------------------------------------
# Search (REST) API > queries for past Tweets
# --------------------------------------------------------
# https://dev.twitter.com/rest/public

def requestSearchTweetsKeywords(w, num):
	# https://www.scribd.com/doc/30146338/map-of-a-tweet
	client = Query(**oauth)
	tweets = client.search_tweets(keywords=w, limit=num) 
	return tweets # dict

def requestUserInformation(ids,num):
	tmp = []
	client = Query(**oauth)
	user_info = client.user_info_from_id(ids)
	for info in user_info:
		name = info['screen_name']
		followers = info['followers_count']
		following = info['friends_count']
		tmp.append("{}, followers: {}, following: {}".format(name, followers, following))
	return tmp

tweets = requestSearchTweetsKeywords(keywordsSearch, 1)
for tweet in tweets:
	pprint(tweet, depth=10) # full tweet dict
	# print tweet['text'] # only the tweet's text
	# print tweet['lang'] # only the tweet's language
						# every tweet is 'EN' ???

# print requestUserInformation(accounts, 10)

# ********************************************************
# writing-machine.cgi
# ********************************************************
# function used for the writing machine v3
import os, datetime, json

# hack > write a timestamp locally, to be able to open the .json file later
# the timestamped_file functions from nltk (http://www.nltk.org/_modules/nltk/twitter/twitterclient.html)
def timestamped_file():
	"""
	:return: timestamped file name
	:rtype: str
	"""
	subdir = 'tweets'
	fprefix = 'tweets'
	if subdir:
		if not os.path.exists(subdir):
			os.mkdir(subdir)
	fname = os.path.join(subdir, fprefix)
	fmt = '%Y%m%d-%H%M%S' 
	timestamp = datetime.datetime.now().strftime(fmt)
	filename = '{0}.{1}.json'.format(fname, timestamp)
	return filename

# the language filter is based on a language_detection function in the Twitter API
# https://blog.twitter.com/2015/evaluating-language-identification-performance
def requestTweets(w, num, lang):
	client = Streamer(**oauth)
	filename = timestamped_file() # hack > write a timestamp just before the TweetWriter does so (in the same second! ha)
	client.register(TweetWriter(limit=num, subdir='tweets'))
	client.filter(lang=[lang],track=w)
	return filename

def openTweetsJson(filename):
	tmp = []
	lines = open(filename,'r').readlines()
	for line in lines:
		data = json.loads(line)
		tmp.append(data)
	return tmp # returns list containing json objects

