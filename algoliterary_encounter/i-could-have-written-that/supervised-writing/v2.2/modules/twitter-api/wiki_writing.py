#!/usr/bin/env/ python
# encoding=utf8  
import wikipedia
import sys
import requests
import json


# *******************************************

# ## Get the labels or classes

# a_label = raw_input('Please type label 1: ')
# b_label = raw_input('Please type label 2: ')

# *******************************************

# ## Search Wikipedia with the labels

# wiki_search_a = wikipedia.search(a_label)
# wiki_search_b = wikipedia.search(b_label)

# *******************************************

# ## Ask reader to select the specific Wikipedia page

# if len(wiki_search_a) > 1:
# 	sys.stdout.write("---------------------------------------------\n")
# 	sys.stdout.write("Label 1 has different entries in Wikipedia:\n")
# 	for result in wiki_search_a:
# 		sys.stdout.write(result + "\n")
# 	a_search = raw_input('Please specify your option from the list above: ')
# 	sys.stdout.write("---------------------------------------------\n")

# if len(wiki_search_b) > 1:
# 	sys.stdout.write("---------------------------------------------\n")
# 	sys.stdout.write("Label 2 also has different entries in Wikipedia:\n")
# 	for result in wiki_search_b:
# 		sys.stdout.write(result + "\n")
# 	b_search = raw_input('Please specify your option from the list above: ')
# 	sys.stdout.write("---------------------------------------------\n")

#print a_search, b_search

# *******************************************

a_search = "Artificial Intelligence"
b_search = "Machine Learning"

# *******************************************

## Get text from pages

# Summary
a_summary = wikipedia.summary(a_search)
b_summary = wikipedia.summary(b_search)
# print "summary label 1: ", a_summary
# print "\n"

# *******************************************

# only 1st sentence of summary
# wikipedia.summary(a_search, sentences=1)

# *******************************************

# Get syntax page (this is needed for all further extractions)
a_page = wikipedia.page(a_search)
b_page = wikipedia.page(b_search)
#print "a_page: ", a_page # <WikipediaPage 'Hair'>

# *******************************************

# Get title
a_title = a_page.title
b_title = b_page.title

# print "title page label 1: ", a_title
# print "title page label 2: ", b_title
# print "\n"

# *******************************************

# Get Content of a page
a_content = a_page.content
b_content = b_page.content

print "content page label a: ", a_content
print "\n"
print "content page label b: ", b_content
print "\n"

# *******************************************

# Get Links ########## DOES NOT WORK
# a_links = a_page.links
# print "links on page label 1: ", a_links
# print "\n"

# *******************************************

## Get url of the Wikipedia pages
a_url = a_page.url
# print "page url label 1: ", a_url

b_url = b_page.url
# print b_url

# *******************************************
## Get page revision history
## Find ID of current version of these pages
# a_id = a_page.revision_id
# print a_id

# # ## Find history of those Wikipages in json
# # use wikipedia API
# url = "https://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvlimit=5&rvprop=ids|flags|timestamp|comment|user&titles={0}&format=json".format(a_search)
# #webbrowser.open(url, new=0, autoraise=True)

# # get content
# response = requests.get(url)
# print (response.status_code)
# output = response.json()
# # output = response.content
# print output
# final = output["query"]["pages"]["3708"]["revisions"]


# *******************************************

# # save in json
# with open("wikidata.json", 'w') as f:
# 	f.write(json.dumps(final, sort_keys=True, indent=4, separators=(',', ': ')))

# *******************************************

# output_json = []
# for line in sys.stdin:
# 	item = json.loads(line)
# 	output_json.append(item)
# length_wiki = (len(output_json))
# print length_wiki



