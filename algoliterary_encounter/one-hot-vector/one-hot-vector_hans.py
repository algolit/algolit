import numpy as np
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer
import nltk

la = np.linalg

sentences = [
	"Vandaag hebben we neural networks bestudeerd",
	"Cristina was er ook, en Gijs niet", 
	"vandaag was het deep",
	"net zo deep als deep learning"
]

# sentences = ["I like deep learning.", "I like NLP.", "I enjoy flying."]

# unique words of the text
prematrix = set()

for sentence in sentences:
	print '> sentence: ', sentence
	words = sentence.split(" ")
	for word in words:
		word = word.lower()
		word = word.strip()
		prematrix.add(word)

print '> prematrix: ', prematrix

# order set & turn into list
pre2 = sorted(list(prematrix))

print '> pre2: ', pre2

# create bigrams
bigrams = []
for sentence in sentences:
	for b in nltk.bigrams(sentence.lower().split()):
		print '> bigram:', b
		bigrams.append(b)

print '> bigrams: ', bigrams

# create Co-occurence matrix
# create matrix with zeros, having the length of the vocabulary
X = np.zeros((len(pre2),len(pre2)), dtype=np.int)

print '> co-occurence matrix (empty): ', X

# for each bigram, add one
for b in bigrams:
	X[pre2.index(b[0]), pre2.index(b[1])] = X[pre2.index(b[0]),pre2.index(b[1])] + 1
	X[pre2.index(b[1]),pre2.index(b[0])] = X[pre2.index(b[1]),pre2.index(b[0])] + 1

print '> co-occurence matrix: ', X
