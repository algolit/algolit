﻿Dataset: astroBlackness. Source: Octavia E. Butler. Author: Gerry Canavan. Publisher: University of Illinois Press. Year: 2016. Page: 145. Paragraph: 3. 
…
[]
-
She returned over and over again to world-building aspects of the narrative — the social organization of the colony, the physics governing Bow, the possible organisms that might live in such a place, Earth-historical parallels to their situation—but could never get the plot itself to gel.
.
.

Dataset: Learning from Deep Learning. Source: Deep Learning. Author: Ian Goodfellow and Yoshua Bengio and Aaron Courville. Page: 472. Paragraph: 3. 
The idea of forming an embedding for a symbol was extended to the idea of an embedding for a word by Deerwester et al. These embeddings were learned using the SVD. Later, embeddings would be learned by neural networks. 

Dataset: astroBlackness. Source: Octavia E. Butler. Author: Gerry Canavan. Publisher: University of Illinois Press. Year: 2016. Page: 186. Paragraph: 3. 
More important than technique, however, is for authors to remember that they are writing about people. Authors who forget this, who do not relax and get comfortable with their racially different characters, can wind up creating unbelievable, self-consciously manipulated puppets; pieces of furniture who exist within a story but contribute nothing to it; or stereotypes guaranteed to be offensive. 

Dataset: Frankenstein. Source: Frankenstein. Author: Mary Shelley. 
Invention, it must be humbly admitted, does not consist in creating out of void, but out of chaos; the materials must, in the first place be afforded: it can give form to dark shapeless substances but cannot bring into being the substance itself.

Dataset: astroBlackness. Source: Octavia E. Butler. Author: Gerry Canavan. Publisher: University of Illinois Press. Year: 2016. Page: 46. Paragraph: 5. 
The sole competitor to the unchallenged hegemony of the Patternists, beyond the social instability implied by their own endless internal struggle for dominance, comes in the form of the Clayarks, mutated humans sick from an interplanetary virus, who are partially immune to Patternist control and who exist in the wild spaces between Patternist enclaves. 

Dataset: astroBlackness. Source: Social Text - Afrofuturist Issue. Author: Tracie Morris. Publisher: Duke University Press. Year: 2002. Page: 93. Paragraph: 1. 
My first word was an error according to the machine I spoke it in. 
Whispering into an orifice used to be intimate, 
Now, the Neural Network Noir twitters from every misplaced exclamation. 
Deep spell check. 

Dataset: nearbySaussure. Source: Saussure, Derrida, and the Metaphysics of Subjectivity. Author: Robert M. Strozier. Publisher: Mouton de Gruyter. Year: 1988. Page: 21. Paragraph: 3. 
“the linguist's task is not to study utterances for their own sake … but to provide evidence about the nature of the underlying system, the English language” 

Dataset: astroBlackness. Source: The Immeasurable Equation. Author: Sun Ra. Publisher: Hartmut Geerken. Year: 2005. Page: 306. 
· = aim 
· = end 
· = period 
· = time 
· = era 
· = age 
· = cycle 

Dataset: nearbySaussure. Source: The Cambridge Companion to Saussure. Author: Peter Wunderli. Publisher: Cambridge University Press. Year: 2006. Page: 21. Paragraph: 3. 
All these concessions open the door to coincidence and finally turn the anagram into a phenomenon of probability. In the end one is left wondering whether it is not possible to extract any word out of any text of a certain length. 



Dataset: nearbySaussure. Source: Saussure, Derrida, and the Metaphysics of Subjectivity. Author: Robert M. Strozier. Publisher: Mouton de Gruyter. Year: 1988. Page: 49. Paragraph: 2. 
It is the point of view, as Saussure says, which determines the fact or entity--the object--within the mass of data. “Point of view” is a technical term for Saussure: it is another term for form; that is, the form represents what is induced upon the mass of data, now considered as a material capable of accepting the various forms. 

Dataset: Learning from Deep Learning. Source: Deep Learning for Java. Author: Skymind. 
The vectors we use to represent words are called neural word embeddings, and representations are strange. One thing describes another, even though those two things are radically different. As Elvis Costello said: “Writing about music is like dancing about architecture”. 

Dataset: nearbySaussure. Source: Saussure's Philosophy of Language as Phenomenology. Author: Beata Stawarska. Publisher: Oxford University Press. Year: 2015. Page: 43. Paragraph: 1. Temporality proper to language itself intersects the signifier and the signified in a relation of mutual interdependency; this naturally makes the ideas themselves subject to mutation and change. Importantly, the source materials do not support the structuralist notion of a fixed and immutable system; such a notion emerges rather as the philosophers’ abstraction removed from the concrete reality of language in usage; 

Dataset: Frankenstein. Source: Frankenstein. Author: Mary Shelley. 
The different accidents of life are not so changeable as the feelings of human nature. 

Dataset: Learning from Deep Learning. Source: Deep Learning. Author: Ian Goodfellow and Yoshua Bengio and Aaron Courville. Page: 112. Paragraph: 1. 
In practice, the learning algorithm does not actually ﬁnd the best function, but merely one that signiﬁcantly reduces the training error. These additional limitations, such as the imperfection of the optimization algorithm, mean that the learning algorithm’s eﬀective capacity may be less than the representational capacity of the model family. 


Dataset: Frankenstein. Source: Frankenstein. Author: Mary Shelley. 
My education was neglected, yet I was passionately fond of reading. 



