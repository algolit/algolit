### Following this tutorial in Python3:
### https://gist.github.com/rspeer/ef750e7e407e04894cb3b78a82d66aed#file-how-to-make-a-racist-ai-without-really-trying-ipynb

'''
This script applies a trained model to textfiles.
It splits the text in sentences and predicts a sentiment score for each of the sentences / or for each of the words
The score & sentence/word are saved in a file, ordered from small to big scores 
clean your text using adapting_the_reading_glasses.py
'''

### FOR THE STORYTELLER: uncomment ##time.sleep + print i/line on 45/46

import numpy as np
import pandas as pd
import re
import nltk
import nltk.data

from sklearn.linear_model import SGDClassifier
from sklearn.externals import joblib


### FUNCTIONS

### WRITING
### ------------

def archive(sentence, filename):
	with open(filename, "a") as destination:
		destination.write(sentence)


### LOAD GloVE word Embeddings
### ---------------------------

def load_embeddings(filename):
	"""
	Load a DataFrame from the generalized text format used by word2vec, GloVe,
	fastText, and ConceptNet Numberbatch. The main point where they differ is
	whether there is an initial line with the dimensions of the matrix.
	"""
	labels = []
	rows = []
	with open(filename, 'r') as infile:
		for i, line in enumerate(infile):
			# print(i)
			#print(line)
			items = line.rstrip().split(' ')
			if len(items) == 2:
				# This is a header row giving the shape of the matrix
				continue
			labels.append(items[0])
			values = np.array([float(x) for x in items[1:]], 'f')
			rows.append(values)
	arr = np.vstack(rows)
	return pd.DataFrame(arr, index=labels, dtype='f')


### Combine sentiments for word vectors into an overall sentiment score by averaging them
### --------------------------------------------------------------------------------------
TOKEN_RE = re.compile(r"\w.*?\b")
# The regex above finds tokens that start with a word-like character (\w), and continues
# matching characters (.+?) until the next word break (\b). It's a relatively simple
# expression that manages to extract something very much like words from text.
def text_to_sentiment(text):
	tokens = [token.casefold() for token in TOKEN_RE.findall(text)]
	sentiments = words_to_sentiment(tokens)
	return sentiments['sentiment'].mean()


### ------------------------------------------

### ACTIONS	

### LOAD EMBEDDINGS
#playsound('/path/to/file/you/want/to/play.wav')
embeddings = load_embeddings('data/glove.840B.300d.txt')
#embeddings = load_embeddings('data/glove.42B.300d.txt')
#embeddings = load_embeddings('data/glovesample.txt')
#playsound('/path/to/file/you/want/to/play.wav')

filename = 'data/1984_all_stripped.txt'
#filename = 'data/1984_fragment.txt'
#filename = 'data/frankenstein_for_machines.txt'
pos_output = filename.replace('.txt','_pos.txt')
neg_output = filename.replace('.txt','_neg.txt')
scored_words = filename.replace('.txt','_scored_words.txt')


### SCORE SENTENCES
### ---------------

# load model using joblib
loaded_model = joblib.load('sentiment_thermometer_glove.pkl') 
# get sentence

finding_sentences = nltk.data.load('tokenizers/punkt/english.pickle')
sentences_list = []
all_sents = []

with open(filename, "r") as source:
	for line in source:
		if line:
			print("line", line)
			# this returns a list with 1 element containing the entire text, sentences separated by \n
			sentences = '\n'.join(finding_sentences.tokenize(line.strip()))
			#print("sentences", sentences)
			# transform string into list of sentences
			sentences_list = sentences.split("\n") 
			#print('sentences_list', sentences_list)
			all_sents.append(sentences_list)

pos_scores = set()
neg_scores = set()
## CHECK: WHY DOES IT PRINT DTYPE = FLOAT
for sentences in all_sents:
	if sentences:
		for sentence in sentences:
		# tokenize sentence
			if sentence:
				TOKEN_RE = re.compile(r"\w.*?\b")
				tokens = [token.casefold() for token in TOKEN_RE.findall(sentence)]
				tokens_set = set(tokens)
				# look for words of given sentence in word embeddings
				vecs = embeddings.loc[tokens].dropna()
				# find logarithmic scores
				predictions = loaded_model.predict_log_proba(vecs)
				# print("tot1", predictions[:, 1])
				# print("tot0", predictions[:, 0])
				log_odds = predictions[:, 1] - predictions[:, 0]
				#print("log_odds", log_odds)

				# print score for each word
				#print(pd.DataFrame({'': log_odds}, index=vecs.index))
				# print average score for sentence
				score = pd.DataFrame({':': log_odds}, index=vecs.index).mean()*10
				scorestr = str(score)
				clean_score = re.sub('[^0-9\.\-]', '', scorestr)
				scores = [clean_score, sentence]

				#sort sentences in pos/neg scores
				if int(pd.DataFrame({'': log_odds}).mean()) > 0.00:
					pos_scores.add(tuple(scores))
				else:
					neg_scores.add(tuple(scores))

				# print(sentence, "\t", clean_score)
				# print("\n")

#sort pos/net sentences+scores in ascending order
pos_scores = sorted(pos_scores)
print("positive scores", pos_scores)
neg_scores = sorted(neg_scores)
print("neg scores", neg_scores)

#write to file
for s in pos_scores:
	archive(s[0], pos_output)
	archive("\t", pos_output)
	archive(s[1], pos_output)
	archive("\n", pos_output)
for s in neg_scores:
	archive(s[0], neg_output)
	archive("\t", neg_output)
	archive(s[1], neg_output)
	archive("\n", neg_output)