### Following this tutorial in Python3:
### https://gist.github.com/rspeer/ef750e7e407e04894cb3b78a82d66aed#file-how-to-make-a-racist-ai-without-really-trying-ipynb

'''
This script applies a trained model to textfiles.
It splits the text in sentences and predicts a sentiment score for each of the sentences / or for each of the words
The score & sentence/word are saved in a file, ordered from small to big scores 
clean your text using adapting_the_reading_glasses.py
'''


import numpy as np
import pandas as pd

from sklearn.linear_model import SGDClassifier
# from sklearn.model_selection import train_test_split
# from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score
from sklearn.externals import joblib


### FUNCTIONS

### LOAD GloVE word Embeddings
### ---------------------------

def load_embeddings(filename):
	"""
	Load a DataFrame from the generalized text format used by word2vec, GloVe,
	fastText, and ConceptNet Numberbatch. The main point where they differ is
	whether there is an initial line with the dimensions of the matrix.
	"""
	labels = []
	rows = []
	with open(filename, 'r') as infile:
		for i, line in enumerate(infile):
			# print(i)
			#print(line)
			items = line.rstrip().split(' ')
			if len(items) == 2:
				# This is a header row giving the shape of the matrix
				continue
			labels.append(items[0])
			values = np.array([float(x) for x in items[1:]], 'f')
			rows.append(values)
	arr = np.vstack(rows)
	return pd.DataFrame(arr, index=labels, dtype='f')


### ------------------------------------------

### ACTIONS	

### LOAD EMBEDDINGS
embeddings = load_embeddings('data/glove.840B.300d.txt')
#embeddings = load_embeddings('data/glove.42B.300d.txt')
#embeddings = load_embeddings('data/glovesample.txt')

#filename = 'data/1984_all_stripped.txt'
#filename = 'data/1984_fragment.txt'
filename = 'data/frankenstein_for_machines.txt'
pos_output = filename.replace('.txt','_pos.txt')
neg_output = filename.replace('.txt','_neg.txt')
scored_words = filename.replace('.txt','_scored_words.txt')


### SCORE WORDS
### ---------------

# load model using joblib
loaded_model = joblib.load('sentiment_thermometer_glove.pkl') 

# get text & turn into set of unique words
all_words = set()
with open(filename, "r") as source:
	for line in source:
		if line:
			#print("line", line)
			# return word list
			words = line.strip().split()
			# make set of unique words of the text
			for word in words:
				all_words.add(word)

# look for words of given sentence in word embeddings
vecs = embeddings.loc[all_words].dropna()
# find logarithmic scores
predictions = loaded_model.predict_log_proba(vecs)
# print("tot1", predictions[:, 1])
# print("tot0", predictions[:, 0])
log_odds = predictions[:, 1] - predictions[:, 0]
#print("log_odds", log_odds)

# print score for each word
word_scores = pd.DataFrame({'': log_odds}, index=vecs.index)
# write words+scores to file
word_scores.to_csv(scored_words, sep='\t', encoding='utf-8')
print("done")

				