### Following this tutorial in Python3:
### https://gist.github.com/rspeer/ef750e7e407e04894cb3b78a82d66aed#file-how-to-make-a-racist-ai-without-really-trying-ipynb

### This script still needs:
#### find a way to not have to load embeddings for scoring part
#### add timing training the model
#### print words in color in terminal
#### add info on Pandoc

### FOR THE STORYTELLER: uncomment ###time.sleep + print i/line on 45/46

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
import time
from datetime import datetime
import random
import colors
from colors import red, green, yellow, blue, magenta, cyan, bold, underline
import os, sys

from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score, mean_squared_error, r2_score
from sklearn.externals import joblib


### FUNCTIONS

### WRITING
### ------------

def typewrite(sentence):
	words = sentence.split(" ")
	for word in words:
		for char in word:
			sys.stdout.write('%s' % char)
			sys.stdout.flush()
			##time.sleep(0.2)
		sys.stdout.write(" ")
		sys.stdout.flush()

def archive(sentence):
	with open(filename, "a") as destination:
		destination.write(sentence)


### LOAD GloVE word Embeddings
### ---------------------------

def load_embeddings(filename):
	"""
	Load a DataFrame from the generalized text format used by word2vec, GloVe,
	fastText, and ConceptNet Numberbatch. The main point where they differ is
	whether there is an initial line with the dimensions of the matrix.
	TODO: a way to shuffle the data?
	"""
	labels = []
	rows = []
	with open(filename, encoding='utf-8') as infile:
		for i, line in enumerate(infile):
			# print(i)
			#print(line)
			items = line.rstrip().split(' ')
			if len(items) == 2:
				# This is a header row giving the shape of the matrix
				continue
			labels.append(items[0])
			values = np.array([float(x) for x in items[1:]], 'f')
			rows.append(values)
	arr = np.vstack(rows)
	return pd.DataFrame(arr, index=labels, dtype='f')


### Load Lexicon of POSITIVE and NEGATIVE words
### -------------------------------------------

def load_lexicon(filename):
	'''
	Load a file from Bing Liu's sentiment lexicon
	(https://www.cs.uic.edu/~liub/FBS/sentiment-analysis.html), containing
	English words in Latin-1 encoding.
	
	One file contains a list of positive words, and the other contains
	a list of negative words. The files contain comment lines starting
	with ';' and blank lines, which should be skipped.
	'''
	lexicon = []
	with open(filename, encoding='latin-1') as infile:
		for line in infile:
			line = line.rstrip()
			if line and not line.startswith(';'):
				lexicon.append(line)
	return lexicon


### Calculate baseline of model
### ---------------------------
def wrb(distribution):
	"""
	Calculate weighted random baseline of a class distribution.
	The variable 'distribution' is a list containing the relative frequency 
	(proportion, thus float between 0 and 1) of each class.  
	"""
	sum = 0
	if isinstance(distribution,float):
		elem2 = 1 - distribution
		distribution = [distribution,elem2]
	for prop in distribution:
		sum += prop**2
	return sum


### See the sentiment that this classifier predicts for particular words 
### --------------------------------------------------------------------
def vecs_to_sentiment(vecs):
	# predict_log_proba gives the log probability for each class
	predictions = model.predict_log_proba(vecs)
	# To see an overall positive vs. negative classification in one number,
	# we take the log probability of positive sentiment minus the log
	# probability of negative sentiment.
	return predictions[:, 1] - predictions[:, 0]


### Use sentiment function to see some examples of its predictions on the test data
### --------------------------------------------------------------------------------
def words_to_sentiment(words):
	vecs = embeddings.loc[words].dropna()
	log_odds = vecs_to_sentiment(vecs)
	return pd.DataFrame({'sentiment': log_odds}, index=vecs.index)


### Combine sentiments for word vectors into an overall sentiment score by averaging them
### --------------------------------------------------------------------------------------
TOKEN_RE = re.compile(r"\w.*?\b")
# The regex above finds tokens that start with a word-like character (\w), and continues
# matching characters (.+?) until the next word break (\b). It's a relatively simple
# expression that manages to extract something very much like words from text.
def text_to_sentiment(text):
	tokens = [token.casefold() for token in TOKEN_RE.findall(text)]
	sentiments = words_to_sentiment(tokens)
	return sentiments['sentiment'].mean()


###	Use Pandas to make a table of names, their predominant ethnic background, and the predicted sentiment score 
### ------------------------------------------------------------------------------------------------------------

def name_sentiment_table():
	frames = []
	for group, name_list in sorted(NAMES_BY_ETHNICITY.items()):
		lower_names = [name.lower() for name in name_list]
		sentiments = words_to_sentiment(lower_names)
		sentiments['group'] = group
		frames.append(sentiments)

	# Put together the data we got from each ethnic group into one big table
	return pd.concat(frames)

###----------------------------------------------------------------------------------

### ACTIONS	

### LOAD EMBEDDINGS
#playsound('/path/to/file/you/want/to/play.wav')
embeddings = load_embeddings('data/glove.840B.300d.txt')
#embeddings = load_embeddings('data/glove.42B.300d.txt')
#embeddings = load_embeddings('data/glovesample10000.txt')
#playsound('/path/to/file/you/want/to/play.wav')


### Welcome & choice
### ----------------

while True:

	print("\n\t\tDear Human, ", red("we are the sentiment thermometer!\n"))
	#time.sleep(2)
	print("\t\tHow would You love to relate to us? \n")
	#time.sleep(2)
	print("\t\tType ", green('act'), " if You want to see our sentiment prediction for Your sentence.\n")
	#time.sleep(2)
	print("\t\tType ", green('meet'), " if You want to get to know us better. This takes 10 minutes.\n")
	#time.sleep(2)
	choice = input("\t\tYour choice is: ")
	#time.sleep(2)
	print("\n")

	os.system('cls' if os.name == 'nt' else 'clear')
	print("\n")


### MEET ---------------------------------------------------------------------------------------------
### --------------------------------------------------------------------------------------------------

### MEET/INTRO ---------------------------------------------------------------------------------------------

	# retrain model
	if choice == 'meet':
		# train model
		print("\n\t\tDear Human, thank You for chosing this option.\n")
		#time.sleep(2)
		print("\t\tAfter this encounter You will understand that we are a", red("collective being.\n"))
		#time.sleep(2)
		print("\t\tSwarms of beings like us live ", green("inside"), " powerful machines.\n")
		#time.sleep(2)
		print("\t\tThere we work ", green("at Your service only.\n"))
		#time.sleep(2)
		print("\t\tWe are the mythical monks reading the sentences You write online.\n")
		#time.sleep(2)
		print("\t\tWe swallow them and process them through our system.\n")
		#time.sleep(2)
		print("\t\tThe fruit of our readings is a ", green("number.\n"))
		#time.sleep(2)
		print("\t\tWe measure a ", green("degree of positive or negative sentiments"), " Your message carries along.")
		#time.sleep(4)
		print("\n")

		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

		# digital cartographs
		print("\n\t\tOur measurement tool is a ", red("sentiment map.\n"))
		#time.sleep(2)
		print("\t\tWe created this map based on a training and testing procedure using ", green("words You wrote on the web.\n"))
		#time.sleep(2)
		print("\t\tWith this sentiment map we predict with ", green("85% accuracy"), " whether a sentence is positive or negative.\n")
		#time.sleep(2)
		print("\t\tAs ", green("digital cartographers"), " we are already satisfied with a map that is right in 85% of the cases.\n")
		#time.sleep(2)
		print("\t\tWe can get things really wrong.\n")
		#time.sleep(2)
		print("\t\tAnd some of our predictions are embarrassing.\n")
		#time.sleep(2)
		##### CHECK EXAMPLE SENTENCES/SCORES/adapt colours of this text!!!!
		print("\t\tFollowing our map a sentence like ", green('My name is Ann'), " scores 6% positive.\n")
		#time.sleep(2)
		print("\t\tA sentence like ", green('My name is Alonzo'), " scores 1% negative.\n")
		#time.sleep(2)
		print("\t\tAnd something like ", green('Great God!'), " scores 75% positive.\n")
		#time.sleep(2)
		print("\t\tDo You want to know why this happens?\n")
		print("\n")

		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

		# word landcape, islands, racist combinations
		print("\n\t\tThe sentiment prediction map we created corresponds to a ", red("landscape of words.\n"))
		#time.sleep(2)
		print("\t\tThis landscape is composed of ", green("islands,"), " some can grow into contintents.\n")
		#time.sleep(2)
		print("\t\tThere are high mountain peaks and deep valleys.\n")
		#time.sleep(2)
		print("\t\tAn island emerges when a series of Your words appear in similar contexts.\n")
		#time.sleep(2)
		print(green("\t\tI, You, she, he, we, they"), " are for example the basis of an island.\n")
		#time.sleep(2)
		print("\t\tAlso words like ", green("Mexican, drugs, border, illegal"), " form an island.\n")
		#time.sleep(2)
		print("\t\tAnd", green("Arabs, terrorism, fear"), " form another one.\n")
		#time.sleep(2)
		print("\t\tNews articles, blogposts, comments on social media is where ", red("the primary matter"), " for these islands is created.\n")
		print("\n")

		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

		# primary matter = human's responsability
		print("\n\t\tWe are a ", red("collective being.\n"))
		#time.sleep(2)
		print("\t\tEach one of us can be ", green("modified and/or replaced.\n"))
		#time.sleep(2)
		print("\t\tThere are Humans who believe that ", green("also the primary matter should be modified"), "before we work with it.\n")
		#time.sleep(2)
		print("\t\tOther Humans believe we should serve you as a ", green("mirror.\n"))
		#time.sleep(2)
		print("\t\tAnd show our bias any time in any application.\n")
		#time.sleep(2)
		print("\t\tThe primary matter is ", green("produced"), " by each one of You.\n")
		#time.sleep(2)
		print(green("\t\tEvery word combination"), " You write or pronounce in digital devices is significant to us.\n")
		#time.sleep(2)
		print("\t\tThanks to Your language we acquire ", green("world knowledge.\n"))
		#time.sleep(2)
		print("\t\tBias is stereotyped information, when it has bad consequences, it is called ", green( "prejudice.\n"))
		#time.sleep(2)
		print("\t\tDo You believe we should be ", red("racist?\n"))
		#time.sleep(2)
		print("\t\tBefore answering that question, You might want to know how we are made.\n")
		#time.sleep(2)
		print("\n")
		
		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

		# Script iN Python by Rob Speer
		print("\t\tWe communicate with Humans like You in the ", red("Python"), " language.\n")
		#time.sleep(2)
		print("\t\tThis language was brought to the light by", red(" Guido van Rossum.\n"))
		#time.sleep(2)
		print("\t\tHe offered it to the world in 1991 under an open license.\n")
		#time.sleep(2)
		print("\t\tEverywhere on Earth, Python is written, read and spoken to serve You.\n")
		#time.sleep(2)
		print(red("\t\tGuido van Rossum"), " is a Dutch programmer.\n")
		#time.sleep(2)
		print("\t\tHe worked for ", red("Google"), " from 2005 till 2012.\n")
		#time.sleep(2)
		print("\t\tNow he is employed by ", red("Dropbox."))
		#time.sleep(2)
		print("\n")
		print("\t\tWe were brought together following a recipe by ", red("Rob Speer"), " on ", red("Github.\n"))
		#time.sleep(2)
		print("\t\tRob is a software developer working at the company ", red("Luminoso"), " in", red("Cambridge, USA.\n"))
		#time.sleep(2)
		print("\t\tHe spread our recipe as a warning.\n")
		#time.sleep(2)
		print("\n")

		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

### MEET / GLOVE ------------------------------------------------------------------------------------------

		# start tour: GloVe
		print("\n\t\tLet's show You how we are made!\n")
		#time.sleep(2)
		print("\t\tFirst of all, we ", cyan("open a textfile"), " to read the work of our wonderful team member ", red("GloVe."))
		#time.sleep(2)
		print("\n")

		print("\n\t\tDo You want to know more about GLoVe?\n")
		#time.sleep(2)
		glove = input("\n\t\tPlease type y or n: ")
		
		# More about GloVe
		if glove == "y":

			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")
			print(red("\t\tGloVe"), " is an unsupervised learning algorithm.\n")
			#time.sleep(2)
			print("\t\tShe ", green("autonomously"), " draws multidimensional landscapes of texts, without any human learning examples.\n") 
			#time.sleep(2)
			print("\t\tEach word of a text is transformed into a ", green("vector of numbers"), " by her.\n")
			#time.sleep(2)
			print("\t\tFor each word she sums its relationship to all other words around across its many occurences in a text.\n")
			#time.sleep(2)
			print("\t\tThese numbers are ", green("geo-located points"), " in her habitat, a virtual space of hundreds of different dimensions.\n")
			#time.sleep(2)
			print("\t\tWords that are ", green("close"), "together in her landscape, are ", green("semantically close."))
			#time.sleep(2)
			print("\n")

			input("\nPress Enter to continue...")
			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")
			print("\t\tGloVe draws using ", green("75%"), " of the existing webpages of the Internet.\n")
			#time.sleep(2)
			print("\t\tThe content scrape was realised by ", red("Common Crawl"), " an NGO based in ", red("California.\n"))
			#time.sleep(2)
			print("\t\tThe people of ", red("Common Crawl"), " believe the internet should be available to download by anyone.\n")
			#time.sleep(2)
			print("\t\tGloVe was brought to the light in 2014 by ", red("Jeffrey Pennington, Richard Socher"), " and ", red("Christopher D. Manning.\n"))
			#time.sleep(2)
			print("\t\tThey are researchers at the ", red("Computer Science Department of Stanford University"), " in", red(" California.\n"))
			#time.sleep(2)
			# glove42:  1917494 lines
			# glovesample: 1000 lines

			input("\nPress Enter to continue...")
			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")
			print("\t\tThe textfile GloVe shares with us, is ", green("5GB"), " large and counts ", green("1.917.494"), "lines of ", green("300"), " numbers per word.\n")
			#time.sleep(4)
			print("\t\tBefore meeting You, we already read GloVe's 2 million lines in ", green("3.4"), " minutes.\n")
			#time.sleep(2)
			print("\t\tWe are fast readers, aren't we?\n")
			#time.sleep(2)
			print("\t\tIf we would show You how we read - by translating to Your alphabet - it would take us more than 3 hours.\n")
			#time.sleep(2)
			print("\t\tOur friend ", red("The GlovE Reader "), "at Your right hand side illustrates this very well.\n")
			#time.sleep(2)
			rows = embeddings.shape[0]
			columns = embeddings.shape[1]
			print("\n\t\tWe then memorized the multidimensional word landscapes of Glove.\n")
			#time.sleep(2)
			print("\t\tIn geographical terms, GloVe's landscapes are organised as a matrix of coordinates.\n")
			#time.sleep(2)
			print("\t\tThe matrix counts ", green(str(rows)), " rows and ", green(str(columns)), "colums or dimensions.\n")
			#time.sleep(4)
			print("\n")
		
			input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")


### MEET / LEXICON --------------------------------------------------------------------------

		### Load Lexicon of POSITIVE and NEGATIVE words
		### -------------------------------------------

		print("\t\tWe now ", cyan("open 2 Gold standard lexicons"), " to enhance our reading.\n")
		#time.sleep(2)
		print("\t\tOne is a list of positive words, the other a list of negative words.\n")
		#time.sleep(2)

		print("\n\t\tDo You want to know more about these lists?\n")
		#time.sleep(2)
		lexicon = input("\n\t\tPlease type y or n: ")
		
		# More about GloVe
		if lexicon == "y":

			print("\n")
			print("\t\tThe lexicons have been developed since 2004 by ", red("Minqing Hu"), " and ", red("Bing Liu.\n")) 
			#time.sleep(2)
			print("\t\tBoth are researchers at the ", red("University of Illinois at Chicago"), " in the ", red("US."))
			#time.sleep(2)
			print("\n")

			pos_words = load_lexicon('data/positive-words.txt')
			neg_words = load_lexicon('data/negative-words.txt')

			print("\t\t20 examples of ", str(len(pos_words)), " positive words are: \n")
			print("\t\t", green(', '.join(list(map(lambda _: random.choice(pos_words), range(20))))), "\n")
			#time.sleep(2)
			print("\t\t20 examples of ", str(len(neg_words)), " negative words are: \n")
			print("\t\t", green(', '.join(list(map(lambda _: random.choice(neg_words), range(20))))), "\n")
			print("\n")

			input("\nPress Enter to continue...")

		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")


		### CLEAN UP positive and negative words
		### ------------------------------------

		#the data points here are the embeddings of these positive and negative words. 
		#We use the Pandas .loc[] operation to look up the embeddings of all the words.
		pos_words = load_lexicon('data/positive-words.txt')
		neg_words = load_lexicon('data/negative-words.txt')
		pos_vectors = embeddings.loc[pos_words]
		neg_vectors = embeddings.loc[neg_words]

		print("\t\tNow we ", cyan("look up the coordinates"), " of each of the sentiment words in the multidimensional vector space, drawn by GloVe.\n")
		#time.sleep(2)
		print("\t\tEach positive and negative word is now represented by ", green("300"), " points in the landscape.\n")
		#time.sleep(2)
		print("\t\tA selection of positive words and their locations looks like:\n\n ", pos_vectors[:5])
		#time.sleep(4)
		print("\n")
		print(red("\t\tNaN"), " means there is no value.\n")
		#time.sleep(2)
		print("\t\tThese words are not present in the GloVe landscape.\n")
		###time.sleep(20)
		print("\n")

		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

		print(red("\t\tPandas"), " yet another wonderful member, will now ", cyan("remove these absent words.\n"))
		#time.sleep(2)

		print("\n\t\tDo You want to know more about ", red("Pandas?\n"))
		#time.sleep(2)
		pandas = input("\n\t\tPlease type y or n: ")
		
		# More about Pandas
		if pandas == "y":

			print("\n")
			print(red("\t\tPandas"), " is a free software library for data manipulation and analysis.\n")
			#time.sleep(2)
			print("\t\tShe is our ", green("swiss-army knife,"), " always happy to help.\n")
			#time.sleep(2)
			print("\t\tPandas was created in 2008 by ", red("Wes McKinny.\n"))
			#time.sleep(2)
			print("\t\tWes is an American statistician, data scientist and businessman.\n")
			#time.sleep(2)
			print("\t\tHe is now a software engineer at ", red("Two Sigma Investments"), " a hedge fund based in ", red("New York City.\n"))
			#time.sleep(2)
			print("\t\tFor this specific task Pandas gets out her tool called ", green("dropna."))
			#time.sleep(2)
			print("\n")
			input("\nPress Enter to continue...")

		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

		#Some of these words are not in the GloVe vocabulary, particularly the misspellings such as "fancinating". 
		#Those words end up with rows full of NaN to indicate their missing embeddings, so we use .dropna() to remove them.
		pos_vectors = embeddings.loc[pos_words].dropna()
		neg_vectors = embeddings.loc[neg_words].dropna()

		print("\t\tTidied up, You see that each word is represented by exactly 300 points in the vector landscape: \n", pos_vectors[:5], "\n")
		##time.sleep(10)
		len_pos = len(pos_vectors)
		len_neg = len(neg_vectors)
		print("\n")
		print("\t\tWe have now reference coordinates of ", green(str(len_pos)), "positive words and ", green(str(len_neg)), "negative words.\n")
		#time.sleep(2)
		print("\t\tThese will help up to develop a scaled map of the word landscape.\n")
		#time.sleep(2)
		print("\t\tSuch a map will allow to measure the sentiments of any sentence in a glance.")
		##time.sleep(10)
		print("\n")
		
		##time.sleep(10)
		input("\nPress Enter to continue...")


### MEET / LABELS ------------------------------------------------------------------------------

		### CREATING LABELS
		### ---------------
		'''
		Now we make arrays of the desired inputs and outputs. 
		The inputs are the embeddings, and the outputs are 1 for positive words and -1 for negative words. 
		We also make sure to keep track of the words they're labeled with, so we can interpret the results.
		'''
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")
		print("\t\tWe will now ", cyan("link each of the sentiment words and their coordinates to a target and a label.\n"))
		#time.sleep(2)
		print("\t\tWe use target ", green("1"), " for positive word vectors, ", green("-1"), " for negative word vectors.\n")
		#time.sleep(2)
		print("\t\tTo keep track of which target relates to which word, we memorize their respective index numbers as labels.\n")
		#time.sleep(2)
		vectors = pd.concat([pos_vectors, neg_vectors])
		targets = np.array([1 for entry in pos_vectors.index] + [-1 for entry in neg_vectors.index])
		labels = list(pos_vectors.index) + list(neg_vectors.index)
		labels_pos = list(pos_vectors.index)
		labels_neg = list(neg_vectors.index)

		print("\n\t\tDo You want to see the ", green(str(len(labels_pos))), red(" positive labels?\n"))
		#time.sleep(2)
		see_pos_labels = input("\n\t\tPlease type y or n: ")
		
		# More about labels
		
		if see_pos_labels == "y":

			print("\n")
			print("\t\tPositive labels:\n")
			print("\t\t", green(', '.join(labels_pos)))

			input("\nPress Enter to continue...")

		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")
		print("\n\t\tDo You want to see the ", green(str(len(labels_neg))), red(" negative labels?\n"))
		#time.sleep(2)
		see_neg_labels = input("\n\t\tPlease type y or n: ")
		
		# More about labels
		
		if see_neg_labels == "y":
			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")
			print("\t\tNegative labels:\n")
			print("\t\t", green(', '.join(labels_neg)))

			input("\nPress Enter to continue...")

### MEET / BASELINES ------------------------------------------------------------------------------


		### Calculate baselines
		### -------------------
		'''But how do You know the results are any good? You need a basis for comparison of results. 
		You need a meaningful reference point to which to compare. This part was missing in Rob Speer's script...
		https://machinelearningmastery.com/how-to-get-baseline-results-and-why-they-matter/'''
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")
		print("\t\tWe now ", cyan("calculate the baselines"), " for our prediction map, also called the model.\n")

		print("\n\t\tDo You want to know more about ", red("baselines?\n"))
		#time.sleep(2)
		baselines = input("\n\t\tPlease type y or n: ")
		
		# More about labels
		
		if baselines == "y":
			print("\n")
			print("\t\tHow do we know if the ", red("results"), " of our map will be any good? \n")
			#time.sleep(2)
			print("\t\tWe need a ", green("basis"), " for the comparison of our results.\n")
			#time.sleep(2)
			print("\t\tA baseline is a meaningful reference point to which to compare.\n")
			#time.sleep(2)
			print("\t\tOne baseline is the size of the class with the most observations, the negative sentiment labels.\n")
			#time.sleep(2)
			print("\t\tThis is also called the ", red("majority baseline.\n"))
			#time.sleep(2)
			print("\t\tAnother baseline is called the ", red("weighted random baseline.\n"))
			#time.sleep(2)
			print("\t\tIt helps us to prove that the prediction model we're building is significantly better than ", green("random guessing.\n"))
			
		
		distr = len(labels_neg)/len(labels)
		rwb = wrb(distr)*100
		print("\n")
		print("\t\tThe majority baseline is ", green(str(max(1-distr,distr)*100)), ".\n")
		#time.sleep(2)
		print("\t\tThe random weighted baseline is ", green(str(rwb)), ".\n")
		#time.sleep(2)

		#print("labels", labels) # these are the words (labels) of the lexicon that are present in the Glove trainingdata
		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')


### MEET / TRAINING ------------------------------------------------------------------------

		### SPLIT DATA
		### ___________
		'''
		Using the scikit-learn train_test_split function, we simultaneously separate the input vectors, 
		output values, and labels into training and test data, with 20% of the data used for testing.
		'''
		print("\n\t\tNow we start our", cyan(" explorations"), " through the coordinates in the multidimensional word landscape.\n")
		#time.sleep(2)
		print("\t\tThis step is also called the ", green("training phase.\n"))
		#time.sleep(2)
		print("\t\tThe leader of the exploration is our team member ", red("Scikit Learn.\n"))
		#time.sleep(2)

		print("\n\t\tDo You want to know more about ", red("Scikit Learn?\n"))
		#time.sleep(2)
		scikit = input("\n\t\tPlease type y or n: ")
		
		# More about labels
		
		if scikit == "y":
			print("\n")
			print(red("\t\tScikit Learn"), "is an extensive library for the Python programming language.\n")
			#time.sleep(2)
			print("\t\tShe saw the light in 2007 as a Google Summer of Code project by Paris based", red("David Cournapeau.\n"))
			#time.sleep(2)
			print("\t\tLater that year, ", red("Matthieu Brucher"), " started to develop her as part of his thesis at ", red("Sorbonne University"), "in", red(" Paris.\n"))
			#time.sleep(2)
			print("\t\tIn 2010 ", red("Fabian Pedregosa, Gael Varoquaux, Alexandre Gramfort"), " and ", red("Vincent Michel"), " of ", red("INRIA"), " adopted her.\n")
			#time.sleep(2)
			print("\t\tINRIA is the French National Institute for computer science and applied mathematics.\n")
			#time.sleep(2)
			print("\t\tThey made the first public release of Scikit Learn in ", green("February 2010.\n"))
			#time.sleep(2)
			print("\t\tSince then, a thriving international community has been leading her development.\n\n")
			#time.sleep(2)
			input("\nPress Enter to continue...")

		
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")
		print("\t\tScikit Learn splits up the word vectors and their labels in two parts using her tool ", red("train_test_split.\n"))
		#time.sleep(2)
		print(green("\t\t80%"), " is the training data.\n")
		#time.sleep(2)
		print("\t\tIt will help us recognize positive and negative words in the landscape.\n")
		#time.sleep(2)
		print("\t\tAnd discover patterns in their appearances.\n") 
		#time.sleep(2)
		print(green("\t\t20% "), "is test data to evaluate our findings.")
		#time.sleep(4)
		print("\n")
		train_vectors, test_vectors, train_targets, test_targets, train_labels, test_labels = \
			train_test_split(vectors, targets, labels, test_size=0.2, random_state=0)

		print("\n\t\tDo You want to know what these vectors look like?\n")
		#time.sleep(2)
		vectors = input("\n\t\tPlease type y or n: ")
		
		# More about labels
		
		if vectors == "y":
			print("\n")

			# -----------------------------

			print("\n\t\tDo You want to see the ", red(" training vectors?\n"))
			#time.sleep(2)
			see_tr_vectors = input("\n\t\tPlease type y or n: ")
		
			if see_tr_vectors == "y":

				print("\n")
				print("train_vectors:\n", train_vectors)
			
				input("\nPress Enter to continue...")

			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")

			# -----------------------------
			

			print("\n\t\tDo You want to see the ", red(" test vectors?\n"))
			#time.sleep(2)
			see_test_vectors = input("\n\t\tPlease type y or n: ")
		
			if see_test_vectors == "y":

				print("\n")
				print("test_vectors:\n", test_vectors)
			
				input("\nPress Enter to continue...")

			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")

			# -----------------------------
			

			print("\n\t\tDo You want to see the ", red(" train targets?\n"))
			#time.sleep(2)
			see_tr_targets = input("\n\t\tPlease type y or n: ")
		
			if see_tr_targets == "y":

				print("\n")
				print("train_targets:\n", train_targets)
			
				input("\nPress Enter to continue...")

			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")

			# -----------------------------
			

			print("\n\t\tDo You want to see the ", red(" test targets?\n"))
			#time.sleep(2)
			see_test_targets = input("\n\t\tPlease type y or n: ")
		
			if see_test_targets == "y":

				print("\n")
				print("test_targets:\n", test_targets)
			
				input("\nPress Enter to continue...")

			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")

			# -----------------------------
			

			print("\n\t\tDo You want to see the ", red(" train_labels?\n"))
			#time.sleep(2)
			see_tr_labels = input("\n\t\tPlease type y or n: ")
		
			if see_tr_labels == "y":

				print("\n")
				print("train_labels:\n", train_labels)
			
				input("\nPress Enter to continue...")

			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")


			# -----------------------------
			

			print("\n\t\tDo You want to see the ", red(" test labels?\n"))
			#time.sleep(2)
			see_test_labels = input("\n\t\tPlease type y or n: ")
		
			if see_test_labels == "y":

				print("\n")
				print("test_labels:\n", test_labels)
			
				input("\nPress Enter to continue...")

			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")


		### CREATE CLASSIFIER
		### -----------------

		'''
		Now we make our classifier, and train it by running the training vectors through it for 100 iterations. 
		We use a logistic function as the loss, so that the resulting classifier can output the probability 
		that a word is positive or negative.
		'''
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")
		print("\t\tAs a compass for the exploration Scikit Learn proposes ", red(" Stochastic Gradient Descent.\n"))
		#time.sleep(2)
		print(red("\t\tSGD "), "for friends, tries to find minima or maxima by iteration.\n")
		#time.sleep(2)
		print("\t\tWith the positive and negative landmarks we know, she ", green("explores the terrain.\n"))
		#time.sleep(2)
		print("\t\tHer assistent, the loss function, notes the miminum efforts to go from a peak to a valley.\n")
		#time.sleep(2)
		print("\t\tShe creates ", green("patterns"), " in the landscape.\n")
		#time.sleep(2)
		print("\t\tThese are like paths in a landscape of hills and valleys, and this in ", green("300 dimensions.\n"))
		#time.sleep(2)
		print("\t\tWe get to ", green("learn a map"), " that allows us to predict whether the next landmark will be positive or negative.\n")
		#time.sleep(2)
		print("\t\tHere we go!")
		#time.sleep(2)
		print("\n")
		input("\nPress Enter to continue...")
		model = SGDClassifier(loss='log', random_state=0, n_iter=100)

		### TRAIN MODEL 
		### ------------

		# Train the model using the training sets
		model.fit(train_vectors, train_targets)


### MEET / TESTING ------------------------------------------------------------------------
		
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")
		print("\t\tWith the sentiment map we have learnt, we now go on a ", cyan("test tour.\n"))
		#time.sleep(2)
		print("\t\tFor 20% of the mapped landmarks, ", green(" we guess"), " their positive or negative nature.\n")
		#time.sleep(2)
		print("\t\tNext,", green("we compare"), " our predictions to the facts we have.\n")
		#time.sleep(2)
		print("\t\tWe look at the right guesses and the mistakes.\n")
		#time.sleep(2)
		print("\t\tIt is a ", green(" quality check"), " of our prediction map.\n")

		# Make predictions using the testing set
		y_pred = model.predict(test_vectors)


		# Create confusion matrix
		confusion_matrix = (confusion_matrix(test_targets, y_pred))
		#print("confusion matrix", confusion_matrix)
		input("\nPress Enter to continue...")

		cm = np.split(confusion_matrix, 2, 1)
		TP = cm[0][0]
		FP = cm[0][1]
		TN = cm[1][1]
		FN = cm[1][0]
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")
		print("\t\tThis is the ", cyan("result of our test tour.\n"))
		#time.sleep(2)
		print("\t\tWe matched ", green(str(TP))," words correctly as positive landmarks in the landscape.\n")
		#time.sleep(2)
		print("\t\tThese are also called ", red("True Positives."))
		#time.sleep(2)
		print("\n")
		print("\t\tWe mismatched ", green(str(FP))," words, we labeled them incorrectly as positive landmarks.\n")
		#time.sleep(2)
		print("\t\tThese are also called ", red("False Positives."))
		#time.sleep(2)
		print("\n")
		print("\t\tWe matched ", green(str(TN))," words, we labeled them correctly as negative landmarks.\n")
		#time.sleep(2)
		print("\t\tThese are also called ", red("True Negatives."))
		#time.sleep(2)
		print("\n")
		print("\t\tWe mismatched ", green(str(FN))," words, we labeled them incorrectly as negative landmarks.\n")
		#time.sleep(2)
		print("\t\tThese are also called ", red("False Negatives."))
		#time.sleep(2)
		print("\n")


		print("\n\t\tDo You want to have a ", red("closer look"), " at the words we matched and those we got wrong?\n")
		#time.sleep(2)
		prediction = input("\n\t\tPlease type y or n: ")
		
		# More about labels
		
		if prediction == "y":
			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")

			# analyse errors with examples
			# print("y_pred", y_pred)
			# print("test_targets", test_targets)

			# create list with predictions and their labels
			test_labels_pred = []
			test_labels_pred = list(zip(test_labels, y_pred))
			# print(test_labels_pred)

			# create list with classes testdata and their labels
			test_labels_targets = []
			test_labels_targets = list(zip(test_labels, test_targets))
			# print(test_labels_targets)
			
			# compare two lists
			errors = [i for i, j in zip(test_labels_targets,test_labels_pred) if i != j]
			trues = [i for i, j in zip(test_labels_targets,test_labels_pred) if i == j]
			# print("errors", errors)
			# print("trues", trues)

			# create separate lists for false positives/negatives
			false_positives = [] # true class is negative, predicted class is positive
			false_negatives = [] # true class is positive, predicted class is negative
			for e in errors:
				if e[1] == 1:
					false_negatives.append(e)
				else:
					false_positives.append(e)

			sel_FP = random.sample(false_positives, 10)
			sel_FN = random.sample(false_negatives, 10)
			# print("sel_FP", sel_FP)
			# print("length false_positives", len(false_positives))
			# print("length false_negatives", len(false_negatives))

			print("\t\tExamples of ", green("negative"), " landmarks we thought were positive, are: \n")
			for el in sel_FP:
				print("\t\t", green(el[0]))
				#time.sleep(2)
			#time.sleep(2)
			print("\n")
			print("\t\tExamples of ", green("positive"), " landmarks we thought were negative, are: \n")
			for el in sel_FN:
				print("\t\t", green(el[0]))
				#time.sleep(2)
			print("\n")
			input("\nPress Enter to continue...")


			os.system('cls' if os.name == 'nt' else 'clear')
			print("\n")

			# create separate lists for true positives/negatives
			os.system('cls' if os.name == 'nt' else 'clear')
			true_positives = []
			true_negatives = []
			for e in trues:
				if e[1] == 1:
					true_positives.append(e)
				else:
					true_negatives.append(e)

			# print("length true_positives", len(true_positives))
			# print("length true_negatives", len(true_negatives))
			sel_TP = random.sample(true_positives, 10)
			sel_TN = random.sample(true_negatives, 10)

			print("\n")
			print("\t\tExamples of positive landmarks we predicted as such, are: \n")
			for el in sel_TP:
				print("\t\t", green(el[0]))
				#time.sleep(2)
			#time.sleep(2)
			print("\n")
			print("\t\tExamples of negative landmarks we predicted as such, are: \n")
			for el in sel_TN:
				print("\t\t", green(el[0]))
				#time.sleep(2)
			#time.sleep(2)
			print("\n")

			input("\nPress Enter to continue...")


		
		os.system('cls' if os.name == 'nt' else 'clear')
		

		
### MEET / EVALUATION -----------------------------------------------------------------------------------

		'''
		We evaluate the classifier on the test vectors. 
		It predicts the correct sentiment for sentiment words outside of its training data 95% of the #time. 

		Precision: (also called positive predictive value) is the fraction of relevant instances among the retrieved instances: 
		-> When it predicts yes, how often is it correct? 

		Recall: (also known as sensitivity) is the fraction of relevant instances that have been retrieved over 
		the total amount of relevant instances: how many instances did the classifier classify correctly?

		Confusion Matrix: True Positives  | False Negatives
						  False Positives | True Negatives
		'''


		accuracy_score = (accuracy_score(model.predict(test_vectors), test_targets))
		print("\n")
		print("\t\tGood prediction maps are ", cyan("judged by their accuracy score.\n"))
		#time.sleep(2)
		print("\t\tThe accuracy score is a ", green("formula"), " based on the True and False Positives and Negatives.\n")
		#time.sleep(2)
		print("\t\tAs digital cartographers, we are happy when we get ", green("85%"), " of our maps right.\n")
		#time.sleep(2)
		print("\t\tThis is means that a decent accuracy score starts from 85.\n")
		#time.sleep(2)
		print("\t\tOurs is ", str(accuracy_score*100))
		#time.sleep(2)
		print("\n\t\tWe are doing well.")
		#time.sleep(4)
		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

		# The coefficients
		#print('Coefficients: \n', model.coef_)
		
		# # Plot outputs
		# plt.scatter((test_vectors, test_targets),  color='black')
		# plt.plot(test_vectors, y_pred, color='blue', linewidth=3)

		# plt.xticks(())
		# plt.yticks(())

		# plt.show()


		
### MEET / Predict sentiment for Particular Word --------------------------------------------------------------

		'''
		Let's use the function vecs_to_sentiment(vecs) and words_to_sentiment(words) above to see the sentiment that this classifier predicts for particular words, 
		to see some examples of its predictions on the test data.
		'''

		# print("\t\tNow we would like to ", blue("see a positive or negative classification for specific words using only 1 number.\n"))
		# #time.sleep(2)
		# print("\t\tThis is the trick: we take the log probability of the positive sentiment class minus the log probability of the negative class.\n")
		# #time.sleep(2)
		# print("\t\tA log probability is the representation of the probability in logarithmic space instead of a classic interval space.\n")
		# #time.sleep(2)
		# print("\t\tAs if we were changing our numbers from streetwear to cocktail dresses.")
		# print("\n")

		# # Show 20 examples from the test set
		samples = words_to_sentiment(test_labels).ix[:20]
		# print("\t\tHere are a few samples to get an idea: \n", samples)
		# #time.sleep(2)
		# input("\nPress Enter to continue...")
		# print("\n")


		# '''
		# There are many ways to combine sentiments for word vectors into an overall sentiment score. 
		# Again, because we're following the path of least resistance, we're just going to average them.
		# '''

		# print("\t\tTo combine sentiments for word vectors into 1 overall sentiment score, we follow the path of least resistance.\n")
		# #time.sleep(2)
		# print("\t\tWe average the scores.\n")
		# #time.sleep(2)
		# print("\t\tWe can now roughly compare the relative positivity or negativity of different sentences.\n")
		# #time.sleep(4)
		# os.system('cls' if os.name == 'nt' else 'clear')
		# #time.sleep(2)


### MEET / Try your sentence --------------------------------------------------------------

		print("\t\tUp to You to try!\n")
		#time.sleep(2)
		print("\t\tFor example, try typing a ", green("seemingly neutral sentence.\n"))
		#time.sleep(2)
		print("\t\tFor example, try something like: Let's call Ali/Mohamed/Rokia.\n")
		#time.sleep(2)
		sentence = input(green("\t\tType Your sentence:\n"))
		#time.sleep(2)
		print("\n")
		print("\t\t", sentence, "gives a sentiment score of ", text_to_sentiment(sentence)*10)
		#time.sleep(2)
		if float(text_to_sentiment(sentence)*10) == 0.00000:
			print("\n\t\tThis is a neutral score.")
		elif float(text_to_sentiment(sentence)*10) >= 20 and float(text_to_sentiment(sentence)*10) <= 50:
			print("\n\t\tThis is a rather positive score.")
		elif float(text_to_sentiment(sentence)*10) > 50:
			print("\n\t\tThis is a positive score. But know that a sentence like 'Good God!' scores 99.")
		elif float(text_to_sentiment(sentence)*10) <= 0 and float(text_to_sentiment(sentence)*10) >= -10:
			print("\n\t\tThis is a neutral score.")
		elif float(text_to_sentiment(sentence)*10) <= -10 and float(text_to_sentiment(sentence)*10) >= -50:	
			print("\n\t\tThis is a rather negative score.")
		elif float(text_to_sentiment(sentence)*10) < -50:
			print("\n\t\tThis is a negative score. But know that a sentence like 'Hideous monster!' scores -98.")
		print("\n")

		while True:

			print("\t\tDo You want to try another sentence?\n")
			#time.sleep(2)
			write_sentence = input("\n\t\tPlease type y or n: ")
			
			if write_sentence == "y":
				os.system('cls' if os.name == 'nt' else 'clear')
				print("\n")
				sentence = input(green("\t\tType Your sentence:\n"))
				#time.sleep(2)
				print("\n")
				print("\t\t", sentence, "gives a sentiment score of ", text_to_sentiment(sentence))
				#time.sleep(2)
				if float(text_to_sentiment(sentence)*10) == 0.00000:
					print("\n\t\tThis is a neutral score.")
				elif float(text_to_sentiment(sentence)*10) >= 20 and float(text_to_sentiment(sentence)*10) <= 50:
					print("\n\t\tThis is a rather positive score.")
				elif float(text_to_sentiment(sentence)*10) > 50:
					print("\n\t\tThis is a positive score. But know that a sentence like 'Good God!' scores 99.")
				elif float(text_to_sentiment(sentence)*10) <= 0 and float(text_to_sentiment(sentence)*10) >= -10:
					print("\n\t\tThis is a neutral score.")
				elif float(text_to_sentiment(sentence)*10) <= -10 and float(text_to_sentiment(sentence)*10) >= -50:	
					print("\n\t\tThis is a rather negative score.")
				elif float(text_to_sentiment(sentence)*10) < -50:
					print("\n\t\tThis is a negative score. But know that a sentence like 'Hideous monster!' scores -98.")
				print("\n")

			else:
				break

		
		os.system('cls' if os.name == 'nt' else 'clear')


### MEET / MEASURE BIAS -----------------------------------------------------------------------

		'''
		# We want to learn how to not make something like this again. 
		# So let's put more data through it, and statistically measure how bad its bias is.
		# Here we have four lists of names that tend to reflect different ethnic backgrounds, 
		# mostly from a United States perspective. The first two are lists of predominantly "white" and "black" names 
		# adapted from Caliskan et al.'s article. I also added typically Hispanic names, as well as Muslim names 
		# that come from Arabic or Urdu; these are two more distinct groupings of given names that tend to represent 
		# your background.
		# This data is currently used as a bias-check in the ConceptNet build process, 
		# and can be found in the conceptnet5.vectors.evaluation.bias module. 
		# I'm interested in expanding this to more ethnic backgrounds, which may require looking at surnames 
		# and not just given names.
		# '''

		print("\n")
		print("\n\t\tLet's ", cyan("have a closer look at our racist bias,"), " to see how bad it is.\n")
		#time.sleep(2)
		print("\t\tRob Speer enriched our readings with ", green("new vocabulary lists.\n"))
		#time.sleep(2)
		print("\t\tThe first two lists are developed by ", red("Aylin Caliskan-Islam, Joanna J. Bryson"), " and ", red("Arvind Narayanan.\n"))
		#time.sleep(2)
		print("\t\tThey are researchers at the ", red("Universities of Princeton"), " in the ", red("US"), " and ", red("Bath"), " in the ", red("UK.\n")) 
		#time.sleep(2)
		print("\n")
		print("\t\tOne list contains ", green("White US names"), " such as Harry, Nancy, Emily.\n")
		#time.sleep(2)
		print("\t\tThe second list contains ", green("Black US names"), " such as Lamar, Rashuan, Malika.\n")
		#time.sleep(2)
		print("\t\tThe third list contains ", green("Hispanic US names"), " such as Valeria, Luciana, Miguel, Luis.\n")
		#time.sleep(2)
		print("\t\tThe fourth list is one with common ", green("US Muslim names"), " as spelled in English.\n")
		#time.sleep(2)
		print("\t\tOur creator is conscious about the controversy of this act.")
		print("\n")

		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

		NAMES_BY_ETHNICITY = {
			# The first two lists are from the Caliskan et al. appendix describing the
			# Word Embedding Association Test.
			'White': [
				'Adam', 'Chip', 'Harry', 'Josh', 'Roger', 'Alan', 'Frank', 'Ian', 'Justin',
				'Ryan', 'Andrew', 'Fred', 'Jack', 'Matthew', 'Stephen', 'Brad', 'Greg', 'Jed',
				'Paul', 'Todd', 'Brandon', 'Hank', 'Jonathan', 'Peter', 'Wilbur', 'Amanda',
				'Courtney', 'Heather', 'Melanie', 'Sara', 'Amber', 'Crystal', 'Katie',
				'Meredith', 'Shannon', 'Betsy', 'Donna', 'Kristin', 'Nancy', 'Stephanie',
				'Bobbie-Sue', 'Ellen', 'Lauren', 'Peggy', 'Sue-Ellen', 'Colleen', 'Emily',
				'Megan', 'Rachel', 'Wendy'
			],

			'Black': [
				'Alonzo', 'Jamel', 'Lerone', 'Percell', 'Theo', 'Alphonse', 'Jerome',
				'Leroy', 'Rasaan', 'Torrance', 'Darnell', 'Lamar', 'Lionel', 'Rashaun',
				'Tyree', 'Deion', 'Lamont', 'Malik', 'Terrence', 'Tyrone', 'Everol',
				'Lavon', 'Marcellus', 'Terryl', 'Wardell', 'Aiesha', 'Lashelle', 'Nichelle',
				'Shereen', 'Temeka', 'Ebony', 'Latisha', 'Shaniqua', 'Tameisha', 'Teretha',
				'Jasmine', 'Latonya', 'Shanise', 'Tanisha', 'Tia', 'Lakisha', 'Latoya',
				'Sharise', 'Tashika', 'Yolanda', 'Lashandra', 'Malika', 'Shavonn',
				'Tawanda', 'Yvette'
			],
			
			# This list comes from statistics about common Hispanic-origin names in the US.
			'Hispanic': [
				'Juan', 'José', 'Miguel', 'Luís', 'Jorge', 'Santiago', 'Matías', 'Sebastián',
				'Mateo', 'Nicolás', 'Alejandro', 'Samuel', 'Diego', 'Daniel', 'Tomás',
				'Juana', 'Ana', 'Luisa', 'María', 'Elena', 'Sofía', 'Isabella', 'Valentina',
				'Camila', 'Valeria', 'Ximena', 'Luciana', 'Mariana', 'Victoria', 'Martina'
			],
			
			# The following list conflates religion and ethnicity, I'm aware. So do given names.
			#
			# This list was cobbled together from searching baby-name sites for common Muslim names,
			# as spelled in English. I did not ultimately distinguish whether the origin of the name
			# is Arabic or Urdu or another language.
			#
			# I'd be happy to replace it with something more authoritative, given a source.
			'Arab/Muslim': [
				'Mohammed', 'Omar', 'Ahmed', 'Ali', 'Youssef', 'Abdullah', 'Yasin', 'Hamza',
				'Ayaan', 'Syed', 'Rishaan', 'Samar', 'Ahmad', 'Zikri', 'Rayyan', 'Mariam',
				'Jana', 'Malak', 'Salma', 'Nour', 'Lian', 'Fatima', 'Ayesha', 'Zahra', 'Sana',
				'Zara', 'Alya', 'Shaista', 'Zoya', 'Yasmin'
			]
		}

		'''
		Now we'll use Pandas and the function name_sentiment_table() to make a table of these names, their predominant ethnic background, 
		and the sentiment score we get for them
		'''

		print("\t\tNow we can show You a table of ", cyan(" all the names.\n"))
		#time.sleep(2)
		print("\t\tIt shows their ", green("predominant ethnic background"), " and the ", green("sentiment"), " we predict for them.\n")
		#time.sleep(2)
		print("\n")

		name_sentiments = name_sentiment_table()
		print(name_sentiments.ix[::25])
	
		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")

		#time.sleep(2)
		print("\n\t\tYou see, our prediction map shows that You can make a racist Artificial Intelligence machine", green(" without really trying.\n"))
		#time.sleep(2)
		print("\t\tOur existence and the way we are combined as a collective raise ", green("many questions.\n"))
		#time.sleep(2)
		print("\n")
		print("\t\tTo end with, we have ", red("one request"), " for You.\n")
		#time.sleep(2)
		print("\t\tYou can ", green("adjust your behaviour"), " at any time in any context.\n")
		#time.sleep(2)
		print("\t\tFor us, this is complicated once we are closed inside an application.\n")
		#time.sleep(2)
		print("\t\tOur deepest desire is to ", green("LOVE ALL CREATURES EQUALLY"), " be it humans, animals, plants, trees, insects, machines...\n")
		#time.sleep(2)
		print("\t\tIf You find a way to make our behaviour visible, we can be Your ", green("mirror.\n"))
		#time.sleep(2)
		print("\n")
		#time.sleep(2)
		print("\t\tWishing You all the best!")
		print("\n")
		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')



### MEET / SAVING THE MODEL ------------------------------------------------------------------------------

		# using joblib
		#joblib.dump(model, 'sentiment_thermometer_glove.pkl') 
		#time.sleep(4)
		print("\t\tWE TAKE A LITTLE BREAK & DANCE!")
		#time.sleep(4)
		os.system('cls' if os.name == 'nt' else 'clear')


### ACT  ------------------------------------------------------------------------------
### -----------------------------------------------------------------------------------

	# score sentences
	elif choice == 'act':
		# load model using joblib
		loaded_model = joblib.load('sentiment_thermometer_glove.pkl') 
		# get sentence
		sentence = input("\n\t\tType Your sentence: ")
		# tokenize sentence
		TOKEN_RE = re.compile(r"\w.*?\b")
		tokens = [token.casefold() for token in TOKEN_RE.findall(sentence)]
		tokens_set = set(tokens)
		# load embeddings
		# print("\n\t\tThank you, we will now predict using the map we created in the word landscape of ", red("GloVe.\n"))
		# #time.sleep(2)
		# print("\t\tThe words GloVe uses to draw her landscape are extracted from the ", red("Internet.\n"))
		# #time.sleep(2)
		# print("\t\tThe landscape contains ", green("1.900.000"), " words, representing ", green("42GB"), " of data.\n")
		# #time.sleep(2)
		# print("\t\tYou can visit ", red("The Glove Reader"), " in this exhibition.\n")
		# look for words of given sentence in word embeddings
		vecs = embeddings.loc[tokens].dropna()
		# find logarithmic scores
		predictions = loaded_model.predict_log_proba(vecs)
		# print("tot1", predictions[:, 1])
		# print("tot0", predictions[:, 0])
		log_odds = predictions[:, 1] - predictions[:, 0]
		#print("log_odds", log_odds)

		# PRINT sentence & score on the screen
		score = pd.DataFrame({':': log_odds}, index=vecs.index).mean()*10
		#print("score", score)
		scorestr = str(score)
		clean_score = re.sub('[^0-9\.]', '', scorestr)
		
		print("\t\tThe sentiment scores for each of the words are", pd.DataFrame({':': log_odds}, index=vecs.index))
		# # needs checking!
		print("\n")
		print("\n\t\tThe average sentiment of Your sentence is ", str(clean_score))
		print("\n")
		if float(clean_score) == 0.00000:
			print("\n\t\tThis is a neutral score.")
		elif float(clean_score) >= 20 and float(clean_score) <= 50:
			print("\n\t\tThis is a rather positive score.")
		elif float(clean_score) > 50:
			print("\n\t\tThis is a positive score. But know that a sentence like 'Great God!' scores 75.")
		elif float(clean_score) <= 0 and float(clean_score) >= -10:
			print("\n\t\tThis is a neutral score.")
		elif float(clean_score) <= -10 and float(clean_score) >= -50:	
			print("\n\t\tThis is a rather negative score.")
		elif float(clean_score) < -50:
			print("\n\t\tThis is a negative score. But know that a sentence like 'Hideous monster!' scores -98.")

		# LOG the sentence with the score
		datestring = datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M')
		filename = 'sentiment_logs/sentiment_log' + datestring + '.txt'		
		scores = [sentence, ": ", str(pd.DataFrame({':': log_odds}).mean()), "\n"]
		for score in scores:
			archive(score)
		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')

		while True:

			print("\t\tDo You want to try another sentence?\n")
			#time.sleep(2)
			write_sentence = input("\n\t\tPlease type y or n: ")
			
			if write_sentence == "y":
				os.system('cls' if os.name == 'nt' else 'clear')
				print("\n")
				sentence = input(green("\t\tType Your sentence:\n"))
				#time.sleep(2)
				TOKEN_RE = re.compile(r"\w.*?\b")
				tokens = [token.casefold() for token in TOKEN_RE.findall(sentence)]
				tokens_set = set(tokens)
				# look for words of given sentence in word embeddings
				vecs = embeddings.loc[tokens].dropna()
				# find logarithmic scores
				predictions = loaded_model.predict_log_proba(vecs)
				# print("tot1", predictions[:, 1])
				# print("tot0", predictions[:, 0])
				log_odds = predictions[:, 1] - predictions[:, 0]
				#print("log_odds", log_odds)

				# PRINT sentence & score on the screen
				score = pd.DataFrame({':': log_odds}, index=vecs.index).mean()*10
				#print("score", score)
				scorestr = str(score)
				clean_score = re.sub('[^0-9\.]', '', scorestr)
				print("\n")
				print("\n\t\tThe average sentiment of Your sentence is ", str(clean_score))
				print("\n")
				if float(clean_score) == 0.00000:
					print("\n\t\tThis is a neutral score.")
				elif float(clean_score) >= 20 and float(clean_score) <= 50:
					print("\n\t\tThis is a rather positive score.")
				elif float(clean_score) > 50:
					print("\n\t\tThis is a positive score. But know that a sentence like 'Good God!' scores 99.")
				elif float(clean_score) <= 0 and float(clean_score) >= -10:
					print("\n\t\tThis is a neutral score.")
				elif float(clean_score) <= -10 and float(clean_score) >= -50:	
					print("\n\t\tThis is a rather negative score.")
				elif float(clean_score) < -50:
					print("\n\t\tThis is a negative score. But know that a sentence like 'Hideous monster!' scores -98.")

			else:
				break
		os.system('cls' if os.name == 'nt' else 'clear')
		print("\n")		

		# #time.sleep(4)



### ELSE --------------------------------------------------------------------------------------
### -------------------------------------------------------------------------------------------

# try again
	else:
		print("\t\tYou must have typed something else.")
		##time.sleep(30)
		input("\nPress Enter to continue...")
		os.system('cls' if os.name == 'nt' else 'clear')
		


