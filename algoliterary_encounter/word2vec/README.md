Word2Vec Algolit extensions
===========================

This is an annoted version of the word2vec_basic.py script.

Added Algolit extension:
- input text = Frankenstein (cleaned version of the book)
- writes a dictionary.txt
- possibility to pick your own test words
- writes a log.txt of the training proces

Procedure:
1. download data (word2vec)
1. read data from plain text file (algolit)
2. create a dictionary and replace rare words with UNK token

