<?xml version="1.0"  encoding="UTF-8"?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
  <metadata xmlns:calibre="http://calibre.kovidgoyal.net/2009/metadata" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:opf="http://www.idpf.org/2007/opf" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <dc:creator opf:role="aut" opf:file-as="Butler, Octavia E. &amp; Mosley, Walter">Octavia E. Butler</dc:creator>
<dc:creator opf:role="aut">Walter Mosley</dc:creator>
<dc:contributor opf:role="bkp">calibre (1.34.0) [http://calibre-ebook.com]</dc:contributor>
    <dc:identifier id="uuid_id" opf:scheme="uuid">urn:ean:9781497601376</dc:identifier>
    <dc:publisher>Open Road Media Sci-Fi &amp; Fantasy</dc:publisher>
    <dc:description>&lt;div&gt;&lt;p&gt;&lt;strong&gt;Two never-before-published stories from the archives of one of science fiction’s all-time masters&lt;/strong&gt;  &lt;/p&gt;&lt;p&gt;The novella “A Necessary Being” showcases Octavia E. Butler’s ability to create alien yet fully believable “others.” Tahneh’s father was a Hao, one of a dwindling race whose leadership abilities render them so valuable that their members are captured and forced to govern. When her father dies, Tahneh steps into his place, both chief and prisoner, and for twenty years has ruled without ever meeting another of her kind. She bears her loneliness privately until the day that a Hao youth is spotted wandering into her territory. As her warriors sharpen their weapons, Tahneh must choose between imprisoning the newcomer—and living the rest of her life alone.  &lt;/p&gt;&lt;p&gt;The second story in this volume, “Childfinder,” was commissioned by Harlan Ellison for his legendary (and never-published) anthology &lt;em&gt;The Last Dangerous Visions&lt;/em&gt;™. A disaffected telepath connects with a young girl in a desperate attempt to help her harness her growing powers. But in the richly evocative fiction of Octavia E. Butler, mentorship is a rocky path, and every lesson comes at a price.&lt;/p&gt;&lt;p&gt;Harlan Ellison and Dangerous Visions are registered trademarks of the Kilimanjaro Corporation. &lt;em&gt;All rights reserved.&lt;/em&gt;&lt;/p&gt;&lt;h3&gt;Review&lt;/h3&gt;&lt;p&gt;“These riveting additions to Butler’s oeuvre are superb examples of her craft and will be welcomed by Butler fans and scholars alike.” —&lt;em&gt;Publishers Weekly&lt;/em&gt;, starred review  &lt;/p&gt;&lt;p&gt;“Both stories deal in their own way with the issues of race and gender that informed much of Butler’s work. . . . These stories, with an introduction by Walter Mosley, will be a joy for Butler’s many readers.” —&lt;em&gt;Library Journal&lt;/em&gt;  &lt;/p&gt;&lt;p&gt;“Striking social commentary underscores the action in two dark, previously unpublished stories from the late sci-fi master. . . . A small but important addition to the oeuvre of a writer deeply concerned with issues of race and gender.” —&lt;em&gt;Kirkus Reviews&lt;/em&gt;, starred review  &lt;/p&gt;&lt;p&gt;“One of the finest voices in fiction—period. . . . Butler casts an unflinching eye on racism, sexism, poverty and ignorance and lets the reader see the terror and beauty of human nature.” —&lt;em&gt;The Washington Post Book World&lt;/em&gt;  &lt;/p&gt;&lt;p&gt;“What we have in these stories is actually something less like a childhood photograph, or juvenilia, or apocrypha, and more like the miraculous discovery that the beloved book you’ve read a dozen times has an extra chapter you’ve somehow never noticed. These stories don’t feel different; they feel like just her.” —&lt;em&gt;Los Angeles Review of Books&lt;/em&gt;  &lt;/p&gt;&lt;p&gt;“In both stories Butler is able to create a whole world and a whole history out of very few words, by centering them on women who suffer no illusions about the worlds and circumstances they live in. She addresses race and class head-on as well as in metaphorical terms.” —&lt;em&gt;NPR Books&lt;/em&gt;&lt;/p&gt;&lt;h3&gt;About the Author&lt;/h3&gt;&lt;p&gt;Octavia E. Butler (1947–2006) was a bestselling and award-winning author, considered one of the best science fiction writers of her generation. She received both the Hugo and Nebula awards, and in 1995 became the first author of science fiction to receive a MacArthur Fellowship. She was also awarded the prestigious PEN Lifetime Achievement Award in 2000. Her first novel, &lt;em&gt;Patternmaster&lt;/em&gt; (1976), was praised both for its imaginative vision and for Butler’s powerful prose, and spawned four prequels, beginning with &lt;em&gt;Mind of My Mind&lt;/em&gt; (1977) and finishing with &lt;em&gt;Clay’s Ark&lt;/em&gt; (1984).  &lt;/p&gt;&lt;p&gt;Although the Patternist series established Butler among the science fiction elite, it was &lt;em&gt;Kindred&lt;/em&gt; (1979), a story of a black woman who travels back in time to the antebellum South, that brought her mainstream success. In 1985, Butler won Nebula and Hugo awards for the novella “Bloodchild,” and in 1987 she published &lt;em&gt;Dawn&lt;/em&gt;, the first novel of the Xenogenesis trilogy, about a race of aliens who visit earth to save humanity from itself. &lt;em&gt;Fledgling&lt;/em&gt; (2005) was Butler’s final novel. She died at her home in 2006. &lt;/p&gt;&lt;/div&gt;</dc:description>
    <dc:date>2014-06-24T05:00:00+00:00</dc:date>
    <meta content="2014-03-25T20:07:14.040376+00:00" name="calibre:timestamp"/>
    <meta name="calibre:title_sort" content="Unexpected Stories"/>
    <meta name="calibre:user_categories" content="{}"/>
    <meta name="calibre:author_link_map" content="{&quot;Octavia E. Butler&quot;: &quot;&quot;, &quot;Walter Mosley&quot;: &quot;&quot;}"/>
  <dc:title>Unexpected Stories</dc:title>
<dc:subject>kickass.to</dc:subject>
<dc:subject>ScreamQueen</dc:subject>
<dc:language>en</dc:language>
<dc:identifier opf:scheme="calibre">8ffafe43-594e-4faf-9a48-f26ce4f8f632</dc:identifier>
<dc:identifier opf:scheme="AMAZON">B00K04NWG0</dc:identifier>
</metadata>
  <manifest>
    <item href="1-cover.xhtml" id="cover1" media-type="application/xhtml+xml"/>
    <item href="note.html" id="note" media-type="application/xhtml+xml"/>
    <item href="index_split_000.html" id="id16" media-type="application/xhtml+xml"/>
    <item href="index_split_001.html" id="id15" media-type="application/xhtml+xml"/>
    <item href="index_split_002.html" id="id14" media-type="application/xhtml+xml"/>
    <item href="index_split_003.html" id="id13" media-type="application/xhtml+xml"/>
    <item href="index_split_004.html" id="id12" media-type="application/xhtml+xml"/>
    <item href="index_split_005.html" id="id11" media-type="application/xhtml+xml"/>
    <item href="Butler_Backas.xhtml" id="auba" media-type="application/xhtml+xml"/>
    <item href="ORIMad.xhtml" id="ORIMba" media-type="application/xhtml+xml"/>
    <item href="toc.ncx" id="ncx" media-type="application/x-dtbncx+xml"/>
    <item href="ORIMstyles.css" id="ORIMstyle" media-type="text/css"/>
    <item href="page_styles.css" id="page_css" media-type="text/css"/>
    <item href="stylesheet.css" id="css" media-type="text/css"/>
    <item href="colophon.jpg" id="logo3" media-type="image/jpeg"/>
    <item href="cover.jpg" id="cover-image" media-type="image/jpeg"/>
    <item href="images/Butler_AdulthoodRites_fmt.png" id="Butler_AdulthoodRites_fmt.png" media-type="image/png"/>
    <item href="images/Butler_Bloodchild_fmt.jpeg" id="Butler_Bloodchild_fmt.jpeg" media-type="image/jpeg"/>
    <item href="images/Butler_ClaysArk_fmt.png" id="Butler_ClaysArk_fmt.png" media-type="image/png"/>
    <item href="images/Butler_Dawn_fmt.jpeg" id="Butler_Dawn_fmt.jpeg" media-type="image/jpeg"/>
    <item href="images/Butler_Imago_fmt.png" id="Butler_Imago_fmt.png" media-type="image/png"/>
    <item href="images/Butler_MindOfMyMind_fmt.png" id="Butler_MindOfMyMind_fmt.png" media-type="image/png"/>
    <item href="images/Butler_ParableSower_fmt.png" id="Butler_ParableSower_fmt.png" media-type="image/png"/>
    <item href="images/Butler_ParableTalents_fmt.png" id="Butler_ParableTalents_fmt.png" media-type="image/png"/>
    <item href="images/Butler_Patternmaster_fmt.jpeg" id="Butler_Patternmaster_fmt.jpeg" media-type="image/jpeg"/>
    <item href="images/Butler_WildSeed_fmt.png" id="Butler_WildSeed_fmt.png" media-type="image/png"/>
    <item href="logo.jpg" id="logo1" media-type="image/jpeg"/>
    <item href="logo2.jpg" id="logo2" media-type="image/jpeg"/>
  </manifest>
  <spine toc="ncx">
    <itemref idref="cover1"/>
    <itemref idref="note"/>
    <itemref idref="id16"/>
    <itemref idref="id15"/>
    <itemref idref="id14"/>
    <itemref idref="id13"/>
    <itemref idref="id12"/>
    <itemref idref="id11"/>
    <itemref idref="auba"/>
    <itemref idref="ORIMba"/>
  </spine>
  <guide>
    <reference href="index_split_000.html" title="Startup Page" type="text"/>
    <reference href="index_split_001.html" title="Table of Contents" type="toc"/>
    <reference href="1-cover.xhtml" title="Cover Page" type="cover"/>
  </guide>
</package>
