#!/usr/bin/python -tt
# -*- coding: utf-8 -*-

from nltk.tokenize import word_tokenize
import re
import string
import codecs

filename = '../input/frankenstein.txt'

sentences = []
regex = re.compile('[%s]' % re.escape(string.punctuation)) #see documentation here: http://docs.python.org/2/library/string.html
decoded = ''

with open(filename, 'r') as source:
	lines = source.readlines()
	if len(lines) == 1:
		# lines = lines[0].decode('utf8')
		decoded = 'yes'
		lines = lines.split('. ')
	for line in lines:
		print(line)
		if "”" in line:
			line = line.replace("”","")
		if "“" in line:
			line = line.replace("“","")

		words = word_tokenize(line)
		string = []
		for word in words:
			new_word = regex.sub(u'', word)
			if not new_word == u'':
				string.append(new_word)
		string = ' '.join(string)
		sentences.append(string)

outputfilename = filename.replace('.txt','_stripped.txt')
with codecs.open(outputfilename, "w", "utf-8") as destination:
	for sentence in sentences:
		destination.write(sentence.strip().lower()+" ")
	print('*text is stripped*')
