
# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import math
import os, sys
import random
import zipfile
import codecs

import numpy as np
from six.moves import urllib
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
from nltk.tokenize import word_tokenize

# Algolit settings:

txtExport = 'no'
plotExport = 'no'
plotFullMatrixExport = 'yes'
exportFullMatrixHTML = 'no'
graphExport = 'no'

# trainingsets = [
# 	'algoliterary-datasets/WikiHarass.txt',
# 	'algoliterary-datasets/learning-deep-learning.txt',
# 	'algoliterary-datasets/nearbySaussure.txt',
# 	'algoliterary-datasets/frankenstein.txt',
# 	'algoliterary-datasets/astroBlackness.txt'
# ]
trainingsets = [
	'algoliterary-datasets/astroBlackness.txt'
]

for trainingset in trainingsets:

	filename = trainingset.replace('algoliterary-datasets/','')
	filename = filename.replace('.txt','')

	# Algolit adaption:
	print('TensorFlow version:', tf.__version__)

	# Algolit adaption: 
	# allow to print full arrays / prevents printing truncated representations of arrays 
	np.set_printoptions(threshold=np.inf)
	float_formatter = lambda x: "%.2f" % x
	np.set_printoptions(formatter={'float_kind':float_formatter})

	# Algolit adaption:
	# save data to txt file
	def export(fn, data):
		if txtExport == 'yes':
			subdir = filename
			outputdir = 'txt/'+subdir+'/'
			if not os.path.exists(outputdir):
		   		os.makedirs(outputdir)
			with open(outputdir+fn,'w+') as output:
				output.write(str(data))
				print('*exported '+fn+'*')
		else:
			print('*txtExport is disabled.*')

	# ***********************************************************************************
	# Step 1: Download data.
	# ***********************************************************************************
	# url = 'http://mattmahoney.net/dc/'

	# def maybe_download(filename, expected_bytes):
	# 	"""
	# 		Step 1: Download data.
	# 	"""
	# 	if not os.path.exists(filename):
	# 		filename, _ = urllib.request.urlretrieve(url + filename, filename)
	# 	statinfo = os.stat(filename)
	# 	if statinfo.st_size == expected_bytes:
	# 		print('Found and verified', filename)
	# 	else:
	# 		print(statinfo.st_size)
	# 		raise Exception('Failed to verify ' + filename + '. Can you get to it with a browser?')
	# 	return filename

	# # filename = maybe_download('text8.zip', 31344016)

	# def read_data(filename):
	# 	"""
	# 		Read the data into a list of strings. Extract the first file enclosed in a zip file as a list of words
	# 	"""
	# 	with zipfile.ZipFile(filename) as f:
	# 		data = tf.compat.as_str(f.read(f.namelist()[0])).split()
	# 		return data

	# read_data(filename)

	# ***********************************************************************************
	# ALGOLIT step 1: read data from plain text file
	# ***********************************************************************************

	def read_input_text(trainingset):
		"""
			ALGOLIT step 1: read data from plain text file.
		"""
		words = []
		with open(trainingset, 'r') as source:
			lines = source.readlines()
			for line in lines:
				line = line.decode('utf8')
				wordlist = word_tokenize(line)
				for word in wordlist:
					words.append(word)
		return words

	words = read_input_text(trainingset)
	print('Data size:', len(words))

	# Algolit inspection
	words = words
	export('wordlist-'+str(len(words))+'.txt', words)

	# ***********************************************************************************
	# Step 2: Create a dictionary and replace rare words with UNK token.
	# ***********************************************************************************

	vocabulary_size = 5000

	def build_dataset(words):
		"""
			Step 2: Create a dictionary and replace rare words with UNK token.
		"""

	 	# define the unknown word
		count = [['UNK', -1]]

	 	# extend the list with the (vocabulary_size) most frequent words of the text
		count.extend(collections.Counter(words).most_common(vocabulary_size - 1))

		# >>> printing the counting the words, output is a Counter({"word", "1234"}) object, where 1234 is the amount of times a word appears
		# print('collections.Counter(words)', collections.Counter(words))

		# >>> printing the number of unique words
		# print(len(collections.Counter(words)))
		# result: 7259

		# Algolit adaption
		export('counted.txt', collections.Counter(words))

		# >>> printing a selection of a chunk of the words, in the size of the vocabulary_size
		# print('collections.Counter(words).most_common(vocabulary_size - 1) >>>', collections.Counter(words).most_common(vocabulary_size - 1))

		# >>> printing the length of the previews command
		# print('len(collections.Counter(words).most_common(vocabulary_size - 1)) >>>', len(collections.Counter(words).most_common(vocabulary_size - 1)))

		# >>> print the extended count list
		# print('count >>> ',count)

	 	# create dictionary of most common words + index number
	 	# frequency value is ignored, the number sets the order
		dictionary = dict()
		for word, _ in count:
			dictionary[word] = len(dictionary)

		# print('dictionary', dictionary)

		# counting how many words from the input file are disregarded
		unk_count = 0

		# if word of text is in (vocabulary_size) most common words,
		# it is translated into the index number of that word
		data = []
		disregarded = []
		for word in words:
			if word in dictionary:
				index = dictionary[word]
			else:
				index = 0  # dictionary['UNK']
				unk_count += 1
				disregarded.append(word)
				# printing the excluded words
				# print(word)
			data.append(index)

		# Algolit adaption: exporting the disregarded words
		export('disregarded-'+str(unk_count)+'.txt', disregarded)

		# >>> print list of all words, connected to their index number. Unk words are connected to index number 0
		# output: [1836, 1216, 33, 243, 89, 6] > Frankenstein in index numbers, following the order of the text.
		# print('data', data)

		# >>> print the counting how many words are excluded
		# print('unk_count',unk_count)

		# expands the first item of the count list with the number of unknown words
		count[0][1] = unk_count

		# >>> printing the count list
		# print('count', count)

		# keys become values and values become keys
		reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
		return data, count, dictionary, reverse_dictionary

	data, count, dictionary, reverse_dictionary = build_dataset(words)

	del words  # Hint to reduce memory.
	# print('Most common words (+UNK)', count[:5])
	# print('Sample data', data[:10], [reverse_dictionary[i] for i in data[:10]])

	data_index = 0
	#  what does this do???

	# Algolit adaption
	export('dictionary-'+str(len(reverse_dictionary))+'.txt', reverse_dictionary)
	export('data-'+str(len(data))+'.txt', data)

	# Algolit adaption:
	# translate the data object back to words
	words = []
	for index in data:
		word = reverse_dictionary[index]
		words.append(word)
	fulltext = ' '.join(words).encode('utf-8')
	export('reversed-input-'+str(len(words))+'.txt', fulltext)

	# ***********************************************************************************
	# Step 3: Function to generate a training batch for the skip-gram model.
	# ***********************************************************************************

	# "Recall that skip-gram inverts contexts and targets, and tries to predict each context word from its target word, so the task becomes to predict 'the' and 'brown' from 'quick', 'quick' and 'fox' from 'brown', etc." source: https://www.tensorflow.org/tutorials/word2vec

	def generate_batch(batch_size, num_skips, skip_window):
		"""
			Step 3: Function to generate a training batch for the skip-gram model.
		"""
		global data_index

		# test to see if variables are set well, 'ensure data integrity'
		# % is a modulo, performing a division with a integer and outputs what is left
		# for example: 7 modulo 3 = 1 / 7 % 3 = 1
		assert batch_size % num_skips == 0
		assert num_skips <= 2 * skip_window

		# np = numpy
		# ndarray = N-dimensional array
		# does the ndarray create a list of random numbers? (or otherwise: where do these numbers come from?)
		batch = np.ndarray(shape=(batch_size), dtype=np.int32)
		labels = np.ndarray(shape=(batch_size, 1), dtype=np.int32)

		# print('batch', batch)
		# print('labels', labels)

		span = 2 * skip_window + 1 # [ skip_window target skip_window ]

		# deque = double-ended queue
		# collections.deque() = a list that can be accessed on both 'sides' (the beginning and end of a list)
		buffer = collections.deque(maxlen=span)

		# print('buffer', buffer)

		# buffer.append(indexnumber1)
		# buffer.append(indexnumber2)
		# buffer.append(indexnumber3)
		for _ in range(span):
			buffer.append(data[data_index])

			# check if data_index + 1 is the same as length, if so, data_index is set to 0
			# 2 % 4 = 2    because >>>    0 * 4 + 2 = 2
			data_index = (data_index + 1) % len(data)

		# after the last iteration, data_index = 3
		# now buffer = [indexnumber1, indexnumber2, indexnumber3]

		# // = a floor division
		# 8 // 2 = 4, 9 // 2 = 4, 7 // 3 = 2
		for i in range(batch_size // num_skips):
			# [ skip_window target skip_window ]
			target = skip_window  # target label at the center of the buffer
			targets_to_avoid = [ skip_window ]

			for j in range(num_skips):

				# first you check if target is still in the targets_to_avoid list
				# it loops untill it is not the same value as target anymore
				# example: target = 1
				while target in targets_to_avoid:
					# first iteration is always true
					# target is reset to a random ? (why random?) (and why do this?)
					target = random.randint(0, span - 1)
					# only break the loop if the number is not in the loop yet

				# add the number to the list, if it is not in the targets_to_avoid yet!
				targets_to_avoid.append(target)

				# here we start to fill the batch with numbers > targets
				# these numbers are index numbers (of the targets)
				# first iteration: batch[0] = buffer[1] > '0', the index number for "gutenbergs" (the second word in the input file)
				batch[i * num_skips + j] = buffer[skip_window]

				# first iteration: labels[0,0] = buffer[randomnumber] > it should be either buffer[0] or buffer[2]
				labels[i * num_skips + j, 0] = buffer[target]

			# data_index = 3, why is this appended to the buffer??
			buffer.append(data[data_index])

			# and why is +1 done on data_index again?
			data_index = (data_index + 1) % len(data)

		return batch, labels

	# creation of batch with word + left/right window word
	batch, labels = generate_batch(batch_size=128, num_skips=2, skip_window=1)

	# print('batch:', batch)
	# print('labels:', labels)

	export('training-words.txt', batch)
	export('training-window-words.txt', labels)

	# loop through the batch that is generated, and print all targets with their window+left and window+right
	inspectBatch = []
	for i in range(128):
		try: 
			inspectBatch.append('\n\nbatch item: '+str(i))
			inspectBatch.append('\ntarget index: '+str(batch[i]))
			inspectBatch.append('\ntarget label: '+str(reverse_dictionary[batch[i]]))
			inspectBatch.append('\nwindow index: '+str(labels[i,0]))
			inspectBatch.append('\nwindow label: '+str(reverse_dictionary[labels[i,0]]))
			# print(batch[i], reverse_dictionary[batch[i]],
				# '->', labels[i, 0], reverse_dictionary[labels[i, 0]])
		except:
			e = sys.exc_info()[0]
			print('*error catched*', e)
			continue

	inspectBatch = ''.join(inspectBatch)
	# print(inspectBatch)

	export('training-bacth-inspection.txt', inspectBatch)


	# ***********************************************************************************
	# Step 4: Build and train a skip-gram model.
	# ***********************************************************************************
	"""
		Step 4: Build and train a skip-gram model.
	"""
	batch_size = 128
	embedding_size = 20		# Dimension of the embedding vector.
	skip_window = 1   	# How many words to consider left and right.
	num_skips = 2       	# How many times to reuse an input to generate a label.

	# create a graph object to project in
	graph = tf.Graph()


	# # We pick a random validation set to sample nearest neighbors. Here we limit the
	# # validation samples to the words that have a low numeric ID, which by
	# # construction are also the most frequent.

	# Initial default settings:
	# valid_size = 16     	# Random set of words to evaluate similarity on.
	# valid_window = 100  	# Only pick dev samples in the head of the distribution.
	# valid_examples = np.random.choice(valid_window, valid_size, replace=False)
	# num_sampled = 64    	# Number of negative examples to sample.
	# 						# 100 - 16 = 84? or: 100 - 16 - 20 (embedding_size) = 64

	# ALGOLIT settings: Pick you own set of validation words!
	# You can use the file dictionary.txt, to look up the index numbers of certain words.
	wordlist = [
		'human',
		'learning',
		'system'
	]
	valid_examples= []
	for word in wordlist:
		if word in dictionary:
			index = dictionary[word]
			valid_examples.append(index)
	valid_size = len(valid_examples)	# Number of words to evaluate similarity on.
	valid_window = 100					# Only pick the 100 samples in the head of the distribution.
	num_sampled = valid_window - embedding_size - valid_size    	
										# Number of negative examples to sample.

	# the sections in quotes come from the Tensorflow tutorial:
	# https://www.tensorflow.org/versions/r0.11/tutorials/word2vec/


	with graph.as_default():

		# Input data.
		# initiate array objects
		train_inputs = tf.placeholder(tf.int32, shape=[batch_size])
		train_labels = tf.placeholder(tf.int32, shape=[batch_size, 1])
		valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

		# Ops and variables pinned to the CPU because of missing GPU implementation
		# with tf.device('/cpu:0'):
		# Look up embeddings for inputs.

		# Algolit adatpion:
		# What is a tensor?
		# https://cs224d.stanford.edu/lectures/CS224d-Lecture7.pdf#7

		# **************
		# *** tensor 1 ***
		# **************
		embeddings = tf.Variable(tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0))

		# define our embedding matrix
		# "This is just a big random matrix to start. We'll initialize the values to be uniform in the unit cube."

		# print('tensor 1 (embeddings):', embeddings)
		# output: Tensor("Variable/read:0", shape=(5000, 20), dtype=float32)
		# matrix = 5000 x 20

		# Algolit adaption: adding a session to be able to print the processes that happen within the tensors
		sess = tf.InteractiveSession()
		bigrandommatrix = tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0).eval()
		# print('tensor 1 (bigrandommatrix)', bigrandommatrix)
		export('big-random-matrix.txt', bigrandommatrix)
		sess.close()

		# **************
		# *** tensor 2 ***
		# **************
		embed = tf.nn.embedding_lookup(embeddings, train_inputs)

		# Algolit adaption:
		# What does this do?
		# It selects rows from the matrix, following the batch_size (currently 128). 
		# "embed" = 128 x 20

		# https://stackoverflow.com/questions/34870614/what-does-tf-nn-embedding-lookup-function-do
		# embedding_lookup function retrieves rows of the params tensor.

		# print('tensor 2 (embed):', embed)
		# output: Tensor("embedding_lookup:0", shape=(128, 20), dtype=float32)

		# **************
		# *** tensor 3 ***
		# **************
		nce_weights = tf.Variable(tf.truncated_normal([vocabulary_size, embedding_size], stddev=1.0 / math.sqrt(embedding_size)))

		# Construct the variables for the NCE loss (noise-contrastive estimation)
		# "The noise-contrastive estimation loss is defined in terms of a logistic regression model. For this, we need to define the weights and biases for each word in the vocabulary (also called the output weights as opposed to the input embeddings). So let's define that."
		# calculation of loss

		# print('tensor 3 (nce_weights):', nce_weights)
		# output: (nce_weights): Tensor("Variable_1/read:0", shape=(5000, 20), dtype=float32)

		# **************
		# *** tensor 4 ***
		# **************
		nce_biases = tf.Variable(tf.zeros([vocabulary_size]))

		# print('tensor 4 (nce_biases):', nce_biases)
		# output: Tensor("Variable_2/read:0", shape=(5000,), dtype=float32)

		# **************
		# *** Tensor 5 ***
		# **************
		loss = tf.reduce_mean(tf.nn.nce_loss(nce_weights, nce_biases, embed, train_labels, num_sampled, vocabulary_size))

		# Compute the average NCE loss for the batch.
		# tf.nce_loss automatically draws a new sample of the negative labels each
		# time we evaluate the loss.
		# reduce_mean > creates an average of all the inputs

		# print('tensor 5 (loss)', loss)
		# output: Tensor("Mean:0", shape=(), dtype=float32)

		# Algolit adaption: *This is where the matrix is updated!!!*
		# original:
		# optimizer = tf.train.GradientDescentOptimizer(1.0).minimize(loss)

		algorithm = 'Adam'
		if algorithm == 'GradientDescent':
			optimizer = tf.train.GradientDescentOptimizer(0.1).minimize(loss)
			# Algolit adaption: suggestion Mike, smaller learning rate.
		if algorithm == 'Adam':
			optimizer = tf.train.AdamOptimizer(0.01).minimize(loss)
			# Algolit adaption: suggestion Mike, use the Adam algorithm

		# Construct the SGD optimizer using a learning rate of 1.0.

		# print('tensor 5 (optimizer):', optimizer)
		# output: 
		# name: "GradientDescent"
		# op: "NoOp"
		# input: "^GradientDescent/update_Variable/ScatterSub"
		# input: "^GradientDescent/update_Variable_1/ScatterSub"

		# **************
		# *** tensor 6 ***
		# **************
		norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keep_dims=True))

		# Compute the cosine similarity between minibatch examples and all embeddings.

		# print('tensor 6 (norm):', norm)
		# output: Tensor("Sqrt:0", shape=(5000, 1), dtype=float32)

		# **************
		# *** tensor 7 ***
		# **************
		normalized_embeddings = embeddings / norm

		# print('tensor 7 (normalized_embeddings)', normalized_embeddings)
		# output: Tensor("truediv:0", shape=(5000, 20), dtype=float32)

		# **************
		# *** tensor 8 ***
		# **************
		valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings, valid_dataset)

		# print('tensor 8 (valid_embeddings):', valid_embeddings)
		# output: Tensor("embedding_lookup_1:0", shape=(5, 20), dtype=float32)

		# **************
		# *** tensor 9 ***
		# **************
		similarity = tf.matmul(valid_embeddings, normalized_embeddings, transpose_b=True)

		# print('tensor 9 (similarity):', similarity)
		# output: Tensor("MatMul_1:0", shape=(5, 5000), dtype=float32)

		# **************
		# *** tensor 10 ***
		# **************
		init = tf.initialize_all_variables()

		# Add variable initializer.

		# print('tensor 10 (init):', init)
		# output: 
		# (init): name: "init"
		# op: "NoOp"
		# input: "^Variable/Assign"
		# input: "^Variable_1/Assign"
		# input: "^Variable_2/Assign"

	# ***********************************************************************************
	# Step 5: Begin training.
	# ***********************************************************************************
	np.set_printoptions(threshold=np.inf) # allows to print full arrays / prevents printing truncated representations of arrays
	"""
		Step 5: Begin training.
	"""

	print('Step 5: Begin training.')

	# num_steps = 1
	num_steps = 100001

	with tf.Session(graph=graph) as session:
		# We must initialize all variables before we use them.
		init.run()
		print("Initialized")

		average_loss = 0
		for step in xrange(num_steps):
			
			# print('\n\n******************')
			# print('num_steps', num_steps)

			batch_inputs, batch_labels = generate_batch(batch_size, num_skips, skip_window)
			feed_dict = {train_inputs : batch_inputs, train_labels : batch_labels}

			# We perform one update step by evaluating the optimizer op (including it
			# in the list of returned values for session.run()
			_, loss_val = session.run([optimizer, loss], feed_dict=feed_dict)
			average_loss += loss_val

			# Algolit inspection:
			# print('loss_val', loss_val)
			# print('average_loss', average_loss)

			if step % 2000 == 0:
				if step > 0:
					average_loss /= 2000
			  	# The average loss is an estimate of the loss over the last 2000 batches.
			  	print("Average loss at step ", step, ": ", average_loss)
			  	current_loss = average_loss # algolit var
				average_loss = 0

				def wordlistMatrixPlot(wordlistA, wordlistB):
					if plotExport == 'yes':
						wordlists = [wordlistA, wordlistB]
						matrix = embeddings.eval()
						wordlistMatrix = []
						wordlistWords = []
						for wl in wordlists:
							for word in wl:
								if word in dictionary:
									index = dictionary[word]
									vector = matrix[index]
									wordlistMatrix.append(vector)
									wordlistWords.append(word)

						print('wordlistMatrix:', wordlistMatrix)
						print('wordlistWords:', wordlistWords)

						# Algolit adaption: plot tmp matrix vectors to image
						# save as image
						import matplotlib as mpl 
						from matplotlib import pyplot
						import numpy as np

						outputfolder = 'plots/'+filename+'/'
						if not os.path.exists(outputfolder):
							os.makedirs(outputfolder)

						font = {'family': 'monospace',
					        'color':  'black',
					        'weight': 'normal',
					        'size': 8,
				        }
						colorbar = ''
						fig = pyplot.figure(2)
						cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap',['blue','black','red'],256)
						img = pyplot.imshow(wordlistMatrix, interpolation='nearest', cmap = cmap, origin='lower')
						title = ', '.join(wordlistA)+'\n'+', '.join(wordlistB)+'\n\n'+str(wordlistMatrix)
						pyplot.title(title, fontdict=font)
						if step == 0:
							if colorbar != 'added':
								pyplot.colorbar(img,cmap=cmap)
								colorbar = 'added'
						fig.savefig("./plots/"+filename+"/plot-at-step_"+str("{0:0=5d}".format(step))+".png")
					else:
						print('*plotExport is disabled.*')
				
				wordlistA = ['the','it','a']
				wordlistB = ['might','should','could']
				wordlistMatrixPlot(wordlistA, wordlistB)

				# for i, vector in enumerate(wordlistMatrix):
				# 	# cosine similarity calculation
				# 	from sklearn.metrics.pairwise import cosine_similarity
				# 	cos_sim = cosine_similarity(vector, wordlistMatrix)
				# 	print('word: ', wordlistWords[i])
				# 	print('cosine_similarity: ', cos_sim)

				def fullMatrixPlot(matrix):
					# Algolit adaption: plot full matrix vectors to image
					# save as image
					if plotFullMatrixExport == 'yes':
						import matplotlib as mpl 
						from matplotlib import pyplot
						import numpy as np

						outputfolder = 'plots/full-matrix/'
						if not os.path.exists(outputfolder):
							os.makedirs(outputfolder)

						font = {'family': 'monospace',
					        'color':  'black',
					        'weight': 'normal',
					        'size': 15
				        }
						colorbar = 'added'
						fig = pyplot.figure(figsize=(18, 18)) 
						cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap',['blue','black','red'],256)
						img = pyplot.imshow(matrix, interpolation='nearest', cmap = cmap, origin='lower', aspect='auto')
						title = 'full matrix (5000x20) at step '+str(step)+'\n'
						pyplot.title(title, fontdict=font)
						# pyplot.colorbar(img,cmap=cmap)
						fig.savefig(outputfolder+filename+"-full-matrix-at-step_"+str(step)+".png")
						print("*"+outputfolder+filename+"full-matrix-at-step_"+str("{0:0=5d}".format(step))+".png is printed*")
					else:
						print('*plotFullMatrixExport is disabled.*')

				fullMatrixPlot(embeddings.eval())

			# Note that this is expensive (~20% slowdown if computed every 500 steps)
			if step % 2000 == 0:
				print ("\n")
				outputfolder = 'txt/'+filename+'/'
				if not os.path.exists(outputfolder):
					os.makedirs(outputfolder)
				mode = 'a+' if os.path.exists(outputfolder+'logfile.txt') else 'w+'
				with codecs.open(outputfolder+'logfile.txt', mode, 'utf-8') as destination:
					destination.write('step: '+str(step)+'\n')
					destination.write('algorithm: '+algorithm+'\n')
					destination.write('loss value: '+str(current_loss)+'\n')
					sim = similarity.eval()
					try:
						for i in xrange(valid_size):
							valid_word = reverse_dictionary[valid_examples[i]]
							top_k = 10 # number of nearest neighbors
							nearest = (-sim[i, :]).argsort()[1:top_k+1]
							log_str = "Nearest to %s:" % valid_word
							for k in xrange(top_k):
								close_word = reverse_dictionary[nearest[k]]
								close_word = close_word
								log_str = "%s %s," % (log_str, close_word)
								log_str = log_str
							print(log_str)
							destination.write(log_str+'\n')
						destination.write('\n\n')
					except:
						e = sys.exc_info()[0]
						print('*error catched*', e)
						continue
				print ("\n")
				print('*txt/'+filename+'/logfile.txt written*')

				if step >= 5001:
					if current_loss <= 2.0:
						break


		# print('batch_inputs:', batch_inputs)
		# print('batch_labels:', batch_labels)
		export('training-batch_inputs.txt', batch_inputs)
		export('training-batch_labels.txt', batch_labels)

		# Algolit adaption:
		batch_inputs_in_words = []
		batch_labels_in_words = []
		for i in range(128):
			try:
				batch_inputs_in_words.append(str(reverse_dictionary[batch_inputs[i]]))
				batch_labels_in_words.append(str(reverse_dictionary[batch_labels[i,0]]))
			except:
				e = sys.exc_info()[0]
				print('*error catched*', e)
				continue

		export('training-batch_inputs_in_words.txt', batch_inputs_in_words)
		export('training-batch_labels_in_words.txt', batch_labels_in_words)
		# print('batch_inputs_in_words:', batch_inputs_in_words)
		# print('batch_labels_in_words:', batch_labels_in_words)

		# Algolit adaption: print one-hot-vectors of this training batch
		from onehotvector import export_onehotvector_for_trainingbatch
		batch = []
		for i, word in enumerate(batch_inputs_in_words):
			if i % 2 == 0: 
				batch.append(word)
		batch = ' '.join(batch)
		sortedbatch, onehotvector = export_onehotvector_for_trainingbatch(batch)
		output = batch +'\n\n'+ str(sortedbatch) +'\n\n'+ str(onehotvector)
		export('onehotvector-batch.txt', output)
		
		# Algolit adaption:
		# loop through the batch that is generated, and print all targets with their window+left and window+right
		inspectBatch = []
		for i in range(128):
			try:
				if i % 2 == 0:
					inspectBatch.append('\n\nbatch item: '+str(i))
					inspectBatch.append('\nwindow index: '+str(batch_labels[i,0]))
					inspectBatch.append('\nwindow label: '+str(reverse_dictionary[batch_labels[i,0]]))
					inspectBatch.append('\ntarget index: '+str(batch_inputs[i]))
					inspectBatch.append('\ntarget label: '+str(reverse_dictionary[batch_inputs[i]]))
				if i % 2 == 1:
					inspectBatch.append('\n\nbatch item: '+str(i))
					inspectBatch.append('\ntarget index: '+str(batch_inputs[i]))
					inspectBatch.append('\ntarget label: '+str(reverse_dictionary[batch_inputs[i]]))
					inspectBatch.append('\nwindow index: '+str(batch_labels[i,0]))
					inspectBatch.append('\nwindow label: '+str(reverse_dictionary[batch_labels[i,0]]))
			except:
				e = sys.exc_info()[0]
				print('*error catched*', e)

				continue
		inspectBatch = ''.join(inspectBatch)
		# print(inspectBatch)
		export('training-batch-retraced-to-words.txt', inspectBatch)

		# Algolit evaluation inspection:
		# export('step-5-training_tensor-1-embeddings.txt', embeddings.eval())
		# # export('step-5-training_tensor-2-embed.txt', embed)
		# export('step-5-training_tensor-3-nce_weights.txt', nce_weights.eval())
		# export('step-5-training_tensor-4-nce_biases.txt', nce_biases)
		# # export('step-5-training_tensor-5-loss.txt', loss)
		# # export('step-5-training_tensor-5-optimizer.txt', optimizer)
		# export('step-5-training_tensor-6-norm.txt', norm.eval())
		# export('step-5-training_tensor-7-normalized_embeddings-matrix_5000x20.txt', normalized_embeddings.eval())
		# export('step-5-training_tensor-8-valid_embeddings-matrix_'+str(valid_size)+'x20.txt', valid_embeddings.eval())
		export('cosine-similarity-matrix.txt', sim)

		final_embeddings = normalized_embeddings.eval()
		export('final_embeddings-matrix.txt', final_embeddings)

		# Algolit inspection:
		# export matrix as html, in order to generate a PDF and print it on A0
		if exportFullMatrixHTML == 'yes':
			export_matrix = []
			for i, vector in enumerate(final_embeddings):
				word = reverse_dictionary[i]
				print(word)
				newvector = []
				newvector.append('<div class="values">')
				for value in vector:
					value = "{0:.3f}".format(value)
					newvector.append('<span class="val">'+str(value)+'</span>')
					print(value)
				print(newvector)
				newvector.append('</div>')
				newvector.insert(0, '<div class="word">'+word.encode('utf-8')+'</div>')
				newvector.insert(0, '<div class="wrap">')
				newvector.append('</div>')
				print(newvector)
				export_matrix.append(newvector)
			print(export_matrix)
			nm = []
			for vector in export_matrix:
				nv = ' '.join(vector)
				nm.append(nv)
			output = ' '.join(nm)
			with open('txt/'+filename+'/final_embeddings.html','w+') as html:
				template = '''<!DOCTYPE html>
					<html>
					<head>
					   <meta charset="utf8">
					   <link rel="stylesheet" type="text/css" href="final-embeddings_styles.css">
					</head>
					<style>
					</style>
					<body>
						<div>Frankenstein's matrix</div><br>
						<div>Full matrix (5000x20) at step 10000</div>
						<br><br>
						<div id="wrapper">
							{0}
						</div>
					</body>
					</html>
				'''.format(output)
				html.write(template)
				print('*full matrix printed to html*')
		else:
			print('*full matrix export to HTML is disabled*')

	# ***********************************************************************************
	# Step 6: Visualize the embeddings.
	# ***********************************************************************************

	def plot_with_labels(low_dim_embs, labels, wordlist, filename='graphs/'+filename+'.eps', format='eps'):
		"""
			Step 6: Visualize the embeddings.
		"""
		if graphExport == 'yes':
			assert low_dim_embs.shape[0] >= len(labels), "More labels than embeddings"
			plt.figure(figsize=(18, 18))  #in inches
			ax = plt.axes(frameon=False)
			ax.get_xaxis().tick_bottom()
			ax.axes.get_xaxis().set_visible(False)
			ax.axes.get_yaxis().set_visible(False)
			font = {'family': 'monospace',
		        'color':  'black',
		        'weight': 'normal',
		        'size': 8,
	        }
			for i, label in enumerate(labels):
				x, y = low_dim_embs[i,:]
				plt.scatter(x, y)
				if label in wordlist:
					plt.annotate(label,
						fontsize=10,
						xy=(x, y),
						xytext=(5, 2),
						color='red',
						textcoords='offset points',
						ha='right',
						va='bottom')
				else:
					plt.annotate(label,
						fontsize=10,
						xy=(x, y),
						xytext=(5, 2),
						textcoords='offset points',
						ha='right',
						va='bottom')
			plt.savefig(filename)
			print('*graph plotted*')
		else:
			print('*graphExport is disabled.*')

	try:
		from sklearn.manifold import TSNE
		import matplotlib.pyplot as plt

		tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000)

		# Original code:
		# plot_only = 500
		# low_dim_embs = tsne.fit_transform(final_embeddings[:plot_only,:])
		# labels = [reverse_dictionary[i].decode('utf8') for i in xrange(plot_only)]

		# Algolit adaption: 
		# add metadata to the plot
		# metadata = {}
			# dataset
			# word count of selected words
			# algorythm
			# Learning rate
			# Loss

		# Algolit adaption: 
		# select words to be included in the graph
		# indexnumbers of words that will be included in the graph
		indexnumbers = []
		for word in wordlist:
			if word in dictionary:
				index = dictionary[word]
				appearances = count[index][1]
				print(word+'\'s index:', index)
				print('appearances: ', appearances)
				indexnumbers.append(index)
			else:
				print(word+' is not in reverse_dictionary')

		# print('metadata: ', metadata)
		# print('indexnumbers: ', indexnumbers)

		# Add a lot of most used words to the graph, untill there are 500 words in the list indexnumbers
		for i in range(1000):
			try:
				if len(indexnumbers) <= 500:
					word = reverse_dictionary[i]
					word = word.decode('utf-8')

					# do not include stopwords
					from nltk.corpus import stopwords
					stop = set(stopwords.words('english'))

					if word not in stop:
						# print('SELECTED NOT STOPWORD word:', word)

						# to prevent double words
						if i not in indexnumbers:
							indexnumbers.append(i)
			except:
				e = sys.exc_info()[0]
				print('*error catched*', e)

				continue

		# print('indexnumbers: ', indexnumbers)

		# use the selected words, filled up by the most common words up to a list of 500 words
		plot_only = len(indexnumbers)
		low_dim_embs = tsne.fit_transform(final_embeddings[indexnumbers,:])
		labels = [reverse_dictionary[i].decode('utf8') for i in indexnumbers]

		# print('final_embeddings:', final_embeddings[indexnumbers,:])

		plot_with_labels(low_dim_embs, labels, wordlist)

	except ImportError:
		print("Please install sklearn, matplotlib, and scipy to visualize embeddings.")
