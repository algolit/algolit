import sys, os
import gensim, logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

test_data_dir = '{}'.format(os.sep).join([gensim.__path__[0], 'test', 'test_data']) + os.sep
lee_train_file = '/Users/cristinacochior/Documents/Algolit/algolit/algoliterary_encounter/word2vec/input/mankind-in-the-making_stripped.txt'

class MyText(object):
    def __iter__(self):
        for line in open(lee_train_file):
            # assume there's one document per line, tokens separated by whitespace
            yield line.lower().split()

sentences = MyText()

model = gensim.models.Word2Vec(sentences, min_count=10)

model = gensim.models.Word2Vec(sentences, size=200)
model.save('/Users/cristinacochior/Documents/Algolit/algolit/algoliterary_encounter/word2vec/firstgensimmodel')
print model.most_similar(positive=['man', 'mankind'], negative=['woman'], topn=1)
