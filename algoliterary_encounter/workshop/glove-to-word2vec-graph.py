# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# This is a combination of two scripts: 
# 1. racist_bias.py by Rob Speer
# 2. word2vec_bias.py from the introduction tutorial by Tensorflow
#
# 1. load word-embeddings
# 2. translate word-embeddings to graph
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# from racist_bias.py
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

import codecs
import numpy as np
# import pandas as pd

def load_embeddings(filename):
	"""
	Load a DataFrame from the generalized text format used by word2vec, GloVe,
	fastText, and ConceptNet Numberbatch. The main point where they differ is
	whether there is an initial line with the dimensions of the matrix.
	"""
	labels = []
	rows = []
	with codecs.open(filename, encoding='utf-8') as infile:
		for i, line in enumerate(infile):
			items = line.rstrip().split(' ')
			if len(items) == 2:
				# This is a header row giving the shape of the matrix
				continue
			labels.append(items[0])
			values = np.array([float(x) for x in items[1:]], 'f')
			rows.append(values)
	
	arr = np.vstack(rows)
	return rows, labels, arr
	# return pd.DataFrame(arr, index=labels, dtype='f'), rows

embeddings, labels, array = load_embeddings('data/glove.42B.300d_part.txt')

embeddings = array
# print(embeddings)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# from word2vec_basic.py
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# ***********************************************************************************
# Step 6: Visualize the embeddings.
# ***********************************************************************************

def plot_with_labels(low_dim_embs, labels, wordlist, filename='graph-500-least.png', format='png'):
	"""
		Step 6: Visualize the embeddings.
	"""
	assert low_dim_embs.shape[0] >= len(labels), "More labels than embeddings"
	plt.figure(figsize=(18, 18))  #in inches
	ax = plt.axes(frameon=False)
	ax.get_xaxis().tick_bottom()
	ax.axes.get_xaxis().set_visible(False)
	ax.axes.get_yaxis().set_visible(False)
	font = {'family': 'monospace',
        'color':  'black',
        'weight': 'normal',
        'size': 8,
    }
	for i, label in enumerate(labels):
		x, y = low_dim_embs[i,:]
		plt.scatter(x, y)
		if label in wordlist:
			plt.annotate(label,
				fontsize=10,
				xy=(x, y),
				xytext=(5, 2),
				color='red',
				textcoords='offset points',
				ha='right',
				va='bottom')
		else:
			plt.annotate(label,
				fontsize=10,
				xy=(x, y),
				xytext=(5, 2),
				textcoords='offset points',
				ha='right',
				va='bottom')
	plt.savefig(filename)
	print('*graph plotted*')

try:
	from sklearn.manifold import TSNE
	import matplotlib.pyplot as plt

	tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000)

	wordlist = [
		'human',
		'learning',
		'system'
	]

	low_dim_embs = tsne.fit_transform(embeddings[-500:])
	plot_with_labels(low_dim_embs, labels[-500:], wordlist)

except ImportError:
	print("Please install sklearn, matplotlib, and scipy to visualize embeddings.")
