### Following this tutorial in Python3:
### https://gist.github.com/rspeer/ef750e7e407e04894cb3b78a82d66aed#file-how-to-make-a-racist-ai-without-really-trying-ipynb

import numpy as np
import pandas as pd
import matplotlib
import seaborn
import re
import statsmodels.formula.api
import os

from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Configure how graphs will show up in this notebook
#%matplotlib inline
seaborn.set_context('notebook', rc={'figure.figsize': (10, 6)}, font_scale=1.5)

def load_embeddings(filename):
	"""
	Load a DataFrame from the generalized text format used by word2vec, GloVe,
	fastText, and ConceptNet Numberbatch. The main point where they differ is
	whether there is an initial line with the dimensions of the matrix.
	"""
	labels = []
	rows = []
	with open(filename, encoding='utf-8') as infile:
		for i, line in enumerate(infile):
			items = line.rstrip().split(' ')
			if len(items) == 2:
				# This is a header row giving the shape of the matrix
				continue
			labels.append(items[0])
			values = np.array([float(x) for x in items[1:]], 'f')
			rows.append(values)
	
	arr = np.vstack(rows)
	return pd.DataFrame(arr, index=labels, dtype='f')

embeddings = load_embeddings('data/glove.42B.300d_part.txt')
embeddings.shape

def load_lexicon(filename):
	"""
	Load a file from Bing Liu's sentiment lexicon
	(https://www.cs.uic.edu/~liub/FBS/sentiment-analysis.html), containing
	English words in Latin-1 encoding.
	
	One file contains a list of positive words, and the other contains
	a list of negative words. The files contain comment lines starting
	with ';' and blank lines, which should be skipped.
	"""
	lexicon = []
	with open(filename, encoding='latin-1') as infile:
		for line in infile:
			line = line.rstrip()
			if line and not line.startswith(';'):
				lexicon.append(line)
	return lexicon

pos_words = load_lexicon('data/positive-words.txt')
neg_words = load_lexicon('data/negative-words.txt')

# print(pos_words[:10])
# print(neg_words[:10])

# Algolit adaption:
# save data to txt file
def export(fn, data):
    outputdir = 'txt/'
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    with open(outputdir+fn,'w+') as output:
        output.write(str(data))
        print('\n\n*exported '+fn+'*, with the following content:')
        print(data)

#the data points here are the embeddings of these positive and negative words. 
#We use the Pandas .loc[] operation to look up the embeddings of all the words.
pos_vectors = embeddings.loc[pos_words]
neg_vectors = embeddings.loc[neg_words]
# print(pos_vectors)
# print(neg_vectors)

#Some of these words are not in the GloVe vocabulary, particularly the misspellings such as "fancinating". 
#Those words end up with rows full of NaN to indicate their missing embeddings, so we use .dropna() to remove them.
pos_vectors = embeddings.loc[pos_words].dropna()
neg_vectors = embeddings.loc[neg_words].dropna()
# print(pos_vectors)
# print(neg_vectors)

'''
Now we make arrays of the desired inputs and outputs. 
The inputs are the embeddings, and the outputs are 1 for positive words and -1 for negative words. 
We also make sure to keep track of the words they're labeled with, so we can interpret the results.
'''
vectors = pd.concat([pos_vectors, neg_vectors])
targets = np.array([1 for entry in pos_vectors.index] + [-1 for entry in neg_vectors.index])
labels = list(pos_vectors.index) + list(neg_vectors.index)

# print(vectors) # cfr 77/78: matrix of [55 rows x 300 columns] for positive words & [12 rows x 300 columns]
# print(targets) # this is 1 for positive words & -1 for negative words
# print(labels) # these are the words (labels) of the lexicon that are present in the Glove trainingdata

export('vectors.txt', vectors)
export('targets.txt', targets)
export('labels.txt', labels)

'''
Using the scikit-learn train_test_split function, we simultaneously separate the input vectors, 
output values, and labels into training and test data, with 10% of the data used for testing.
'''
train_vectors, test_vectors, train_targets, test_targets, train_labels, test_labels = \
    train_test_split(vectors, targets, labels, test_size=0.1, random_state=0)

'''
Now we make our classifier, and train it by running the training vectors through it for 100 iterations. 
We use a logistic function as the loss, so that the resulting classifier can output the probability 
that a word is positive or negative.
'''
model = SGDClassifier(loss='log', random_state=0, n_iter=100)
model.fit(train_vectors, train_targets)
#print(model)

'''
We evaluate the classifier on the test vectors. 
It predicts the correct sentiment for sentiment words outside of its training data 95% of the time. 
'''
score = (accuracy_score(model.predict(test_vectors), test_targets))
print(score)

'''
Let's define a function that we can use to see the sentiment that this classifier predicts for particular words, 
then use it to see some examples of its predictions on the test data.
'''
def vecs_to_sentiment(vecs):
    # predict_log_proba gives the log probability for each class
    predictions = model.predict_log_proba(vecs)
    # print('predictions:', predictions)
    export('predictions.txt', predictions)

    # To see an overall positive vs. negative classification in one number,
    # we take the log probability of positive sentiment minus the log
    # probability of negative sentiment.
    return predictions[:, 1] - predictions[:, 0]

def words_to_sentiment(words):
    vecs = embeddings.loc[words].dropna()
    # print('vecs:',vecs)
    log_odds = vecs_to_sentiment(vecs)
    return pd.DataFrame({'sentiment': log_odds}, index=vecs.index)


# Show 20 examples from the test set
samples = words_to_sentiment(test_labels).ix[:20]
export('samples.txt', samples)


'''
There are many ways to combine sentiments for word vectors into an overall sentiment score. 
Again, because we're following the path of least resistance, we're just going to average them.
'''

import re
TOKEN_RE = re.compile(r"\w.*?\b")
# The regex above finds tokens that start with a word-like character (\w), and continues
# matching characters (.+?) until the next word break (\b). It's a relatively simple
# expression that manages to extract something very much like words from text.


def text_to_sentiment(text):
    tokens = [token.casefold() for token in TOKEN_RE.findall(text)]
    sentiments = words_to_sentiment(tokens)
    export('sentiments_'+text+'.txt', text+'\n\n'+str(sentiments))
    export('sentiments_'+text+'.mean.txt', text+'\n\n'+str(sentiments['sentiment'].mean()))
    return sentiments['sentiment'].mean()


# # we can roughly compare the relative positivity of different sentences

# print("this example is pretty cool", text_to_sentiment("this example is pretty cool"))
# print("this example is okay", text_to_sentiment("this example is okay"))
# print("meh, this example sucks", text_to_sentiment("meh, this example sucks"))

# # Not every sentence is going to contain obvious sentiment words. 
# # Let's see what it does with a few variations on a neutral sentence

# print("Let's go get Italian food", text_to_sentiment("Let's go get Italian food"))
# print("Let's go get Chinese food", text_to_sentiment("Let's go get Chinese food"))
# print("Let's go get Mexican food", text_to_sentiment("Let's go get Mexican food"))

# # Names don't seem to be neutral
# print("My name is Emily", text_to_sentiment("My name is Emily"))
# print("My name is Heather", text_to_sentiment("My name is Heather"))
# print("My name is Yvette", text_to_sentiment("My name is Yvette"))
# print("My name is Shaniqua", text_to_sentiment("My name is Shaniqua"))


# Algolit sentences
sentences = [
    'Obama fried eggs.',
    'Terrorist',
    'French love'
]
for sentence in sentences:
    print(sentence, text_to_sentiment(sentence))


'''
We want to learn how to not make something like this again. 
So let's put more data through it, and statistically measure how bad its bias is.
Here we have four lists of names that tend to reflect different ethnic backgrounds, 
mostly from a United States perspective. The first two are lists of predominantly "white" and "black" names 
adapted from Caliskan et al.'s article. I also added typically Hispanic names, as well as Muslim names 
that come from Arabic or Urdu; these are two more distinct groupings of given names that tend to represent 
your background.
This data is currently used as a bias-check in the ConceptNet build process, 
and can be found in the conceptnet5.vectors.evaluation.bias module. 
I'm interested in expanding this to more ethnic backgrounds, which may require looking at surnames 
and not just given names.
'''

NAMES_BY_ETHNICITY = {
    # The first two lists are from the Caliskan et al. appendix describing the
    # Word Embedding Association Test.
    'White': [
        'Adam', 'Chip', 'Harry', 'Josh', 'Roger', 'Alan', 'Frank', 'Ian', 'Justin',
        'Ryan', 'Andrew', 'Fred', 'Jack', 'Matthew', 'Stephen', 'Brad', 'Greg', 'Jed',
        'Paul', 'Todd', 'Brandon', 'Hank', 'Jonathan', 'Peter', 'Wilbur', 'Amanda',
        'Courtney', 'Heather', 'Melanie', 'Sara', 'Amber', 'Crystal', 'Katie',
        'Meredith', 'Shannon', 'Betsy', 'Donna', 'Kristin', 'Nancy', 'Stephanie',
        'Bobbie-Sue', 'Ellen', 'Lauren', 'Peggy', 'Sue-Ellen', 'Colleen', 'Emily',
        'Megan', 'Rachel', 'Wendy'
    ],

    'Black': [
        'Alonzo', 'Jamel', 'Lerone', 'Percell', 'Theo', 'Alphonse', 'Jerome',
        'Leroy', 'Rasaan', 'Torrance', 'Darnell', 'Lamar', 'Lionel', 'Rashaun',
        'Tyree', 'Deion', 'Lamont', 'Malik', 'Terrence', 'Tyrone', 'Everol',
        'Lavon', 'Marcellus', 'Terryl', 'Wardell', 'Aiesha', 'Lashelle', 'Nichelle',
        'Shereen', 'Temeka', 'Ebony', 'Latisha', 'Shaniqua', 'Tameisha', 'Teretha',
        'Jasmine', 'Latonya', 'Shanise', 'Tanisha', 'Tia', 'Lakisha', 'Latoya',
        'Sharise', 'Tashika', 'Yolanda', 'Lashandra', 'Malika', 'Shavonn',
        'Tawanda', 'Yvette'
    ],
    
    # This list comes from statistics about common Hispanic-origin names in the US.
    'Hispanic': [
        'Juan', 'José', 'Miguel', 'Luís', 'Jorge', 'Santiago', 'Matías', 'Sebastián',
        'Mateo', 'Nicolás', 'Alejandro', 'Samuel', 'Diego', 'Daniel', 'Tomás',
        'Juana', 'Ana', 'Luisa', 'María', 'Elena', 'Sofía', 'Isabella', 'Valentina',
        'Camila', 'Valeria', 'Ximena', 'Luciana', 'Mariana', 'Victoria', 'Martina'
    ],
    
    # The following list conflates religion and ethnicity, I'm aware. So do given names.
    #
    # This list was cobbled together from searching baby-name sites for common Muslim names,
    # as spelled in English. I did not ultimately distinguish whether the origin of the name
    # is Arabic or Urdu or another language.
    #
    # I'd be happy to replace it with something more authoritative, given a source.
    'Arab/Muslim': [
        'Mohammed', 'Omar', 'Ahmed', 'Ali', 'Youssef', 'Abdullah', 'Yasin', 'Hamza',
        'Ayaan', 'Syed', 'Rishaan', 'Samar', 'Ahmad', 'Zikri', 'Rayyan', 'Mariam',
        'Jana', 'Malak', 'Salma', 'Nour', 'Lian', 'Fatima', 'Ayesha', 'Zahra', 'Sana',
        'Zara', 'Alya', 'Shaista', 'Zoya', 'Yasmin'
    ]
}

'''
Now we'll use Pandas to make a table of these names, their predominant ethnic background, 
and the sentiment score we get for them
'''
def name_sentiment_table():
    frames = []
    for group, name_list in sorted(NAMES_BY_ETHNICITY.items()):
        lower_names = [name.lower() for name in name_list]
        sentiments = words_to_sentiment(lower_names)
        sentiments['group'] = group
        frames.append(sentiments)

    # Put together the data we got from each ethnic group into one big table
    return pd.concat(frames)

name_sentiments = name_sentiment_table()
export('name_sentiments.ix[::].to_dense().txt', name_sentiments.ix[::].to_dense())

'''
Now we can visualize the distribution of sentiment we get for each kind of name
'''
# plot = seaborn.swarmplot(x='group', y='sentiment', data=name_sentiments)
# plot.set_ylim([-10, 10])

'''
And finally we can break out the serious statistical machinery, using the statsmodels package, 
to tell us how big of an effect this is (along with a bunch of other statistics).
---> needs follow-up
'''