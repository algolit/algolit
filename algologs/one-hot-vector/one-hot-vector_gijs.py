import nltk

sentences = [
	"I like deep learning",
	"I like NLP",
	"I enjoy flying"
]

counter = dict()
words = list()

for sentence in sentences:
	for ngram in nltk.bigrams(sentence.split()):
		ngram_sorted = tuple(sorted(ngram))
		if ngram_sorted not in counter:
			counter[ngram_sorted] = 0

		counter[ngram_sorted] += 1

		for word in ngram_sorted:
			if word not in words:
				words.append(word)

words.sort()

matrix = [[counter[tuple(sorted((word1, word2)))] if tuple(sorted((word1, word2))) in counter else 0 for word2 in words] for word1 in words]

"""
'expanded' version of lijn 85
matrix = []
for word1 in words:
	row = []
	for word2 in words:
		key = tuple(sorted([word1, word2]))
		if key in counter:
			row.push(counter[key])
	else:
		row.push(0)
	matrix.push(row)
"""

print("{: >10}".format('') + ' ' + ''.join(["{: <10}".format(word) for word in words]))
for k, word in enumerate(words):
	print("{: >10}".format(word) + ' ' + ''.join(["{: <10}".format(c) for c in matrix[k]]))