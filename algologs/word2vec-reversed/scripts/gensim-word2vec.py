import os
from gensim.models import Word2Vec

# based on the following tutorial: https://rare-technologies.com/word2vec-tutorial/

class MySentences(object):
	def __init__(self, dirname):
		self.dirname = dirname
 
	def __iter__(self):
		for fname in os.listdir(self.dirname):
			for line in open(os.path.join(self.dirname, fname)):
				
				# more text-processing steps can be added here
				# for example: "convert to unicode, lowercase, remove numbers, extract named entities…"

				print(line.split())
				yield line.split() # returns a list of words

sentences = MySentences('./input/test')

model = Word2Vec(sentences, size=100, window=5, min_count=5, workers=4)
# model = Word2Vec(sentences, size=1, window=1, min_count=1, workers=1)

print(model)

