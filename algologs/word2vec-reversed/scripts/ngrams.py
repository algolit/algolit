import sys
from nltk import ngrams

# text = 'this is a test sentence for now'
text = sys.stdin.read()

# out = ngrams(text, 3)

n = 3
out = [text[i:i+n] for i in range(len(text)-n+1)]

print list(out)

out = str(out)

sys.stdout.write(out)

# [('this', 'is', 'a'), ('is', 'a', 'test'), ('a', 'test', 'sentence'), ('test', 'sentence', 'for'), ('sentence', 'for', 'now')]
