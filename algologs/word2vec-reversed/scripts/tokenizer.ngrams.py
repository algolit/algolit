import sys

# >>> std.in
# text = 'this is a test sentence for now'
text = sys.stdin.read()


# >>> tokenizer
from nltk.tokenize import TreebankWordTokenizer

tokenizer = TreebankWordTokenizer()
tokens = tokenizer.tokenize(text)


# >>> ngrams
# from nltk import ngrams
# out = ngrams(tokens, 3)

n = 3
out = [tokens[i:i+n] for i in range(len(tokens)-n+1)]

# print list(out)
# [('this', 'is', 'a'), ('is', 'a', 'test'), ('a', 'test', 'sentence'), ('test', 'sentence', 'for'), ('sentence', 'for', 'now')]


# >>> std.out
out = str(out)
sys.stdout.write(out)