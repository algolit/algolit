import sys
from nltk.tokenize import TreebankWordTokenizer

# text = 'this is a test sentence for now'
text = sys.stdin.read()

tokenizer = TreebankWordTokenizer()
out = tokenizer.tokenize(text)
out = str(out)

sys.stdout.write(out)

print out