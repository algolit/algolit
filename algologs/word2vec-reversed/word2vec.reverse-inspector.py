import sys
import nltk
from nltk.tokenize import TreebankWordTokenizer, sent_tokenize
from nltk import ngrams
from collections import Counter
import json

# # >>> std.in
# text = sys.stdin.read()
# text = text, errors='ignore'
# print(text)

text = open('input/frankenstein_gutenberg_plain.txt','r').read()

# >>> tokenizer
sentences = sent_tokenize(text)
print(sentences)

tokenizer = TreebankWordTokenizer()

data = []
for sentence in sentences: 
	tokens = tokenizer.tokenize(sentence)
	data.append(tokens)
	# print(tokens)
# print(data)
# [['this','is','a','sentence'], ['this','is','another','sentence']]

# >>> ngrams
# ngrams = ngrams(tokens, 3)

n = 3
ngrams = []
for sentence_tokens in data:
	current_ngrams = [sentence_tokens[i:i+n] for i in range(len(sentence_tokens)-n+1)]
	for ngram in current_ngrams:
		ngrams.append(ngram)

# print(list(ngrams))
# [('this', 'is', 'a'), ('is', 'a', 'test'), ('a', 'test', 'sentence'), ('test', 'sentence', 'for'), ('sentence', 'for', 'now')]


# >>> filter 
targets = ['human', 'fellow']

ngrams1 = []
ngrams2 = []
counter1 = Counter()
counter2 = Counter()
# Counter({u'black': 380, u'of': 76, u'the': 50, u'militant': 25, u'people': 21, ... })

for ngram in ngrams:
	if targets[0] == ngram[1]:
		ngrams1.append(ngram)
		for word in ngram:
			# print(word)
			counter1[word] += 1
	if targets[1] == ngram[1]:
		ngrams2.append(ngram)
		for word in ngram:
			counter2[word] += 1

# print('\n---------\n')
# for ngram in ngrams1:
# 	print(ngram)
# print('\n---------\n'
# for ngram in ngrams2:
# 	print(ngram)
# print('\n---------\n'
# print(counter1)
# print('\n---------\n'
# print(counter2)
# print('\n---------\n'

out = {}
for word, value in counter1.items():
	# if the word is not the target word
	if word != any((targets[0], targets[1])):
		# print(word, value)
		# if the word also appeared in the window of the second target word
		if word in counter2:
			val1 = value
			val2 = counter2[word]		
			# print(word, val1, val2)

			sentences1 = []
			sentences2 = []
			bigrams1 = [' '+word+' '+targets[0]+' ', ' '+targets[0]+' '+word+' ']
			bigrams2 = [' '+word+' '+targets[1]+' ', ' '+targets[1]+' '+word+' ']
			for sentence in data:
				if targets[0] in sentence:
					index = sentence.index(targets[0])
					if sentence[index-1] == word:
						sentences1.append(' '.join(sentence))
					if sentence[index+1] == word:
						sentences1.append(' '.join(sentence))
				if targets[1] in sentence:
					index = sentence.index(targets[1])
					if sentence[index-1] == word:
						sentences2.append(' '.join(sentence))
					if sentence[index+1] == word:
						sentences2.append(' '.join(sentence))

			out[word] = {
				targets[0] : {
					'freq' : val1,
					'sentences': sentences1
				},
				targets[1] : {
					'freq' : val2,
					'sentences' : sentences2
				}
			}

# out = {
# 	'of' : {
# 		'human' : {
# 			freq : 23,
# 			sentences: [
# 				'sentence 1', 
# 				'sentence 2'
# 			]
# 		},
# 		'black' : {
# 			freq : 111,
# 			sentences: [
# 				'sentence 1', 
# 				'sentence 2'
# 			]
# 		}
# 	}
# }

# >>> std.out
out = json.dumps(out, sort_keys=True, indent=4, separators=(',', ': '))
sys.stdout.write(out)