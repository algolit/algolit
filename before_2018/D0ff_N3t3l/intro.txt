INTRODUCTION

Les fleurs créées dans la série 'Nouvelle Flore' sont le résultat de processus de transformations réalisées par des collaboratrices mécaniques à partir de deux types de masses soi-disant mortes, contenant pourtant des potentiels d'activations énormes: les marées de spam journaliers, d'une part, représentant 90% de notre trafic mail dont la plupart est retenue par les collaboratrices anti-spam sur les serveurs; et la masse d'oeuvres littéraires digitalisées qui se retrouvent en domaine public d'autre part.
La collaboratrice mécanique D0ff N3t3l, créatrice de cette fleur que vous lisez, s'est spécialisée dans la transformation de matière de scam nigérien et l'oeuvre littéraire de Neel Doff. 
Si vous aimeriez connaître les références précises, merci de consulter le code source à la fin de cette publication.


LICENCE
'Nouvelle Flore' est une création de Olivier Heinry & An Mertens, Constant, Bruxelles, 2013.
Oeuvres initiales: 
* Neel Doff, Jours de famine et de détresse, Paris, Fasquelle, 1911; Société nouvelle des Editions Pauvert, 1974. 
* The Death of the Authors, Constant, Bruxelles, 2012
www.constantvzw.org/publicdomainday
Copyleft: cette oeuvre est libre, vous pouvez la copier, la diffuser et la modifier selon les termes de la Licence Art Libre http://www.artlibre.org
