#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import nltk
import time
import random

# Collaborator and influences
collaborator = 'D0ff N3t3l'
new_flora = "Status sociabilis"

author = "Neel Doff"
year = 1941
works = "Jours de famine et de détresse, Société nouvelle des Editions Pauvert, 1974"
txt_ins = "jours_1974_nettoye_p1-22.txt"

spam = "Nigerian scam"
txt_spam = "dieu.txt"

now = time.strftime("%Y-%m-%d_%H:%M:%S")
filename = new_flora+'_'+now+'.txt'



# Selected fragments
## Miserie
# A l'école catholique, p. 32
#### Rendre références en variables aussi pour le colophon????
miserie1 = """Comme les deux bras de mon père ne pouvaient suffire à nourrir dix bouches, 
et que ma mère, à cause de ses huit enfants, avait dû abandonner son métier de dentellière, 
la misère était continue chez nous. Aussi, de temps à autre, ma mère écrivait-elle à quelques 
dames charitables pour obtenir des secours ; parfois, on nous en donnait."""
# J'entends les puces marcher, p. 45
miserie2 = """Nous habitions une chambre unique, dans une im­passe gluante d'Amsterdam. 
Le soleil n'y pénétrait jamais et si, en hiver, le froid humide y était glacial, 
en été la chaleur moite nous anéantissait. Il n'y avait qu'une alcôve à étage, ainsi 
que dans les barques de pêcheurs, mais cloisonnée : on y était comme dans un placard."""
# Mon père propose de nous abandonner, p. 51
miserie3 = """La propriétaire était venue nous insulter pour les deux semaines de loyer 
que nous lui devions. On s'était couchés après cela, tout agités. Sur les paillasses à terre, 
les enfants s'endormirent vite. Moi, je ne pouvais. Les parents dans l' alcôve, causèrent. 
Mon père proposa à ma mère d' abandonner tous les enfants, disant que la Ville prendrait 
certainement soin d'eux et qu' ils auraient moins souvent faim et froid que maintenant ; 
que lui était à bout de forces, qu' il n' avait que trente-huit ans, qu' elle sans doute 
n' aurait plus d' enfants, et qu ' ils pourraient se refaire une vie à deux."""

## Statements
# Non! Non! p.29
statement1 = """Nous nous regardâmes. Elle avait les yeux bleus et les cheveux blonds bouclés, 
comme moi. Je la comprenais mieux en ce moment que je n'avais jamais compris les gens de ma classe ; 
mais pour­quoi, étant si semblables, était-elle si autre ? Je l' au­rais griffée, je l'aurais 
piétinée pour cette différence, que je ne pouvais comprendre et qui me semblait hostile. """
# Non! Non! p.30
statement2 = """Ces « Non ! Non ! » dits d'une voix énergique, mais délicieuse, par la 
petite demoiselle, m'avaient paru les mots les plus beaux, les plus aristocratiques 
que j'eusse jamais entendus. J'ignorais ce qu'ils voulaient dire, mais je me les étais 
incrustés dans la mémoire."""
# A l'école catholique p. 34
statement3 = """Je préférais la faim, ça me connaissait : la faim est silencieuse et, 
si vous savez vous taire également, elle vous détruit en douceur."""

## Souvenirs
# Mes parents, p. 13
souvenir1 = """Nulle part, autant que chez nous, je n'ai entendu parler de beauté. 
Quand nous nous rêvions riches, nous nous entretenions surtout de ce que nous aurions appris, 
de toutes les belles choses dont nous nous serions entourés, et, pour des affamés comme nous, 
la nourriture ne venait qu'en dernier lieu. """
# Non! Non!, p. 26-27
souvenir2 = """Je choisissais une marche du perron et vidais mon sac : je disposais mes morceaux 
de faïence tout autour de la marche, comme des plats sur un dressoir, et asseyais ma poupée au 
milieu. Tout en jouant, mon esprit se délectait dans des rêves qui se passaient à l'intérieur 
de la maison. J'y habitais en compagnie des personnages des contes de Perrault. J'avais des 
salles remplies de poupées de toute gran­deur, habillées comme les princesses des images d'Epinal : 
elles étaient coiffées de vraies chevelures, avaient des yeux qui s'ouvraient et se fermaient, 
et elles disaient « Papa » et « Maman ». """
# Catéchisme et première communion, p.43
souvenir3 = """Toutes les fillettes étaient un peu pâles d'être à jeun ; moi, 
cela ne me faisait rien, j'étais entraînée."""

# measure weight of text
poids = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2, 
         "f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3, 
         "l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1, 
         "r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4, 
         "x": 8, "z": 10}
masse = ()
         
#def masse_volumique(word):
#    masse = 0
#    for i in word.lower():
#        if i in poids:
#            masse += poids[i]
#    return masse

# Write text to new document

def writetoLog(content):
		try:
			logfile = open(filename, "a")
			try:
				logfile.write(content)
			finally:
				logfile.close()
		except IOError:
			pass


# Open text 
#### CREATE FUNCTION!!!!
#def open_file(txt, name):
#	file = open(txt, "r")
#	name = file.read()
#	name = name.replace("\r\n", " ")
#	name = name.replace("\r", " ")
#	name = name.replace("\n", " ")
#	name = name.replace("  ", " ")
#
#open_file(txt_ins, text_ins)
#open_file(txt_spam, text_spam)

## Open spam 
file = open(txt_spam, "r")
text_spam = file.read()
text_spam = text_spam.replace("\r\n", " ")
text_spam = text_spam.replace("\r", " ")
text_spam = text_spam.replace("\n", " ")
text_spam = text_spam.replace("  ", " ")

## Open text fragment
file = open(txt_ins, "r")
text_ins = file.read()
text_ins = text_ins.replace("\r\n", " ")
text_ins = text_ins.replace("\r", " ")
text_ins = text_ins.replace("\n", " ")
text_ins = text_ins.replace("  ", " ")

# Split text_spam into sentences with help of nltk
sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')
sentences_spam = sent_tokenizer.tokenize(text_spam)
sentences_ins = sent_tokenizer.tokenize(text_ins)

# Turn string spam into list
sentences_spam_l = list(sentences_spam)
sentences_ins_l = list(sentences_ins)

########print sentences_spam_l
########print sentences_ins_l

# Select sentence in spam that contains crucial keywords

# Sets
#### CREATE FUNCTION to insert different fragments in different places

# Je vous raconte mon histoire: description de la misère

a = "J'ai connu la misère:"
miserie = [miserie1.upper(),miserie2.upper(), miserie3.upper()]

mi = random.sample(miserie, 1)

select_sentence_mi = [x for x in sentences_spam_l if 'Je' and 'suis' in x]

## Define position of select_sentence in sentences_spam_l
for i in select_sentence_mi:
	if i in sentences_spam_l:
		x = sentences_spam_l.index(i)

#print x

mi_ins = "".join(mi)

## Insert sentence a and fragment s_ins after selected_sentence
#### QUESTION: comment insérer saut à la ligne ici?
sentences_spam_l.insert(x+1, a)
sentences_spam_l.insert(x+2, mi_ins)

# Voici ma leçon de vie sur la pauvreté: statement

b = "J'ai appris aussi:"
statements = [statement1.upper(),statement2.upper(), statement3.upper()]

st = random.sample(statements, 1)

select_sentence_st = [x for x in sentences_spam_l if 'maladies' or 'cancer' or 'diabète' in x]

## Define position of select_sentence in sentences_spam_l
for i in select_sentence_st:
	if i in sentences_spam_l:
		x = sentences_spam_l.index(i)

#print x

st_ins = "".join(st)

## Insert sentence a and fragment s_ins after selected_sentence
#### QUESTION: comment insérer saut à la ligne ici?
sentences_spam_l.insert(x+1, b)
sentences_spam_l.insert(x+2, st_ins)


# Mais j'ai survécu la misère et plus, j'ai fait fortune grâce à ce souvenir d'enfance joyeux: 

c = "Et je me suis faite une vie grâce à ce souvenir joyeux:"
souvenirs = [souvenir1.upper(),souvenir2.upper(), souvenir3.upper()]

sou = random.sample(souvenirs, 1)

#### QUESTION: how to select only the first encounter?
select_sentence_sou = [x for x in sentences_spam_l if 'argent' in x]

## Define position of select_sentence in sentences_spam_l
for i in select_sentence_sou:
	if i in sentences_spam_l:
		x = sentences_spam_l.index(i)

#print x

sou_ins = "".join(sou)

## Insert sentence a and fragment s_ins after selected_sentence
#### QUESTION: comment insérer saut à la ligne ici?
sentences_spam_l.insert(x, c)
sentences_spam_l.insert(x+1, sou_ins)

#print sentences_spam_l

# Measure weight of words of the text (cfr scrabble)
sentences = " ".join(sentences_spam_l)
print sentences

## Split sentences into words
zin = sentences.split(" ")

## Count the weight of each word
def count(x):
	masse = 0
	for word in x:
		for letter in word:
			if letter in poids:
				masse += poids[letter]
	return masse


masse = count(zin)

# Write sentences back into file
# Open file and write book

#def write():
writetoLog("NOUVELLE FLORA: 'Status sociabilis'\n\n")
writetoLog("Taille: "+str(len(sentences)*8)+" bits \n")
writetoLog("Masse volumique: "+str(masse)+" \n\n")
	
writetoLog('après un processus de transformation réalisé par '+collaborator+'\n\n')
	
writetoLog("LES MOTS ECRITS COMME CECI appartiennent à l'oeuvre de "+author+" \n\n\n")

for sentence in sentences:
	writetoLog(sentence)

# Write colophon
## Take this out when you want to do fluid editing and printing!
writetoLog("\n\n\n\n")

file = open("append.txt", "r")
append = file.read()
writetoLog(append)

#write()

# Influence masse volumique
 
while x in range(10):
    result = []
    for words in zin:
	     if len(words) > 5:
	         result.append(words)  
   
    test = random.choice(result)

    print "La masse volumique de cette nouvelle fleur est de "+ str(masse)+"\nVous pouvez l'influencer!"
    syn=raw_input('Donnez un synonyme pour '+test+' : ')
    
    sentences.replace(test,syn)
    zin = sentences.split(" ")
    masse = count(zin)
    
    writetoLog("NOUVELLE FLORA: 'Status sociabilis'\n\n")
    writetoLog("Taille: "+str(len(sentences)*8)+" bits \n")
    writetoLog("Masse volumique: "+str(masse)+" \n\n")
    writetoLog('après un processus de transformation réalisé par '+collaborator+'\n\n')
    writetoLog("LES MOTS ECRITS COMME CECI appartiennent à l'oeuvre de "+author+" \n\n\n")
    
    for sentence in sentences:
        writetoLog(sentence)
    
	#if x == 9:
#		break

# Write colophon
## Take this out when you want to do fluid editing and printing!
writetoLog("\n\n\n\n")

file = open("append.txt", "r")
append = file.read()
writetoLog(append)

