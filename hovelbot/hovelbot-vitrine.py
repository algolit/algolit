#!/usr/bin/python
# -*- coding: utf-8 -*-

from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket
from datetime import datetime
from geoip import geolite2
from subprocess import check_output
#from geopy.geocoders import Nominatim

import re
import sys
import json
import random
import threading
import time
import pygeoip

print 'Hello hovelbot...'

file = open('/home/pi/templates.txt', 'r')
template_txts = file.read()
templates = template_txts.split('//////////')
user_locs = {}
stories = {} 
clients = []

class WSClient(WebSocket):
    def handleMessage(self):
        # echo message back to client
        self.sendMessage(self.data)

    def handleConnected(self):
        #print self.address, 'connected'
        clients.append(self)

    def handleClose(self):
        #print self.address, 'closed'
        clients.remove(self)

idents = {}

date_pattern = re.compile('([A-Za-z])+ [0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}')
ident_pattern = re.compile('DHCP (?:[0-9]{1,3}\.){3}[0-9]{1,3}')
query_pattern = re.compile('query.* from ((?:[0-9]{1,3}\.){3}[0-9]{1,3})')

#def handle_ident_str(str):
#  txt = str.split('DHCP')
#  elems = txt[len(txt) - 1].split(' ')
#  idents[elems[1]] = elems[3].strip()

#print date_pattern.search('May 27 12:11:38').group()


with open('/var/log/daemon.log') as f:
    for line in f:
        pass
    date_str = date_pattern.search(line).group()
    last_date = datetime.strptime(date_str, '%b %d %H:%M:%S')

def story(ident):
    global user_locs, stories, templates
    if ident in stories:
        return stories[ident]
    else:
        template = templates[random.randint(0, len(templates) - 1)].decode('utf-8').encode('utf-8')
        story = template.replace('[nickname]', ident)

        prev_story = ''
        locs = user_locs[ident]
        loc_index = len(locs) 

        while prev_story != story:
          prev_story = story
          if loc_index > 0:
            loc_index -= 1
          loc = locs[loc_index]
          if loc == None:
            loc = 'Unknown'
          story = story.replace('[location]', loc, 1)
        stories[ident] = story
        return story

geodata = pygeoip.GeoIP('/home/pi/GeoLiteCity.dat')

def location(url):
    text =[]
    elements = []

    try:
      #out = check_output(['traceroute', url])
      out = check_output(['dig', url])
    except:
      return "nowhere"

    #text = out.split('\n')
    # # extract IPaddress
    #elements = text[0]
    #ip = elements[elements.find("(")+1:elements.find(")")]
    diglist = out.split(";;")
    answers = []
    pick = []
    for el in diglist:
      if "ANSWER SECTION:" in el:
        pick = el.split("\n")

    if len(pick) < 2:
      return ["Nowhere"]
      
    el = pick[1]
    elements = el.split("\t")
    ip = elements[-1]

    #print IP
    try:
      global geodata
      data = geodata.record_by_name(ip)
      country = data['country_name']
      city = data['city']
      longi = data['longitude']
      lat = data['latitude']


      return [url + ', ' + str(city)+', ' + str(country)] 
    except:
      return ["Nowhere"]

def write_story(ident, story_txt):
  story_txt = story_txt.replace('<br>', '\n')
  text_file = open("/home/pi/Stories/" + ident + ".txt", "w")
  text_file.write(story_txt)
  text_file.close()

def broadcast(ident, url, date):
    global user_locs

    locs = location(url)

    if ident not in user_locs:
      user_locs[ident] = []

    for loc in locs:
      if loc not in user_locs:
        user_locs[ident].append(loc)
    
    story_txt = story(ident)

    write_story(ident, story_txt)
    resp = {
        'url': url,
        'name': ident,
        'date': int(time.mktime(date.timetuple())),
        'story': story_txt
    }
    json_str = json.dumps(resp)
    #print(json_str)
    global clients
    for client in clients:
        client.sendMessage(json_str)

def handle_query_str(str, date):
    txt = str.split('query')
    elems = txt[len(txt) - 1].split(' ')
    if len(elems) >= 4:
        ip = elems[3].strip()
        req = elems[1].strip()
        if "in-addr.arpa" in req:
          return
        try:
          ident = idents[ip]
          print ident + ' spoke to ' + req
          broadcast(ident, req, date)
        except:
          print ip + ' spoke to ' + req
          broadcast(ip, req, date)

#query[A] physics.stackexchange.com from 10.1.1.136

def loop():
    last_date = datetime.utcnow()
    while True:
      time.sleep(5)
      with open('/var/lib/misc/dnsmasq.leases') as f:
        for line in f:
          elems = line.split(' ')
          idents[elems[2]] = elems[3]

      with open('/var/log/daemon.log') as f:
        for line in f:
          if query_pattern.search(line) is not None:
            date_str = date_pattern.search(line).group()
            date = datetime.strptime(date_str, '%b %d %H:%M:%S')
            date = date.replace(datetime.utcnow().year)
            if date > last_date:
              handle_query_str(line, date)
              last_date = date
      sys.stdout.flush()

t = threading.Thread(target=loop)
t.start()

server = SimpleWebSocketServer('', 8000, WSClient)
server.serveforever()
n-addr.arpa
