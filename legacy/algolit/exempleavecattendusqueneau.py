#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
import treetaggerwrapper
import pickle
import codecs
#import pickle pour bien décoder en utf-8 le fichier sinon TreeTagger hurle

# classes et fonction d'affichage du TreeTaggerWrapper
# source: http://www.fabienpoulard.info/post/2011/01/09/Python-et-Tree-Tagger


class TreeTaggerWord:
  def __init__(self, triplet):
    self.word,self.postag,self.lemma = triplet


def formatTTG(output):
  words = []
  for w in output:
    words.append(TreeTaggerWord(w.split("\t")))
  return words

# fonction perso dérivée de formatTTG(output)

def wordCountTTG(output):
    wordCount = 0
    for w in output:
        wordCount += 1
    return wordCount

#TODO
# insérer une commande readline demandant si on veut:
# 1/ ouvrir un fichier 2/ saisir un texte
# 3/ si on affiche la sortie 4/ si on sort dans un fichier

#initialisation du dictionnaire et de la contrainte
mots_numerotes = {}
contrainte = int(7)

#début du programme lui-même : on charge un fichier

fichier = open("queneau_source.txt", 'r')
texteBrut = fichier.read().decode('utf-8')
fichier.close()

# on colle des étiquettes dans une variable nommée texte
tagger = treetaggerwrapper.TreeTagger(TAGLANG='fr',TAGDIR='/usr/share/treetagger',TAGINENC='utf-8',TAGOUTENC='utf-8')
textePOS = formatTTG(tagger.TagText(texteBrut))

#on duplique texte pour effectuer les traitements dans une copie
texteContraint = textePOS

#extraction des positions des substantifs dans une liste
nombreMots = wordCountTTG(tagger.TagText(texteBrut))
print "nombre de mots: " + str(nombreMots)
listeSubstantifs = []
for i in range(len(textePOS)):
    if textePOS[i].postag == "NOM":
        listeSubstantifs.append(int(i))
#oncrée_unelistecontraintedessunstatifs_car_laforme_\
#textePOS[listeContrainteSubstantifs[l+contrainte]].word_généèreuneerreurdetypage
listeContrainteSubstantifs = [int(x+contrainte) for x in listeSubstantifs]


TODO appliquer le décalage à la liste de positions pas aux positions dans le texte
# on remplace les substantifs par position & on les remplace dans le texte
# contraint par le mot situé  position + contrainte
for l in range(len(listeSubstantifs)):
    if l < len(listeSubstantifs) - contrainte:
        texteContraint[listeSubstantifs[l]].word = (textePOS[listeSubstantifs[l+contrainte]].word).encode("utf-8")
        print str(texteContraint[listeSubstantifs[l]]) + " VS " + str(textePOS[listeSubstantifs[l]])
    else:
            pass
# on recolle ensemble les mots du texte contraint dans un fichier de sortie
# en utilisant la syntaxe des lists comprehensions (listcomps)
#listeSortie = [texteContraint[x].word for x in range(nombreMots)]
fichierContraint = codecs.open("sortie.txt", "w", "utf-8")
sortieContrainte = ''
listeSortie = ''
exceptions = set(",;.\(")
for string in listeSortie:
    #print string
    if string in exceptions :
        sortieContrainte += string
    elif string == 'Attendu':
        sortieContrainte += "\n" + string + " "
    else:
        sortieContrainte += string + " "
#print "sortieContrainte : " + sortieContrainte
fichierContraint.write(sortieContrainte)
fichierContraint.close()

