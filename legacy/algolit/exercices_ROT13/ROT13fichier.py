#!/usr/bin/env python
# -*- coding: utf8 -*-

from codecs import encode, decode

print "\n###########################################################\n#                                                         #\n#                ROT13 is NOT fun!                        #\n#                                                         #\n###########################################################"

def rotatif(source):
    source = unicode(source,"utf8")
    print "type de saisie : "
    print type(source)
    # on continue
    sortie = encode(source, "rot13")
    sortie = unicode(decode(sortie, "iso-8859-1"))
    print "type de sortie : "
    print type(sortie)
    return sortie

while 1:
    print rotatif(raw_input(u"\nSaisissez une phrase : "))

#bug en cas de caractères non-ASCII
