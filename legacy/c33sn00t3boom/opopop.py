#!/usr/bin/env python
# -*- coding: utf8 -*-

import getpass, poplib

def monMail(MAILSERVER, USER, PASS):
    """récupération des mails depuis un serveur pop3"""
    serveur = poplib.POP3(MAILSERVER)
#    serveur.quit()
#    print serveur.getwelcome()
    serveur.user(USER)
    serveur.pass_(PASS)
    serveur.list()
    (nombreMsg, tailleTotale) = serveur.stat()
    if nombreMsg == 0:
        print "\n "+USER+" at "+MAILSERVER
        return
    else:
        sortie = ''
        print "Récupération de "+`nombreMsg`+" messages pour "+USER+" sur "+MAILSERVER
        for nb in range(nombreMsg):
            sortie += "Message "+str(nb)+" : \n"
            for contenu in serveur.retr(nb+1)[1]:
            #print "Header : \n", header
                sortie += str(contenu)
    serveur.quit()
    return sortie

if __name__ == "__main__":

print "\n\nSalut, vieille branche!\n\n"
monMail("pop.free.fr","c33sn0t3b00m","")
