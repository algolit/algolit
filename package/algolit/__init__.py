#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Algolit est un module de manipulation de texte développé par le groupe de recherche artistique Algorithme et littérature composé de An Mertens, Nicolas Malevé, Catherine Lenoble & Olivier Heinry. http://www.adashboard.org/?tag=i-literature'''

def LISEZMOI():
    print "Ce module n'est pas conçu pour être lancé en ligne de commande."
    
if __name__ == '__main__':
    LISEZMOI()
