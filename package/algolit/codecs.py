#!/usr/bin/env python
# -*- coding: utf-8 -*-

from codecs import encode, decode

def rot13(source):
    """encode en ROT13 une chaîne utf-8 et la recrache en utf-8"""
    source = unicode(source,"utf8")
    sortie = encode(source, "rot13")
    #rot13 recrache toujours un string, on doit donc décoder & réencoder en UTF-8
    sortie = "\n"+unicode(decode(sortie, "iso-8859-15"))
    return sortie

def l33tEncode():
    pass
    
def l33tDecode():
    pass
   
def Splus7():
    pass


