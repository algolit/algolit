#!/usr/bin/env python
# -*- coding: utf-8 -*-
fondamental=['A','a','B','b','C','c','D','d','E','e','F','f','G','g','H','h','I','i','J','j','K','k','	L','l','	M','m','N','n','	O','o','	P','p','	Q','q','	R','r','	S','s','	T','t','	U','u','	V','v','	W','w','	X','x','	Y','y','	Z','z']
propre=[]
voyelles=['A','a','E','e','I','i','O','o','U','u','Y','y']
consonnes=['B','b','C','c','D','d','F','f','G','g','H','h','J','j','K','k','L','l','M','m','N','n','P','p','Q','q','R','r','S','s','T','t','V','v','W','w','X','x','Z','z']
ponctuation=[',',';','.','!','?','-','_',':',','(',')','{','}','[',']','/']
digrammes=['ai','ay','ch','ck','ea','ie','ei','ee','gh','kn','ll','ng','oo','ph','pn','ps','pt','sh','th','ue','wh']
