#!/usr/bin/env python
# -*- coding: utf8 -*-
"""récupération et préfiltrage du spam situé d'un honeypot sur un serveur IMAP""" 
import os, imaplib, codecs
from ConfigParser import SafeConfigParser

cfgParser = SafeConfigParser({'serverType':'IMAP','serverName':'imap.free.fr', 'port':'993', 'connectionSecurity':'SSL','login':'portnawak','password':'dummy', 'folder':'INBOX'})

#print cfgParser.sections()
   

def getMailCfg(cfgParser, cfgFile, cfgSection):
    if os.path.isfile(cfgFile):
        cfgParser.read(cfgFile)
    else:
        print "config file does not exist"
    for section in cfgParser.sections():
        if section != cfgSection:
            print "Skipping configSection " + section 
        elif section == cfgSection:        
            print "Parsing %s from file %s \n " %  (section, cfgFile)
            cfgDict= {}
            for name, value in cfgParser.items(section): 
                cfgDict[name] = value
            return cfgDict
    
def getImapMail(servername, port, connectionsecurity, login, password, folder):
    """Récupération et filtrage des mails depuis un serveur IMAP. Grandement redevable à Doug Hellman http://pymotw.com/2/imaplib/ CC 3.0 BY-SA-NC"""
    
    if connectionsecurity.upper() == "SSL":
        myConn = imaplib.IMAP4_SSL(servername, port)
    else:
        myConn = imaplib.IMAP4(servername, port)
        
    myConn.login(login, password)
    answer = myConn.list()
    print 'Liste des dossiers:\n', answer

    
    # TODO mettre en fonction la connection à un dossier
    
    #folder = 'NigerianScam'
    
    answerSelect = myConn.select(folder, readonly=1)
    
    if answerSelect[0] !=  'OK':
        print 'La connection au dossier %s a échoué: %s' % (folder, answerSelect[1])
    else:
        print "\nLe dossier %s contient %s messages." % ( folder, str(answerSelect[1][0]))
        
#    echo, uids = myConn.search(None, '(TO "undisclosed-recipients:;")')
    echo, uids = myConn.search(None, 'FLAGGED')
    numMsg = 0
    for uid in uids[0].split():
        numMsg +=1        
    print 'Nombre de messages sélectionnés: %s \n' % (numMsg)
    
    for uid in uids[0].split():
        print 'identifiant unique du message:', uid
        # les 2 lignes qui suivent viennent de Laurence's blog http://blog.entek.org.uk/?p=168 
        echo, contenu = myConn.fetch(uid,'(BODY.PEEK[HEADER.FIELDS (SUBJECT FROM TO DATE)])')
        print '===================================\n %s \n'% ('\n'.join(contenu[0][1].rstrip().split("\r\n")))
        #on traite l'élément 1 du premier élément 0 du tuple formé par contenu
        # la méthode rstrip() permet de faire sauter les espaces à la droite de chaque ligne
        # la méthode .split("\r\n") permet de découper à chaque combinaison de retour chariot + nouvelle ligne
        # la méthode join permet de concaténer les différentes lignes séparées par un saut de ligne \n
    myConn.close()    
    myConn.logout()

if __name__ == "__main__":

    print "\n\nSalut, vieille branche!\n\n"
    
    cfgDict = getMailCfg(cfgParser,'cfgMailAccount.ini','secureImapSpamServer')
    
    if cfgDict['servertype'].lower() == 'imap':
        getImapMail(cfgDict['servername'], cfgDict['port'], cfgDict['connectionsecurity'], cfgDict['login'], cfgDict['password'], cfgDict['folder'])
        
