#!/usr/bin/python
# -*- coding: utf-8 -*-

# Chargement des modules pour l'encodage et les expressions rationnelles
import re
import codecs
import os
# Préparation de l'expression rationnelle
reg_split = re.compile("[ \t]")
# Initialisation du compteur de mots
nbmots = 0
# Initialisation du dictionnaire pour compter la distribution des mots
dic_mots = {}
# Parcours des fichiers du corpus
for fichier in os.listdir("corpus/"):
	fh = codecs.open( os.path.join("corpus", fichier), "r", "utf-8")
	contenu = fh.read()
	mots = reg_split.split(contenu)
	nbmots += len(mots)
	fh.close()
	# On ajoute les mots à la distribution
	for mot in mots:
		if dic_mots.has_key(mot):
			dic_mots[mot] += 1
		else:
			dic_mots[mot] = 1

print "%d mots dans le corpus" % nbmots

# On extrait les 10 mots les plus présents
from operator import itemgetter
mots_sorted = sorted(dic_mots.items(), key=itemgetter(1), reverse=True)
print "Les 10 mots les plus fréquents : %s" % mots_sorted[:10]
