#!/usr/bin/python
# -*- coding: utf-8 -*-

# Chargement des modules pour l'encodage et les expressions rationnelles
import sys
import re
import codecs
import os
import treetaggerwrapper
# Initialisation du compteur de mots
nbmots = 0
# Initialisation du dictionnaire pour compter la distribution des mots
dic_mots = {}
# Initialisation de l'étiqueteur
tagger = treetaggerwrapper.TreeTagger(TAGLANG='fr',TAGDIR='/tmp',TAGINENC='utf-8',TAGOUTENC='utf-8')
# Parcours des fichiers du corpus
for fichier in os.listdir("corpus/"):
	print "Dealing with %s" % fichier
	fh = codecs.open( os.path.join("corpus", fichier), "r", "utf-8")
	contenu = fh.read()
	fh.close()
	# Étiquetage du contenu
	tags = tagger.TagText(contenu)
	print tags
	nbmots += len(tags)
	print "%d words collected" % nbmots
	# Filtrage des noms communs
	for tag in tags:
		try:
			mot,pos,lemme = tag.split("\t")
			if pos == "NOM":
				# Ajout du NOM à la distribution
				if dic_mots.has_key(mot):
					dic_mots[mot] += 1
				else:
					dic_mots[mot] = 1
		except ValueError:
			# Mauvaise étiquette, on ignore
			print "Ignoring tag %s" % tag
			pass

# On extrait les 100 noms les plus présents
from operator import itemgetter
mots_sorted = sorted(dic_mots.items(), key=itemgetter(1), reverse=True)
print "Les 100 mots les plus fréquents :"
for mot in mots_sorted[:100]:
	line = u"%s:%s\n" % (mot[0],mot[1])
	sys.stdout.write(line.encode("utf-8",'replace'))

