#!/usr/bin/python
# -*- coding: utf-8 -*-

# Chargement des modules
import sys
import re
import codecs
import os
import treetaggerwrapper

#############################################################################
############### FONCTIONS DIVERSES DE PRÉTRAITEMENT #########################
#############################################################################

def loadSW():
	# Liste des mots outils pour le français (http://members.unine.ch/jacques.savoy/clef/frenchST.txt)
	stopwords = []
	fh = codecs.open("frenchST.txt", "r", "latin-1")
	for line in fh.readlines():
		stopwords.append(line.strip())
	fh.close()
	print "%d stopwords loaded..." % len(stopwords)
	return stopwords

def tokenize(text):
	# Il vaudrait mieux la définir à l'extérieur... mais bon !
	regexp = re.compile("\s+", re.UNICODE)
	return regexp.split(text)

#############################################################################
############## FONCTIONS D'EXTRACTION DES TRIGRAMMES ########################
#############################################################################

def extractTrigrams(mots, dic):
	"""
	@param mots : liste des mots
	@param dic : dictionnaire des trigrammes extraits et leur nombre
		d'occurrence associé
	"""
	for i in range(3, len(mots)):
		trig = " ".join(mots[i-3:i])
		if not dic.has_key(trig):
			dic[trig] = 1
		else:
			dic[trig] += 1

def extractTrigramsSW(mots, stopwords, dic):
	"""
	@param mots : liste des mots
	@param stopwords : liste des mots outils à filtrer
	@param dic : dictionnaire des trigrammes extraits et leur nombre
		d'occurrence associé
	"""
	# On supprimme les mots outils
	motsclean = []
	for mot in mots:
		if not mot in stopwords:
			motsclean.append(mot)
	# On extrait les trigrammes
	extractTrigrams(motsclean, dic)

def extractTrigramsNouns(tags, dic):
	"""
	@param tags : liste des mots étiquetés par TreeTagger
	@param dic : dictionnaire des trigrammes extraits et leur nombre
		d'occurrence associé
	"""
	# Filtrage des mots qui sont des noms ou des adjectifs
	noms = []
	for tag in tags:
		try:
			mot,role,lemme = tag.split("\t")
			if (role == "NOM") or (role == "ADJ"):
				noms.append(mot)
		except ValueError:
			# Mauvaise étiquette
			pass
	# Extraction des trigrammes
	extractTrigrams(noms, dic)

def extractTrigramsVerbs(tags, dic):
	"""
	@param tags : liste des mots étiquetés par TreeTagger
	@param dic : dictionnaire des trigrammes extraits et leur nombre
		d'occurrence associé
	"""
	# Filtrage des mots qui sont des verbes ou des adverbes
	verbs = []
	for tag in tags:
		try:
			mot,role,lemme = tag.split("\t")
			if (role == "ADV") or role.startswith("VER:"):
				verbs.append(mot)
		except ValueError:
			# Mauvaise étiquette
			pass
	# Extraction des trigrammes
	extractTrigrams(verbs, dic)

#############################################################################

# Chargement des mots outils
stopwords = loadSW()
# Initialisation de l'étiqueteur
tagger = treetaggerwrapper.TreeTagger(TAGLANG='fr',TAGDIR='/tmp',TAGINENC='utf-8',TAGOUTENC='utf-8')

# Initialisation des dictionnaires
lstRegularTrigrams = {}
lstRegularTrigramsSW = {}
lstNounsTrigrams = {}
lstVerbsTrigrams = {}

# Parcours des fichiers du corpus
for fichier in os.listdir("corpus/"):
	print "Dealing with %s" % fichier
	fh = codecs.open( os.path.join("corpus", fichier), "r", "utf-8")
	contenu = fh.read()
	fh.close()
	# Découpage en mots
	mots = tokenize(contenu)
	# Extraction des trigrammes sans filtrage des mots outils
	extractTrigrams(mots, lstRegularTrigrams)
	# Extraction des trigrammes avec filtrage des mots outils
	extractTrigramsSW(mots, stopwords, lstRegularTrigramsSW)
	# Étiquetage du contenu
	tags = tagger.TagText(contenu)
	# Extraction des trigrammes nominaux (noms + adjectifs)
	extractTrigramsNouns(tags, lstNounsTrigrams)
	# Extraction des trigrammes verbaux (verbes + adverbes)
	extractTrigramsVerbs(tags, lstVerbsTrigrams)

# On extrait les 100 trigrammes les plus présents de chaque catégorie
from operator import itemgetter
for dic in [lstRegularTrigrams, lstRegularTrigramsSW, lstNounsTrigrams, lstVerbsTrigrams]:
	trigrams_sorted = sorted(dic.items(), key=itemgetter(1), reverse=True)
	print "-"*80
	for trig in trigrams_sorted[:100]:
		line = u"%s:%s\n" % (trig[0],trig[1])
		sys.stdout.write(line.encode("utf-8",'replace'))

