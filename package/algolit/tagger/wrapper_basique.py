#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
import treetaggerwrapper
texteBrut = u'essai avec un é et/où un à'

tagger = treetaggerwrapper.TreeTagger(TAGLANG='fr',TAGDIR='/usr/share/treetagger',TAGINENC='utf-8',TAGOUTENC='utf-8')
texteTagged =  tagger.TagText(texteBrut)
print texteTagged
