#!/usr/bin/env python

import os
from distutils.core import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name='algolit',
    version = '0.1',
    description = 'Set of tools to manipulate natural language texts (spam, free literature...)',
    license = 'LGPL',
    platforms = ['any'],
    author = 'Olivier Heinry','An Mertens'
    author_email = 'olivier.heinry@free.fr',
    url = 'http://gitorious.org/ci-git/algolit',
    packages = ['algolit'],
    package_data = {'algolit': ['dict/*', ]},
    long_description = read('README'),
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers, writers',
        'License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)',
        'Natural Language :: French',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Text Processing :: Linguistic :: Spam :: Literature',
    ],
      )
