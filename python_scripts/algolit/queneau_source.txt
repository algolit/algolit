Tribunal de grande instance de Paris, Ordonnance de référé du 5 mai 1997
Jean-Marie Q. / Christian L., l’Université Paris VIII

reproduction - droit moral - agent assermenté app - respect intégrité de l’ œuvre - exception de copie privée

Vu l’assignation introductive de la présente instance, et les motifs y énoncés,

Jean-Marie Q. fils unique et unique légataire de Raymond Queneau auteur de l’œuvre littéraire Cent Mille Milliards de Poèmes parue aux Editions Gallimard jouit des droits exclusifs tant patrimoniaux que moraux sur l’œuvre de son père ;

Ayant appris, et fait constater le 16 janvier 1997 par un agent assermenté de l’Agence pour la Protection des Programmes, que l’œuvre précitée était diffusée illicitement, et sans restriction d’accès sur le réseau Internet par Monsieur Christian L., hébergé par un organisme dénommé "La Mygale" au moyen des serveurs fournis par les services de l’Université de Paris VIII, la connexion au réseau étant fournie par le réseau Renater, Jean-Marie Q. invocant tout d’abord une atteinte au droit moral de l’auteur, au motif que celui-ci (ou ses ayants droits) a seul le droit de divulguer son œuvre et de déterminer le procédé de divulgation et ensuite une atteinte au droit au respect et à l’intégrité de l’œuvre, motif pris que l’acte de diffusion entraîne ipso facto une dénaturation de l’œuvre à raison de son insertion dans un cadre non neutre constitué par la page Web et qu’au surplus les vers reproduits et tels qu’ils sont visualisés ne respectent pas la structure même de l’œuvre, nous demande de :

    -constater que la diffusion de l’œuvre de Raymond Queneau et en particulier de "Cent Mille Milliards de Poèmes" sous réserve de tous autres qui seraient reconnues à la barre, sur le réseau d’Internet constitue un acte de contrefaçon et un trouble manifestement illicite qu’il convient de faire cesser sans délai, 

    -faire interdiction à Christian L. et l’Université Paris VIII sous astreinte de 100.000 francs français par jour à compter du prononcé de l’ordonnance sollicitée, d’implanter les services litigieux sur un site ou un serveur tiers en France ou dans quelque pays que ce soit, 

    -condamner solidairement Christian L. et l’Université Paris VIII à payer à Jean-Marie Q. par provision sur les dommages-intérêts la somme de 450.000 francs français, 

    -ordonner conformément à l’article L.335-6 alinéa 2 du Code de la Propriété Intellectuelle la publication de la décision à intervenir dans cinq supports de presse écrite spécialisée, au choix du demandeur et aux frais in solidum des défendeurs sans que le montant de chaque insertion ne puisse dépasser 30.000 francs, 

    -ordonner l’insertion de la décision à intervenir sur les pages d’accueil des serveurs et des sites du défendeur en langue française et en langue anglaise pendant une durée de deux mois, 

    -faire injonction aux défendeurs de supprimer les liens avec tous autres sites renvoyant vers leurs serveurs et contenant des oeuvres de Raymond Queneau sous même astreinte, 

    -nommer tel huissier au constat et au contrôle des mesures ordonnées qu’il plaira à Monsieur le Président du tribunal de désigner, 

    -condamner Christian L. et l’Université Paris VIII à payer au demandeur la somme de 5O.OO0 francs sur le fondement de l’article 700 du nouveau code de procédure civile ainsi qu’aux dépens dont distraction au profit de Maître Jacques-Georges Bitoun conformément aux dispositions de l’article 699 du nouveau code de procédure civile ; 

    -renvoyer pour le surplus les parties à se pourvoir devant les juridictions compétentes. 

Au soutien de ses conclusions tendant principalement au rejet de la demande, Monsieur C. L. fait valoir tout d’abord que le constat du 16 janvier 1997 a été établi par une personne non habilitée pour ce faire, l’Agent de l’Agence pour la Protection des Programmes n’étant habilité à dresser des constats qu’à l’égard de contrefaçons de programmes informatiques, de jeux vidéo, de progiciels... et ce à l’exclusion de tout autre type d’œuvre, notamment une œuvre littéraire ;

Il fait valoir ensuite que l’agent a excédé les limites de sa mission puisqu’il indique dans ses conclusions :

"Nous sommes donc en présence de reproduction par numérisation d’oeuvres littéraires protégées par le droit d’auteur, susceptibles d’être mises à la disposition de personnes connectées au réseau Internet sans avoir été expressément autorisées par le titulaire ou le cessionnaire des droits".

"Il appartiendra au Tribunal de dire s’il s’agit d’un service de communication audiovisuelle,..., et si à ce titre, il y a défaut de déclarations préalable", ce qui constitue des appréciations d’ordre juridique ;

Il fait valoir en outre que le constat relève d’une analyse incomplète puisqu’il ne précise pas de quelles façons l’agent a opéré, passant ainsi sous silence les manipulations spécifiques auxquelles il s’est livré, ce que n’aurait pas pu faire un utilisateur non expérimenté ;

Il conteste, par ailleurs, avoir commis des actes de contrefaçon, n’ayant selon ses propres termes fait qu’’user de la possibilité offerte à tout un chacun et conformément à l’esprit de Raymond Queneau créer sa propre poésie, selon l’inspiration du moment ou le hasard des bandelettes incorporant les alexandrins ;

Il tient à préciser que pour illustrer sa page Web, il ne proposait que la visualisation de l’un des "Cent Mille Milliards de Poèmes" possibles et ce grâce à un programme de combinaison aléatoire appelé "générateur de poème" ;

Il soutient qu’en procédant ainsi il n’a fait qu’utiliser le droit de citation reconnu par l’article L.122-5-3 du code de la propriété intellectuelle exclusif de toute atteinte au droit moral de l’auteur ;

Il conteste enfin le bien fondé des demandes de provision et de publication insistant sur le fait que le constat du 16 janvier 1997 n’a recensé que 104 visiteurs à titre gratuit de son site Web et que depuis la délivrance de l’assignation, aucun accès n’est plus possible ainsi qu’il a été constaté par l’huissier le 8 AVRIL 1997 ;

L’Association Mygale Point Org et Frédéric C. concluent à l’irrecevabilité de l’ensemble des demandes au motif tout d’abord que dans on procès verbal de constat du 16 janvier 1997 qui sert de fondement à la présente demande, l’Agent de l’A.P.P. s’est livré non seulement à des commentaires juridiques qui sortent du cadre de sa mission mais encore à des appréciations inexactes et incomplètes de nature à induire le profane en erreur sur la nature de la responsabilité de Mygale qui ne gère qu’un serveur d’hébergement gratuit de pages Web sur le réseau Internet, mais qui ne propose ni accès à Internet ni conception des pages Web et qu’en conséquence, l’Agent ayant excédé les limites de sa mission, son procès verbal ne saurait constituer une preuve régulière des faits allégués en demande ;

Ils ajoutent qu’en tout état de cause le trouble allégué a cessé puisque dès que Monsieur C. L. eut connaissance des revendications du demandeur, il supprima de sa propre initiative tous les fichiers litigieux de son site ;

Ils contestent le bien fondé de la demande de provision ;

    -Monsieur F. C. qui a créé le serveur d’hébergement Mygale dans le cadre d’un projet de maîtrise accepté par l’Université qui lui a fourni les moyens nécessaires, sollicite sa mise hors de cause en ce qu’il a été attrait dans la procédure à titre personnel ;

    -L’Université Paris VIII sollicite sa mise hors de cause et le rejet de l’ensemble des demandes au motif tout d’abord qu’au jour de la délivrance de l’assignation, le trouble allégué avait cessé et que l’éventuelle voie de fait qui aurait pu lui être opposée n’existait plus. Elle fait valoir ensuite qu’elle n’a commis aucun acte positif portant atteinte aux droits patrimoniaux du demandeur n’ayant été qu’un simple fournisseur d’accès pour le serveur Mygale.

***

    Attendu que la numérisation d’une œuvre, technique consistant à traduire le signal analogique qu’elle constitue en un mode numérique ou, binaire qui représentera l’information dans un symbole à deux valeurs 0 et 1 dont l’unité est le Bit, constitue une reproduction de l’œuvre qui requiert en tant que telle lorsqu’il s’agit d’une œuvre originale, l’autorisation préalable de l’auteur ou de ses ayants droits ;

    Attendu qu’une numérisation sans autorisation est donc illicite et constitue une contrefaçon ;

    Attendu toutefois qu’une telle numérisation peut échapper à la sanction de la contrefaçon lorsqu’elle a été faite pour une usage strictement privé dans les conditions de l’article L.122-5-2 du code de la propriété intellectuelle ou lorsqu’elle est invoquée dans le cadre de l’exception dite "de courtes citations" ;

    Attendu, en l’espèce, que l’œuvre de Raymond Queneau, œuvre originale, a été numérisée et mise en ligne sans l’autorisation de Jean-Marie Q., seul titulaire des droits patrimoniaux et moraux sur l’œuvre de son père, et sans l’autorisation des Editions Gallimard, cessionnaires des droits de reproduction et de représentation ;

    Attendu que l’ensemble des défendeurs contestent la validité du procès verbal de constat du 16 janvier 1997 au motif tout d’abord que l’Agent de l’A.P.P. n’avait pas qualité pour effectuer ce constat et au motif ensuite que son constat contient des appréciations d’ordre juridique notamment en ce qu’il énonce "nous sommes en présence de reproductions par numérisation d’oeuvres protégées par le droit d’auteur susceptibles d’être mises à la disposition des personnes connectées au réseau Internet sans avoir été expressément autorisées par le titulaire ou le cessionnaire des droits. . . ". "Il appartiendra au Tribunal de dire s’il s’agit d’un service de communication audiovisuelle et si à ce titre, il y a défaut de déclaration préalable" ;

    Attendu que Emmanuel C. qui a dressé le procès-verbal litigieux est agent assermenté de l’Agence pour la Protection des Programmes ;

    Attendu que l’APP a pour objet de défendre les personnes physiques ou morales, auteur de programmes informatiques, de jeux vidéo, de progiciels, d’oeuvres numériques, d’études et de documents associés :

    Attendu que la Société les Editions Gallimard est la cessionnaire exclusive des droits de reproduction et de représentation de l’œuvre de Raymond Queneau y compris dans le cadre de réseaux numériques ;

    Qu’en conséquence, pour assurer la protection de ses droits comme ceux de sa cessionnaire, Jean-Marie Q. était fondé à solliciter le concours de l’APP ;

    Attendu que Christian L. soutient, par ailleurs, que la numérisation à laquelle il a procédé se trouverait légitimée dans le cadre de l’exception dite de copie privée de l’article L.122-5-2 du code de la propriété intellectuelle ;

    Mais attendu qu’il est acquis aux débats que l’accès à ses pages privées se faisait sans aucune restriction ;

    Qu’ainsi en permettant à des tiers connectés au réseau Internet de visiter ses pages privées et d’en prendre éventuellement copie, il a favorisé l’utilisation collective da sa reproduction ; qu’au demeurant, il importe peu qu’il n’ait effectué lui-même aucun acte positif d’émission, l’autorisation de prendre copie étant implicitement contenue dans le droit de visiter ses pages privées ;

    Attendu que Christian L. oppose en outre à la demande l’exception dite de courte citation :

    Mais attendu que le procédé qu’il a employé et qui a consisté à découper l’œuvre qui a été numérisée dans son intégralité en une succession de poèmes, chacun des visiteurs de ses pages privées ne pouvant visualiser à la fois que l’un des poèmes parmi les "Cent Mille Milliards" qui sont proposés, ne relève pas de la courte citation au sens que lui donne l’article L.122-5-3 du code de la propriété intellectuelle ;

    Qu’en effet, d’une part, le poème visualisé par le visiteur n’est pas destiné à être incorporé à une autre œuvre à laquelle il apporterait un élément pédagogique, scientifique ou d’information ;

    Que d’autre part, le procédé employé autorise, dans l’absolu, la reconstitution intégrale de l’œuvre par rapprochement de "citations successives", cette reconstitution étant incompatible avec la notion de courte citation ;

    Attendu que les défendeurs font valoir en tout état de cause que le trouble allégué ayant cessé depuis la délivrance de l’assignation, il n’y a pas matière à référé ;

    Mais attendu que pour être utilement invoquée, la cessation dudit trouble doit avoir été constatée contradictoirement et amiablement par l’ensemble des parties concernées ; que tel n’a pas été le cas en l’espèce ;

    Attendu que Jean-Marie Q. et les Editions Gallimard étaient donc fondés à saisir notre juridiction aux fins de voir ordonner ou constater la cessation du trouble ;

    Attendu qu’il y a lieu de constater en l’espèce que dès qu’il eut connaissance des revendications du demandeur, Christian L. a supprimé de sa propre initiative tous les fichiers litigieux de son site, ce que Maître Plumel huissier de Justice a pu vérifier ainsi qu’il résulte de son procès-verbal de combat du 8 avril 1997 ;

    Qu’en tant que de besoin, il sera ordonné sous astreinte aux défendeurs de prendre à cette fin les mesures nécessaires ;

    Attendu qu’il existe en l’espèce un principe de préjudice ;

    Attendu qu’en l’état, il sera alloué au demandeur et à la charge de Christian L. une provision sur dommages intérêts de 1 franc ;

    Attendu qu’il est justifié d’allouer au demandeur et à la charge de Christian L. la somme de 6.000 francs sur le fondement de l’article 700 du nouveau code de procédure civile ;

    Attendu que la question du contrôle des activités de Christian L. sur son site Web, comme celle des responsabilité qui pourraient peser tant sur l’association Mygale qui a hébergé ce site que sur l’Université de Paris VIII qui a ouvert le site doivent faire l’objet d’un débat de fond ;

    Attendu que dans les circonstances de l’espèce il n’y a pas lieu d’ordonner la publication de notre décision ; que les demandeurs et intervenante volontaire pourront toutefois procéder à cette publication à leurs frais avancés dans tout support de leur choix ;

    Attendu, enfin, que la mise en cause de Monsieur C. ne se justifie pas ;

    Par ces motifs

    Mettons Frédéric C. hors de cause ;

    Donnons acte aux Editions Gallimard de ce qu’elles déclarent s’associer à la demande de Monsieur Queneau ;

    Constatons que Christian L. a, sans autorisation, reproduit et favorisé une utilisation collective de l’œuvre de Raymond Queneau "Cent Mille Milliards de Poèmes", protégée par le droit d’auteur et dont les Editions Gallimard sont les cessionnaires du droit de reproduction et de représentation y compris sous une forme numérique ;

    Mais constatons qu’il a été mis fin au trouble illicite qui en résultait par suite de la décision de Christian L. de supprimer les fichiers litigieux ;

    En tant que de besoin, faisons interdiction aux défendeurs de mettre l’œuvre de Raymond Queneau à la disposition des utilisateurs du réseau Internet ce sous astreinte de 10.000 francs par infraction constatée ;

    Condamnons Christian L. à payer au demandeur la somme de 1 franc à titre de provision sur dommages-intérêts, outre celle de 6.000 francs sur le fondement de l’article 700 du nouveau code de procédure civile ;

    Disons qu’il n’y a pas lieu, eu égard aux circonstances de l’espèce, d’ordonner la publication de la présente décision ;

    Autorisons toutefois les demandeurs et intervenant volontaire à procéder à cette publication dans tout support de leur choix et a leurs frais avancés ;

    Renvoyons la connaissance des autres chefs de demande au juge du fond ;

    Mettons les dépens à la charge de Christian L. ;

    Le tribunal : J.J. Gomez (Président)

    Avocats : Me Bitoun - Me Langlois - Me Gomez del Junco - Me Sédallian

    Intervenant volontaire : Les Editions Gallimard représentées par Me Bénazeraf.

