#!/usr/bin/env python

# -*- coding: utf8 -*-

from codecs import encode, decode

print "\n###########################################################\n#                                                         #\n#                ROT13 is NOT fun!                        #\n#                                                         #\n###########################################################\n"

print "Ctrl+c pour quitter le programme.\n"

def rotatif(source):


    source = unicode(source,"utf8")

    sortie = encode(source, "rot13")


    sortie = "\n"+unicode(decode(sortie, "iso-8859-15"))

    return sortie

try:

    while 1:

        print rotatif(raw_input(u"\nSaisissez une phrase : "))

except KeyboardInterrupt:

    print "\n\nTot later!\n"

    pass

    

#TODO  if __name__ == __main__ thingie
