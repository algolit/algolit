#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass, poplib

M = poplib.POP3('pop3.free.fr')
M.user(getpass.getuser())
M.pass_(getpass.getpass())
numMessages = len(M.list()[1])
for i in range(numMessages):
    for j in M.retr(i+1)[1]:
        print j
M.quit()
