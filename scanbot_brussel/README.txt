How to use ScanBot
------------------

2012-02-07


- Make sure ScanBot is switched on.

- First switch on the LEFT camera ( just press on/off button ).
- A dialog box opens and asks you what to do with the camera. Click on "Cancel".
- Then switch on the RIGHT camera.
- Again a dialog box opens and asks you what to do with the camera. Click on "Cancel".
- The system has mounted the cameras. Two camera icons show up on the desktop.
- Right-click on each of the camera icons on desktop and select "unmount".
- Important: Both the camera icons should now be gone.

- Open the Terminal by double-clicking on it.
- In the terminal type: showscans
- Two images of (old) scanned pages will show up on the desktop.
  Arrange them left and right on the screen. They will update with fresh
  images as the book is being scanned.

To load a book:
- Press the red button "Turn" ( right front corner of ScanBot ) for a few seconds.
  The vacuum box will lift up.
- When the vacuum box is in the top position flip the switch "Motor Stop" next to the 
  red button in the up position. The motor will stop and the vacuum box will stop moving.
- Put your book in the cradle. Adjust the sides of the cradle to make a good
  fit if necessary.
- Flip the switch back to the bottom position. The vacuum box will come down 
  and stop on top of the book

To scan pages:
- In the terminal type: scanbot 10 
  This will scan 10 double pages automatically ( scanbot 5 will scan 5 double pages etc... )
- To scan just the present pages without turning the pages type: scanbot       
- The pages are saved in the folder called "images" ( Double-click on the "ScanBot" icon to see it )

