#!/usr/bin/env python

#  Driver for HSBXL ScanBot
#
#  2012-02-09  v0.2
#
#  Usage: scanbot n
#
#    This will scan n double pages automatically
#    To scan just the present pages without turning the pages use: scanbot       
#    The pages are saved in the folder called "images" 
#
#  Copyright 2011, 2012 Pieter Heremans, Michael Korntheuer
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import serial
import time
import subprocess
import re
import string
import Queue
import threading
import glob
import os
import sys


serialport = "/dev/ttyACM0"     # linux: "/dev/ttyACM0"     Windows: "COM6"

imagedir = "images"



def execute(command):  # execute a shell command in a subprocess and return output
    return subprocess.Popen(command, stdout=subprocess.PIPE, shell=True).communicate()[0]  # , stderr=subprocess.STDOUT

def ptpcam(args):     # sent ptpcam command and return output
    return execute('ptpcam ' + args)

def shoot(camera):
        ptpcam(camera + ' --chdk="luar shoot()"')

def download(camera,newfilename):  # download most recent picture from camera and delete it on the camera
    # find most recent image directory
    output = ptpcam(camera + ' --chdk="luar os.listdir(\\"A/DCIM\\")"') 
    match = re.findall("\d\d\dCANON",output)
    match.sort()
    directory = match[-1]

    # find most recent image file
    output = ptpcam(camera + ' --chdk="luar os.listdir(\\"A/DCIM/' + directory + '\\")"')
    match = re.findall("IMG_\d\d\d\d\.JPG",output)
    match.sort()   
    filename = match[-1]

    # download image file
    ptpcam(camera + ' --chdk="download A/DCIM/' + directory + '/' + filename + ' ' + newfilename + '"') 

    # delete image file
    ptpcam(camera + ' --chdk="luar os.remove(\\"A/DCIM/' + directory + '/' + filename + '\\")"')

def send_serial(strs):  # send a command to the arduino and ignore the reply
    global arduino
    arduino.write(strs)
    arduino.flushInput()

def turn_page():  # all steps to turn one page
    send_serial("vacuum 1;")
    time.sleep(1.5);
    send_serial("turn 1;")
    time.sleep(2.6);
    send_serial("servo 180;")
    send_serial("servo 10;")
    time.sleep(1.0)
    send_serial("servo 180;")
    send_serial("vacuum 0;")
    time.sleep(0.1);
    send_serial("turn 0;")
    time.sleep(0.1);

class ThreadCamera(threading.Thread):   # a thread to take a picture and download it in background
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue
          
    def run(self):
        while True:
            command = self.queue.get()
            if command[0] == 'shoot':
                shoot(command[1])
            elif command[0] == 'download':
                download(command[1],command[2])
            self.queue.task_done()

class ThreadRotate(threading.Thread):  # a thread to give the picture the correct orientation in background
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue
          
    def run(self):
        while True:
            command = self.queue.get()
            #parsed = command.split(" ")
            if command[0] == 'rotateleft':
                execute('mogrify -rotate 270 ' + command[1])
                execute("cp " + command[1] + " left.jpg")
            elif command[0] == 'rotateright':
                execute('mogrify -rotate 90 ' + command[1])
                execute("cp " + command[1] + " right.jpg")
            self.queue.task_done()
       
### initialisation starts here

if not os.path.isdir(imagedir):  # make sure directory for images exists
   os.mkdir(imagedir)

files = glob.glob(imagedir + '/*.jpg')  # find the serial number of the last picture in the image directory
files.sort()
if len(files) == 0:
    counter = 1
else:
    match = re.search("page(\d\d\d\d\d)",files[-1])
    counter = int(match.group(1)) + 1

camera_queue = Queue.Queue()  # the queue for taking pictures and dowloading them
rotate_queue = Queue.Queue()  # the queue for rotating pictures
         
for i in range(2):    #spawn a pool of threads for camera control and picture rotation 
    t = ThreadCamera(camera_queue)
    t.setDaemon(True)
    t.start()
    t = ThreadRotate(rotate_queue)
    t.setDaemon(True)
    t.start()

output = ptpcam('-l')    # find out usb bus and device number of cameras
output = string.replace(output, "\n", "")    #Windows needs   output = string.replace(output,"\r\n", "")
match = re.search("(\d\d\d)/(\d\d\d).*PowerShot.*(\d\d\d)/(\d\d\d).*PowerShot",output) 
camera1 = "--bus==" + match.group(1) + " --dev=" + match.group(2)
camera2 = "--bus==" + match.group(3) + " --dev=" + match.group(4)

ptpcam(camera1 + ' --chdk="mode 1"')  # start up cameras
ptpcam(camera2 + ' --chdk="mode 1"')

arduino = serial.Serial(serialport, 9600)  # start up serial communication to arduino

if len(sys.argv) < 2:   # make sure scanbot argument is legal
    pages = 1
else:
    pages = int(sys.argv[1])
if pages < 1:
    pages = 1

### start of main loop

for i in range(pages):
    camera_queue.put(['shoot',camera1])
    camera_queue.put(['shoot',camera2])
    camera_queue.join()
    camera_queue.put(['download',camera1,imagedir + "/page" + str(counter).zfill(5) + ".jpg"])
    camera_queue.put(['download',camera2,imagedir + "/page" + str(counter + 1).zfill(5) + ".jpg"])
    if i + 1 < pages:
        turn_page()
        time.sleep(0.1)
    camera_queue.join()
    rotate_queue.join()
    rotate_queue.put(['rotateleft',imagedir + "/page" + str(counter).zfill(5) + ".jpg"])
    rotate_queue.put(['rotateright',imagedir + "/page" + str(counter + 1).zfill(5) + ".jpg"])
    counter = counter + 2

rotate_queue.join()
arduino.close()
 
### end





